################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/memcached_test.c \
../src/multiplexor_test.c \
../src/nipc_test.c \
../src/serializers_test.c \
../src/sync_dictionary_test.c \
../src/test.c 

OBJS += \
./src/memcached_test.o \
./src/multiplexor_test.o \
./src/nipc_test.o \
./src/serializers_test.o \
./src/sync_dictionary_test.o \
./src/test.o 

C_DEPS += \
./src/memcached_test.d \
./src/multiplexor_test.d \
./src/nipc_test.d \
./src/serializers_test.d \
./src/sync_dictionary_test.d \
./src/test.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


