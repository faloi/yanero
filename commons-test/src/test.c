/*
 * test.c
 *
 *  Created on: Jun 20, 2012
 *      Author: faloi
 */

#include <stdlib.h>
#include <stdio.h>
#include <commons/socket.h>
#include <commons/collections/sync_dictionary.h>
#include "test.h"

#define EXIT_CODE 27

void send_string_test() {
	t_socket* client = socket_createClient("127.0.0.1");
	int ret = socket_connect(client, "127.0.0.1", 5330);
	if (ret == -1) {
		perror("No se pudo conectar");
	}
	socket_send(client, "prueba\n", strlen("prueba\n") + 1);
	socket_destroy(client);
}


int main () {
	puts("NETWORK TEST");
	puts("1- Escuchar NIPC");
	puts("2- Escuchar string");
	puts("3- Enviar NIPC de prueba");
	puts("4- Enviar string de prueba");

	puts("");

	puts("THREAD TEST");
	puts("5- Cargar diccionario concurrentemente");
	puts("6- Imprimir diccionario concurrentemente");

	puts("");

	puts("MEMCACHED TEST");
	puts("7- Conectar con la Cache");
	puts("8- Guardar value");
	puts("9- Obtener value");
	puts("10- Intentar obtener value");
	puts("11- Borrar value");

	puts("");

	puts("HANDSHAKE TEST");
	puts("12- Handshake client");
	puts("13- Handshake server");

	puts("");

	puts("SERIALIZERS TEST");

	puts("14- Serializar Getattr");
	puts("15- Serializar Readdir");
	puts("16- Serializar Open");
	puts("17- Serializar Release");
	puts("18- Serializar Read");
	puts("19- Serializar Write");
	puts("20- Serializar Create");
	puts("21- Serializar Unlink");
	puts("22- Serializar Truncate");
	puts("23- Serializar Rmdir");
	puts("24- Serializar Mkdir");

	puts("25- Funcion separar path");
	puts("26- Eliminar path de vector");
	puts("27- Agregar path al vector");

	puts("28- Salir");

	puts ("");

//	t_sync_dictionary* dictionary = sync_dictionary_create(NULL);
	int option;
	scanf("%d", &option);

	while (option != EXIT_CODE) {
		switch (option) {
		case 1:

			printf("aca no hay nada\n");
//			multiplexor_test(listen_nipc);
			break;
		case 2:
			printf("aca no hay nada\n");
			//multiplexor_test(listen_string);
			break;
		case 3:
			nipc_client_test();
			break;
		case 4:
			send_string_test();
			break;
		case 5:

			printf("aca no hay nada\n");
			//thread_test(addToDictionary, dictionary);
			break;
		case 6:

			printf("aca no hay nada\n");
		//	thread_test(printDictionary, dictionary);
			break;
		case 7:
			connect_cache();
			break;
		case 8:
			set_key();
			break;
		case 9:
			get_key();
			break;
		case 10:
			get_fake_key();
			break;
		case 11:
			delete_key();
			break;
		case 12:
			nipc_handshake_test();
			break;
		case 13:
			printf("aca no hay nada\n");
			//multiplexor_test(listen_handshake);
			break;
		case 14:
			 serial_getattr();
			 break;
		case 15:
			 serial_readdir();
			 break;
		case 16:
			serial_open();
			break;
		case 17:
			 serial_release();
			break;
		case 18:
			 serial_read();
			 break;
		case 19:
			 serial_write();
			 break;
		case 20:
			serial_create();
			break;
		case 21:
		    serial_unlink();
		    break;
		case 22:
		    serial_truncate();
		    break;
		case 23:
			serial_rmdir();
			break;
		case 24:
			serial_mkdir();
			break;
		case 25:
			EliminarPath();
			break;
		case 26:
			agregarPathVector();
			break;
		}

		scanf("%d", &option);
	}

	return(EXIT_SUCCESS);
}
