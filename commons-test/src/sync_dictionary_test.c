/*
 * sync_dictionary_test.c
 *
 *  Created on: Jun 27, 2012
 *      Author: faloi
 */

#include <commons/thread.h>
#include <commons/syntax_sugars.h>
#include <stdlib.h>
#include "test.h"
#include <stdio.h>
#include "commons/collections/sync_dictionary.h"

void addToDictionary(thread_parameter* param) {
	char* key = malloc(2 + 1);
	sprintf(key, "%d", param->tid);

	sync_dictionary_put(param->parameter, key, key);
}

void printDictionary(thread_parameter* param) {
	char* key = malloc(2 + 1);
	sprintf(key, "%d", param->tid);

	char* value = sync_dictionary_get(param->parameter, key);

	puts(value);
}

void thread_test(void* (*operation) (void*), void* parameter) {
	int i;

	for (i = 0; i <= 50; i++) {
		thread_parameter* param = new(thread_parameter);
		param->parameter = parameter;
		param->tid = i;
		thread_create(operation, param);
	}
}
