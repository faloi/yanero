/*
 * nipc_test.c
 *
 *  Created on: 23/06/2012
 *      Author: utnso
 */
#include <commons/socket.h>
#include <stdio.h>
#include <commons/syntax_sugars.h>
#include <stdlib.h>
#include <commons/fuse/packages.h>
#include <commons/fuse/serializers.h>
#include <commons/fuse/packages_utils.h>

#define NIPC_STRING "Testeando NIPC..."
#define SERVER_PORT 55555
#define SERVER_IP "186.58.18.165"

void nipc_handshake_test() {
	t_paquete* package = new(t_paquete);
	package->type = HANDSHAKE;
	package->payload_len = 0;
	package->payload = NULL;

	t_stream* stream = serial_paquete(package);

	t_socket* client = socket_createClient();
	socket_connect(client, "127.0.0.1", SERVER_PORT);
	socket_send(client, stream->data, stream->length);

	t_paquete* response = socket_recvNipc(client);
	if (response->type == HANDSHAKE_RESPONSE)
		puts("Recibimos la respuesta del handshake!");
	else
		puts("No anda una mierda... :(");

	socket_destroy(client);
}

void nipc_client_test() {
	t_pedido_payload_create* pedido = new(t_pedido_payload_create);
	pedido->path = "/home/utnso/verogato/ext2.disk";

	t_paquete* package = serial_pedido_create(pedido);
	t_stream* stream = serial_paquete(package);

	t_socket* client = socket_createClient();
	socket_connect(client, SERVER_IP, SERVER_PORT);
	socket_send(client, stream->data, stream->length);
	socket_destroy(client);
}
