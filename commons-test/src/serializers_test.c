/*
 * serializers.c
 *
 *  Created on: 13/07/2012
 *      Author: vero
 */

#include <commons/fuse/packages.h>
#include <commons/fuse/serializers.h>
#include <commons/fuse/packages_utils.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define STATUS_SIZE 3
#define MODE_SIZE

static char *status_names[STATUS_SIZE] = {
		"OPERATION_ERROR",
		"OPERATION_FILE_NOT_FOUND",
		"OPERATION_OK"};

static char *mode_names[MODE_SIZE] =  {
	"REGULAR_FILE" ,
	"DIRECTORY"
};

char* status_to_string(enum statusCodes type) {
	return status_names[type];}

char* mode_to_string(enum getattrMode type) {
	return mode_names[type];}

void serial_getattr(){
	   //pedido getattr anda
		t_pedido_payload_getattr *pedido_payload = malloc(sizeof(t_pedido_payload_getattr));
		pedido_payload->path = strdup("esto es un path");

		t_paquete *pedido= serial_pedido_getattr(pedido_payload);
		t_stream  *stream = serial_paquete(pedido);
		t_paquete *des_pedido = deserial_paquete(stream);
		t_pedido_payload_getattr *des_pay_pedido = deserial_pedido_getattr(des_pedido);

		printf(" el path es :%s\n",des_pay_pedido->path);
		destroy_pedidoPayloadGetattr(pedido_payload);
		destroy_pedidoPayloadGetattr(des_pay_pedido);
		destroy_stream(stream);
		destroy_packageNipc(pedido);
		destroy_packageNipc(des_pedido);

		//respuesta getattr
		t_respuesta_payload_getattr *respuesta_payload = malloc(sizeof(t_respuesta_payload_getattr));
 		respuesta_payload->status = OPERATION_OK;
		respuesta_payload->size=111;
		respuesta_payload->mode= REGULAR_FILE;

		t_paquete *respuesta= serial_respuesta_getattr(respuesta_payload);
		t_stream  *stream_r = serial_paquete(respuesta);
		t_paquete *des_respuesta = deserial_paquete(stream_r);
		t_respuesta_payload_getattr *des_pay_respuesta = deserial_respuesta_getattr(des_respuesta);

		printf("Estado de la operación: es:%s\n",status_to_string(des_pay_respuesta->status));
		printf("Mode: %s\n",mode_to_string(des_pay_respuesta->mode));
		printf("Size: es:%d\n", des_pay_respuesta->size);

		destroy_respuestaPayloadGetAttr(respuesta_payload);
		destroy_packageNipc(respuesta);
		destroy_stream(stream_r);
		destroy_packageNipc(des_respuesta);
		destroy_respuestaPayloadGetAttr(des_pay_respuesta);

}

void serial_readdir(){
	  //pedido readdir anda
		t_pedido_payload_readdir *pedido_payload = malloc(sizeof(t_pedido_payload_readdir));
		pedido_payload->path =strdup("esto es un path");

		t_paquete *pedido= serial_pedido_readdir(pedido_payload);
		t_stream  *stream = serial_paquete(pedido);
		t_paquete *des_pedido = deserial_paquete(stream);
		t_pedido_payload_readdir *des_pay_pedido = deserial_pedido_readdir(des_pedido);

		printf(" El path es: %s\n",des_pay_pedido->path);

		destroy_pedidoPayloadReaddir(pedido_payload);
		destroy_packageNipc(pedido);
		destroy_stream(stream);
		destroy_packageNipc(des_pedido);
		destroy_pedidoPayloadReaddir(des_pay_pedido);

		//respuesta readdir
		t_respuesta_payload_readdir *respuesta_payload = malloc(sizeof(t_respuesta_payload_readdir));
		respuesta_payload->status = OPERATION_OK;
		respuesta_payload->cant_archivos=3;
		respuesta_payload->arr_archivos = malloc(3*sizeof(char*));
		respuesta_payload->arr_archivos[0]=strdup("hola");
		respuesta_payload->arr_archivos[1]=strdup("hol");
		respuesta_payload->arr_archivos[2]=strdup("ho");

		t_paquete *respuesta= serial_respuesta_readdir(respuesta_payload);
		t_stream  *stream_r = serial_paquete(respuesta);
		t_paquete *des_respuesta = deserial_paquete(stream_r);
		t_respuesta_payload_readdir *des_pay_respuesta = deserial_respuesta_readdir(des_respuesta);

		printf("Estado de la operacion: %s\n",status_to_string(des_pay_respuesta->status));

		int j;
		for (j = 0; j < des_pay_respuesta->cant_archivos; j++){
			printf("Archivo es : %s",des_pay_respuesta->arr_archivos[j]);
		}

		destroy_respuestaPayloadReaddir(respuesta_payload);
		destroy_packageNipc(respuesta);
		destroy_stream(stream_r);
		destroy_packageNipc(des_respuesta);
		destroy_respuestaPayloadReaddir(des_pay_respuesta);
}

void serial_open(){
	//pedido open
	t_pedido_payload_open *pedido_payload = malloc(sizeof(t_pedido_payload_open));
	pedido_payload->path = strdup("esto es un path");

	t_paquete *pedido= serial_pedido_open(pedido_payload);
	t_stream  *stream = serial_paquete(pedido);
	t_paquete *des_pedido = deserial_paquete(stream);
	t_pedido_payload_open *des_pay_pedido = deserial_pedido_open(des_pedido);

	printf(" El path es :%s\n",des_pay_pedido->path);

	destroy_pedidoPayloadOpen(pedido_payload);
	destroy_packageNipc(pedido);
	destroy_stream(stream);
	destroy_packageNipc(des_pedido);
	destroy_pedidoPayloadOpen(des_pay_pedido);

	//respuesta open anda
	t_respuesta_payload_open *respuesta_payload = malloc(sizeof(t_respuesta_payload_open));
	respuesta_payload->status = OPERATION_OK;

	t_paquete *respuesta= serial_respuesta_open(respuesta_payload);
	t_stream  *stream_r = serial_paquete(respuesta);
	t_paquete *des_respuesta = deserial_paquete(stream_r);
	t_respuesta_payload_open *des_pay_respuesta = deserial_respuesta_open(des_respuesta);

	printf("Estado de la operacion: %s\n",status_to_string(des_pay_respuesta->status));

	destroy_respuestaPayloadOpen(respuesta_payload);
	destroy_packageNipc(respuesta);
	destroy_stream(stream_r);
	destroy_packageNipc(des_respuesta);
	destroy_respuestaPayloadOpen(des_pay_respuesta);
}

void serial_release(){
	//pedido release
	t_pedido_payload_release *pedido_payload = malloc(sizeof(t_pedido_payload_release));
	pedido_payload->path = strdup("esto es un path");

	t_paquete *pedido= serial_pedido_release(pedido_payload);
	t_stream  *stream = serial_paquete(pedido);
	t_paquete *des_pedido = deserial_paquete(stream);
	t_pedido_payload_release *des_pay_pedido = deserial_pedido_release(des_pedido);

	printf(" El path es :%s\n",des_pay_pedido->path);

	destroy_pedidoPayloadRelease(pedido_payload);
	destroy_packageNipc(pedido);
	destroy_stream(stream);
	destroy_packageNipc(des_pedido);
	destroy_pedidoPayloadRelease(des_pay_pedido);

	//respuesta release
		t_respuesta_payload_release *respuesta_payload = malloc(sizeof(t_respuesta_payload_release));
		respuesta_payload->status = OPERATION_OK;

		t_paquete *respuesta= serial_respuesta_release(respuesta_payload);
		t_stream  *stream_r = serial_paquete(respuesta);
		t_paquete *des_respuesta = deserial_paquete(stream_r);
		t_respuesta_payload_release *des_pay_respuesta = deserial_respuesta_release(des_respuesta);

		printf("Estado de la operacion: %s\n",status_to_string(des_pay_respuesta->status));
		destroy_respuestaPayloadRelease(respuesta_payload);
		destroy_packageNipc(respuesta);
		destroy_stream(stream_r);
		destroy_packageNipc(des_respuesta);
		destroy_respuestaPayloadRelease(des_pay_respuesta);

}

void serial_read(){

	//pedido read anda
	t_pedido_payload_read *pedido_payload = malloc(sizeof(t_pedido_payload_read));
	pedido_payload->path = strdup("esto es un path");
	pedido_payload->offset=0;
	pedido_payload->size=2134567;

	t_paquete *pedido= serial_pedido_read(pedido_payload);
	t_stream  *stream = serial_paquete(pedido);
	t_paquete *des_pedido = deserial_paquete(stream);
	t_pedido_payload_read *des_pay_pedido = deserial_pedido_read(des_pedido);

	printf("Path: %s\n",des_pay_pedido->path);
	printf("offset: %d\n",des_pay_pedido->offset);
	printf("size:%d\n",des_pay_pedido->size);

	destroy_pedidoPayloadRead(pedido_payload);
	destroy_packageNipc(pedido);
	destroy_stream(stream);
	destroy_packageNipc(des_pedido);
	destroy_pedidoPayloadRead(des_pay_pedido);

	//respuesta read anda
	t_respuesta_payload_read *respuesta_payload = malloc(sizeof(t_respuesta_payload_read));
	respuesta_payload->status = OPERATION_OK;
	respuesta_payload->content=strdup("contenido delsjhshkjshksjhsjhsjkshkjshskjhsjshjshjshskjhs archivo00000000000000000000/0ikdjlwjddlkjdwkldjklejdkejdhjfhfhhfhfhfhhfhfhfhhfhfjfhjfhjfhjfhfjhfjfhjfhfjhfjfhjfhjfhfjhfjfhjfhjfhfjhfjfhjfhjfhfjhfjfhjfhjfhfjhfjfhjfhfjhfjfhjfilejdoiejidjhdjkdahksdjhjaksdhjakshdkjsahskjdhjshajhsjahskjdhakjhdskjahdjashkshjahdskjahkjsdhkajhdskjhakjdhsjahkjashdjahkjshjwhjwhjshkasjhdajshdjkshdkjahdskjhaskjdhkjsahkjsdhksajhdkjsakmkksmdkjsdkjdksdjksdjkdshdkajsdskhjksahdskjahsskj");
	respuesta_payload->size=sizeof(respuesta_payload->content);

	t_paquete *respuesta= serial_respuesta_read(respuesta_payload);
	t_stream  *stream_r = serial_paquete(respuesta);
	t_paquete *des_respuesta = deserial_paquete(stream_r);
	t_respuesta_payload_read *des_pay_respuesta = deserial_respuesta_read(des_respuesta);

	printf("Estado de la operación:%s\n",status_to_string(des_pay_respuesta->status));
	printf("Content: %s\n",des_pay_respuesta->content);
	printf(" Size:%d\n",des_pay_respuesta->size);

	destroy_respuestaPayloadRead(respuesta_payload);
	destroy_packageNipc(respuesta);
	destroy_stream(stream_r);
	destroy_packageNipc(des_respuesta);
	destroy_respuestaPayloadRead(des_pay_respuesta);

}

void serial_write(){
	//pedido write NO anda
	t_pedido_payload_write *pedido_payload = malloc(sizeof(t_pedido_payload_write));
	pedido_payload->offset=111;
	pedido_payload->size=111;
	pedido_payload->path = strdup("esto es un path");
	pedido_payload->content =strdup("contenido a grabar");


	t_paquete *pedido= serial_pedido_write(pedido_payload);
	t_stream  *stream = serial_paquete(pedido);
	t_paquete *des_pedido = deserial_paquete(stream);
	t_pedido_payload_write *des_pay_pedido = deserial_pedido_write(des_pedido);

	printf(" Path:%s\n",des_pay_pedido->path);
	printf(" Offset: %d\n",des_pay_pedido->offset);
	printf(" Size: %d\n",des_pay_pedido->size);
	printf(" Content: %s\n",des_pay_pedido->content);

	destroy_pedidoPayloadWrite(pedido_payload);
	destroy_packageNipc(pedido);
	destroy_stream(stream);
	destroy_packageNipc(des_pedido);
	destroy_pedidoPayloadWrite(des_pay_pedido);

  //respuesta write anda
	t_respuesta_payload_write *respuesta_payload = malloc(sizeof(t_respuesta_payload_write));
	respuesta_payload->status = OPERATION_OK;
	respuesta_payload->requested= 111;

	t_paquete *respuesta= serial_respuesta_write(respuesta_payload);
	t_stream  *stream_r = serial_paquete(respuesta);
	t_paquete *des_respuesta = deserial_paquete(stream_r);
	t_respuesta_payload_write *des_pay_respuesta = deserial_respuesta_write(des_respuesta);

	printf("Estado de la operación:%s\n",status_to_string(des_pay_respuesta->status));
	printf("Requested:%d\n",des_pay_respuesta->requested);

	destroy_respuestaPayloadWrite(respuesta_payload);
	destroy_packageNipc(respuesta);
	destroy_stream(stream_r);
	destroy_packageNipc(des_respuesta);
	destroy_respuestaPayloadWrite(des_pay_respuesta);
}

void serial_create(){
	//pedido create anda
		t_pedido_payload_create *pedido_payload = malloc(sizeof(t_pedido_payload_create));
		pedido_payload->path = strdup("esto es un path");

		t_paquete *pedido= serial_pedido_create(pedido_payload);
		t_stream  *stream = serial_paquete(pedido);
		t_paquete *des_pedido = deserial_paquete(stream);
		t_pedido_payload_create *des_pay_pedido = deserial_pedido_create(des_pedido);

		printf("Path: %s\n",des_pay_pedido->path);

		destroy_pedidoPayloadCreate(pedido_payload);
		destroy_packageNipc(pedido);
		destroy_stream(stream);
		destroy_packageNipc(des_pedido);
		destroy_pedidoPayloadCreate(des_pay_pedido);

		//respuesta create
		t_respuesta_payload_create *respuesta_payload = malloc(sizeof(t_respuesta_payload_create));
		respuesta_payload->status = OPERATION_OK;

		t_paquete *respuesta= serial_respuesta_create(respuesta_payload);
		t_stream  *stream_r = serial_paquete(respuesta);
		t_paquete *des_respuesta = deserial_paquete(stream_r);
		t_respuesta_payload_create *des_pay_respuesta = deserial_respuesta_create(des_respuesta);

		printf("Estado de la operación: %s\n",status_to_string(des_pay_respuesta->status));

		destroy_respuestaPayloadCreate(respuesta_payload);
		destroy_packageNipc(respuesta);
		destroy_stream(stream_r);
		destroy_packageNipc(des_respuesta);
		destroy_respuestaPayloadCreate(des_pay_respuesta);

}

void serial_unlink(){
	//pedido unlink anda
			t_pedido_payload_unlink *pedido_payload = malloc(sizeof(t_pedido_payload_unlink));
			pedido_payload->path = strdup("esto es un path");

			t_paquete *pedido= serial_pedido_unlink(pedido_payload);
			t_stream  *stream = serial_paquete(pedido);
			t_paquete *des_pedido = deserial_paquete(stream);
			t_pedido_payload_unlink *des_pay_pedido = deserial_pedido_unlink(des_pedido);

			printf("Path: %s\n",des_pay_pedido->path);

			destroy_pedidoPayloadUnlink(pedido_payload);
			destroy_packageNipc(pedido);
			destroy_stream(stream);
			destroy_packageNipc(des_pedido);
			destroy_pedidoPayloadUnlink(des_pay_pedido);

			//respuesta unlink anda
			t_respuesta_payload_truncate *respuesta_payload = malloc(sizeof(t_respuesta_payload_unlink));
			respuesta_payload->status = OPERATION_OK;

			t_paquete *respuesta= serial_respuesta_truncate(respuesta_payload);
			t_stream  *stream_r = serial_paquete(respuesta);
			t_paquete *des_respuesta = deserial_paquete(stream_r);
			t_respuesta_payload_unlink *des_pay_respuesta = deserial_respuesta_unlink(des_respuesta);

			printf("%s\n",status_to_string(des_pay_respuesta->status));

			destroy_respuestaPayloadUnlink(respuesta_payload);
			destroy_packageNipc(respuesta);
			destroy_stream(stream_r);
			destroy_packageNipc(des_respuesta);
			destroy_respuestaPayloadUnlink(des_pay_respuesta);

}

void serial_truncate(){
	//pedido truncate anda
		t_pedido_payload_truncate *pedido_payload = malloc(sizeof(t_pedido_payload_truncate));
		pedido_payload->path = strdup("esto es un path");
		pedido_payload->size=111;

		t_paquete *pedido= serial_pedido_truncate(pedido_payload);
		t_stream  *stream = serial_paquete(pedido);
		t_paquete *des_pedido = deserial_paquete(stream);
		t_pedido_payload_truncate *des_pay_pedido = deserial_pedido_truncate(des_pedido);

		printf("Path: %s\n",des_pay_pedido->path);
		printf("Size: %d\n",des_pay_pedido->size);

		destroy_pedidoPayloadTruncate(pedido_payload);
		destroy_packageNipc(pedido);
		destroy_stream(stream);
		destroy_packageNipc(des_pedido);
		destroy_pedidoPayloadTruncate(des_pay_pedido);

		//respuesta truncate anda
		t_respuesta_payload_truncate *respuesta_payload = malloc(sizeof(t_respuesta_payload_truncate));
		respuesta_payload->status = OPERATION_OK;

		t_paquete *respuesta= serial_respuesta_truncate(respuesta_payload);
		t_stream  *stream_r = serial_paquete(respuesta);
		t_paquete *des_respuesta = deserial_paquete(stream_r);
		t_respuesta_payload_truncate *des_pay_respuesta = deserial_respuesta_truncate(des_respuesta);

		printf("Estado de la operación:%s\n",status_to_string(des_pay_respuesta->status));

		destroy_respuestaPayloadTruncate(respuesta_payload);
		destroy_packageNipc(respuesta);
		destroy_stream(stream_r);
		destroy_packageNipc(des_respuesta);
		destroy_respuestaPayloadTruncate(des_pay_respuesta);


}

void serial_rmdir(){
	//pedido rmdir anda
			t_pedido_payload_rmdir *pedido_payload = malloc(sizeof(t_pedido_payload_rmdir));
			pedido_payload->path = strdup("esto es un path");

			t_paquete *pedido= serial_pedido_rmdir(pedido_payload);
			t_stream  *stream = serial_paquete(pedido);
			t_paquete *des_pedido = deserial_paquete(stream);
			t_pedido_payload_rmdir *des_pay_pedido = deserial_pedido_rmdir(des_pedido);


			printf("Path: %s\n",des_pay_pedido->path);

			destroy_pedidoPayloadRmdir(pedido_payload);
			destroy_packageNipc(pedido);
			destroy_stream(stream);
			destroy_packageNipc(des_pedido);
			destroy_pedidoPayloadRmdir(des_pay_pedido);

			//respuesta rmdir nda
			t_respuesta_payload_rmdir *respuesta_payload = malloc(sizeof(t_respuesta_payload_rmdir));
			respuesta_payload->status = OPERATION_OK;

			t_paquete *respuesta= serial_respuesta_rmdir(respuesta_payload);
			t_stream  *stream_r = serial_paquete(respuesta);
			t_paquete *des_respuesta = deserial_paquete(stream_r);
			t_respuesta_payload_rmdir *des_pay_respuesta = deserial_respuesta_rmdir(des_respuesta);

			printf("%s\n",status_to_string(des_pay_respuesta->status));

			destroy_respuestaPayloadRmdir(respuesta_payload);
			destroy_packageNipc(respuesta);
			destroy_stream(stream_r);
			destroy_packageNipc(des_respuesta);
			destroy_respuestaPayloadRmdir(des_pay_respuesta);
}
void serial_mkdir(){
	//pedido mkdir anda
			t_pedido_payload_mkdir *pedido_payload = malloc(sizeof(t_pedido_payload_mkdir));
			pedido_payload->path = strdup("esto es un path");

			t_paquete *pedido= serial_pedido_mkdir(pedido_payload);
			t_stream  *stream = serial_paquete(pedido);
			t_paquete *des_pedido = deserial_paquete(stream);
			t_pedido_payload_mkdir *des_pay_pedido = deserial_pedido_mkdir(des_pedido);

			printf("Path: %s\n",des_pay_pedido->path);


			destroy_pedidoPayloadMkdir(pedido_payload);
			destroy_packageNipc(pedido);
			destroy_stream(stream);
			destroy_packageNipc(des_pedido);
			destroy_pedidoPayloadMkdir(des_pay_pedido);


			//respuesta mkdir nda
			t_respuesta_payload_mkdir *respuesta_payload = malloc(sizeof(t_respuesta_payload_mkdir));
			respuesta_payload->status = OPERATION_OK;

			t_paquete *respuesta= serial_respuesta_mkdir(respuesta_payload);
			t_stream  *stream_r = serial_paquete(respuesta);
			t_paquete *des_respuesta = deserial_paquete(stream_r);
			t_respuesta_payload_mkdir *des_pay_respuesta = deserial_respuesta_mkdir(des_respuesta);

			printf("Estado de la operación:%s\n",status_to_string(des_pay_respuesta->status));

			destroy_respuestaPayloadMkdir(respuesta_payload);
			destroy_packageNipc(respuesta);
			destroy_stream(stream_r);
			destroy_packageNipc(des_respuesta);
			destroy_respuestaPayloadMkdir(des_pay_respuesta);
}

void EliminarPath() {

	t_respuesta_payload_readdir *respuesta_payload = malloc(
			sizeof(t_respuesta_payload_readdir));
	respuesta_payload->status = OPERATION_OK;
	respuesta_payload->cant_archivos = 6;
	respuesta_payload->arr_archivos = malloc(6 * sizeof(char*));
	respuesta_payload->arr_archivos[0] = strdup("vero");
	respuesta_payload->arr_archivos[1] = strdup("natalia");
	respuesta_payload->arr_archivos[2] = strdup("fabbro");
	respuesta_payload->arr_archivos[3] = strdup("universidad");
	respuesta_payload->arr_archivos[4] = strdup("tecnologica");
	respuesta_payload->arr_archivos[5] = strdup("nacional");

	int j = 0, compare, h;

	do {
		compare = strcmp(respuesta_payload->arr_archivos[j], "vero");
		j++;
	} while (compare != 0 && j < respuesta_payload->cant_archivos);
		if (j == 1 && respuesta_payload->cant_archivos == 1) {
			free(respuesta_payload->arr_archivos[(j-1)]);
			respuesta_payload->cant_archivos--;
		} else {
			for (h = (j - 1); h < respuesta_payload->cant_archivos; h++) {
				if (h != (respuesta_payload->cant_archivos - 1)) {
					free(respuesta_payload->arr_archivos[h]);
					respuesta_payload->arr_archivos[h] = strdup(
							respuesta_payload->arr_archivos[h + 1]);
				} else {
					free(respuesta_payload->arr_archivos[h]);
				}
			}
			respuesta_payload->cant_archivos--;
		}
	int x;
	for (x = 0; x < respuesta_payload->cant_archivos; x++) {
		printf("Archivo es : %s\n", respuesta_payload->arr_archivos[x]);
	}

	destroy_respuestaPayloadReaddir(respuesta_payload);

}

void agregarPathVector(){
	t_respuesta_payload_readdir *respuesta_payload = malloc(
			sizeof(t_respuesta_payload_readdir));
	respuesta_payload->status = OPERATION_OK;
	respuesta_payload->cant_archivos = 6;
	respuesta_payload->arr_archivos = malloc(6 * sizeof(char*));
	respuesta_payload->arr_archivos[0] = strdup("vero");
	respuesta_payload->arr_archivos[1] = strdup("natalia");
	respuesta_payload->arr_archivos[2] = strdup("fabbro");
	respuesta_payload->arr_archivos[3] = strdup("universidad");
	respuesta_payload->arr_archivos[4] = strdup("tecnologica");
	respuesta_payload->arr_archivos[5] = strdup("nacional");

	respuesta_payload->arr_archivos= realloc(respuesta_payload->arr_archivos, 7 * sizeof(char*));

	respuesta_payload->arr_archivos[respuesta_payload->cant_archivos]= strdup("pathnuevo");
	respuesta_payload->cant_archivos++;

	int x;
	for (x = 0; x < respuesta_payload->cant_archivos; x++) {
		printf("Archivo es : %s\n", respuesta_payload->arr_archivos[x]);
	}

	destroy_respuestaPayloadReaddir(respuesta_payload);

}
