/*
 ============================================================================
 Name        : sockets.c
 Author      : faloi
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <commons/socket.h>
#include <commons/multiplexor.h>
#include <commons/syntax_sugars.h>
#include <commons/collections/dictionary.h>
#include <commons/fuse/serializers.h>

#define MAXEVENTS 64
#define SERVER_PORT 55555



void listen_nipc(t_socket* client) {
	t_paquete* package = socket_recvNipc(client);
	t_pedido_payload_create* pedido = deserial_pedido_create(package);

	printf("Socket: %d\n", client->desc);
	printf("Path: %s\n", pedido->path);
}

void listen_handshake(t_socket* client) {
	t_paquete* package = socket_recvNipc(client);
	if (package->type == HANDSHAKE) {
		puts("Recibido el handshake!");

		t_paquete* paquete_handshake = new(t_paquete);
		paquete_handshake->type = HANDSHAKE_RESPONSE;
		t_stream* stream_handshake= serial_paquete(paquete_handshake);

		socket_send(client, stream_handshake->data, stream_handshake->length);
	}

}


