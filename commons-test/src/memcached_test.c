/*
 * memcached_test.c
 *
 *  Created on: 02/07/2012
 *      Author: vero
 */

#include <stdio.h>
#include <stdlib.h>
#include <commons/cache_utils.h>
#include <stdio.h>
#include <commons/fuse/packages.h>
#include <commons/syntax_sugars.h>

#define DATA "12345"
#define PATH "/home/utnso/a.txt"

void connect_cache() {
	memcached_return ret = cache_create("127.0.0.1", 11212, 5);
	if (ret == MEMCACHED_SUCCESS)
		printf("salio todo piola el connect\n");
}
void set_key() {
	t_paquete* package = new(t_paquete);
	package->payload = DATA;
	package->payload_len = strlen(DATA);
	package->type = READ_DIRECTORY;

	memcached_return ret_set = cache_set_operation(PATH, package);

	if (ret_set == MEMCACHED_SUCCESS)
		printf("bien!guardaste algo\n");
}

void get_key() {
	t_paquete* package = cache_get_operation(READ_DIRECTORY, PATH);
	if (package == NULL)
		puts("No se encontro el valor...");
	else
		printf("el value es: %s\n", package->payload);
}

void get_fake_key() {
	t_paquete* package = cache_get_operation(READ, PATH);
	if (package == NULL)
		puts("No se encontro el valor...");
	else
		printf("el value es: %s\n", package->payload);
}

void delete_key() {
	if (cache_delete_operation(READ_DIRECTORY, PATH) == MEMCACHED_SUCCESS)
		puts("Clave borrada con exito!");
	else
		puts("No se encontro el valor...");
}
