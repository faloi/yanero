/*
 * test.h
 *
 *  Created on: Jun 27, 2012
 *      Author: faloi
 */

#ifndef TEST_H_
#define TEST_H_
#include <commons/socket.h>

	typedef struct {
		void* 	parameter;
		int 	tid;
	} thread_parameter;

	void nipc_client_test();
	void multiplexor_test(void (*client_callback)(t_socket*));
	void listen_string(t_socket* client);
	void listen_nipc(t_socket* client);
	void listen_handshake(t_socket* client);

	void printDictionary(thread_parameter* param);
	void addToDictionary(thread_parameter* param);

	void connect_cache();
	void set_key();
	void get_key();
	void get_fake_key();
	void delete_key();

	void nipc_handshake_test();

	void serial_getattr();
	void serial_readdir();
	void serial_open();
	void serial_release();
	void serial_read();
	void serial_write();
	void serial_create();
	void serial_unlink();
	void serial_truncate();
	void serial_rmdir();
	void serial_mkdir();

	void pruebaPathAbsoluto();
	void EliminarPath();
	void agregarPathVector();


#endif /* TEST_H_ */
