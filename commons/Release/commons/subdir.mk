################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../commons/bitarray.c \
../commons/cache_utils.c \
../commons/config.c \
../commons/error.c \
../commons/global_log.c \
../commons/log.c \
../commons/math.c \
../commons/multiplexor.c \
../commons/socket.c \
../commons/string.c \
../commons/temporal.c \
../commons/thread.c 

OBJS += \
./commons/bitarray.o \
./commons/cache_utils.o \
./commons/config.o \
./commons/error.o \
./commons/global_log.o \
./commons/log.o \
./commons/math.o \
./commons/multiplexor.o \
./commons/socket.o \
./commons/string.o \
./commons/temporal.o \
./commons/thread.o 

C_DEPS += \
./commons/bitarray.d \
./commons/cache_utils.d \
./commons/config.d \
./commons/error.d \
./commons/global_log.d \
./commons/log.d \
./commons/math.d \
./commons/multiplexor.d \
./commons/socket.d \
./commons/string.d \
./commons/temporal.d \
./commons/thread.d 


# Each subdirectory must supply rules for building sources it contributes
commons/%.o: ../commons/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


