/*
 * thread.h
 *
 *  Created on: Jun 5, 2012
 *      Author: faloi
 */

#ifndef THREAD_H_
#define THREAD_H_

	void 		thread_create(void* (*operation) (void*), void* parameter);

#endif /* THREAD_H_ */
