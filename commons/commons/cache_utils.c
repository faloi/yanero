/*
 * memcached_utils.c
 *
 *  Created on: 28/06/2012
 *      Author: vero
 */

#include <libmemcached/memcached.h>
#include <stdio.h>
#include "cache_utils.h"
#include "syntax_sugars.h"
#include <stdlib.h>
#include "collections/sync_queue.h"

static t_sync_queue* connections;
static int ext2_block_size;

// ----------------------------------------------------------
//						PRIVATE HELPERS
// ----------------------------------------------------------

static memcached_st* cache_get_instance() {
	return sync_queue_pop(connections);
}

static void cache_release(memcached_st* cache) {
	sync_queue_push(connections, cache);
}

static char* make_operation_key(fuseOperationsType operation, const char* path) {
	char* key = malloc(strlen(path) + 3);
	sprintf(key, "%d%s", operation, path);

	return key;
}

static char* make_block_key(int block_number) {
	char* key = malloc(1 + 7);
	sprintf(key, "b%d", block_number);

	return key;
}

static memcached_return cache_set(const char* key, char* value, size_t value_size) {
	memcached_st* cache = cache_get_instance();

	memcached_return ret_value = memcached_set(cache, key, strlen(key), value, value_size, (time_t)0, (uint32_t)0);
	cache_release(cache);

	return ret_value;
}

static memcached_return cache_get(const char* key, void** value, size_t* value_len){
	memcached_st* cache = cache_get_instance();
	memcached_return status_code;
	uint32_t flags;

	*value = memcached_get(cache, key, strlen(key), value_len, &flags, &status_code);
	cache_release(cache);

	return status_code;
}

static memcached_return cache_delete(const char* key) {
	memcached_st* cache = cache_get_instance();

	memcached_return ret_value = memcached_delete(cache, key, strlen(key), (time_t)0);
	cache_release(cache);

	return ret_value;
}

// ----------------------------------------------------------
//						PUBLIC METHODS
// ----------------------------------------------------------

memcached_return cache_create(const char* ip, int port, int max_instances) {
	memcached_server_st *server = NULL;
	memcached_return ret_cache;

	connections = sync_queue_create();
	server = memcached_server_list_append(server, ip, port, &ret_cache);

	int i;
	for (i = 0; i < max_instances; i++) {
		memcached_st* cache = memcached_create(NULL);
		ret_cache = memcached_server_push(cache, server);
		sync_queue_push(connections, cache);
	}

	return ret_cache;
}

void cache_set_block_size(int block_size) {
	ext2_block_size = block_size;
}

memcached_return cache_set_block(int block_number, char* data) {
	char* key = make_block_key(block_number);
	memcached_return status_code = cache_set(key, data, ext2_block_size);
	free(key);

	return status_code;
}

char* cache_get_block(int block_number) {
	char* block = malloc(ext2_block_size);

	char* key = make_block_key(block_number);
	memcached_return status_code = cache_get(key, &block, &block_number);
	free(key);

	return status_code == MEMCACHED_SUCCESS ? block : NULL;

}

memcached_return cache_delete_block(int block_number) {
	char* key = make_block_key(block_number);
	memcached_return status_code = cache_delete(key);
	free(key);

	return status_code;
}

memcached_return cache_set_operation(const char* path, t_paquete* package) {
	char* key = make_operation_key(package->type, path);
	memcached_return status_code = cache_set(key, package->payload, package->payload_len);
	free(key);

	return status_code;
}

t_paquete* cache_get_operation(fuseOperationsType operation, const char* path){
	t_paquete* package = new(t_paquete);

	char* key = make_operation_key(operation, path);
	memcached_return status_code = cache_get(key, &package->payload, &package->payload_len);
	free(key);

	return status_code == MEMCACHED_SUCCESS? package : NULL;
}

memcached_return cache_delete_operation(fuseOperationsType operation, const char* path) {
	char* key = make_operation_key(operation, path);
	memcached_return status_code = cache_delete(key);
	free(key);

	return status_code;
}

void cache_destroy() {
	memcached_server_list_free(cache_get_instance()->servers);
	memcached_free(cache_get_instance());
}
