/*
 * memcached_utils.h
 *
 *  Created on: 28/06/2012
 *      Author: vero
 */

#ifndef CACHE_UTILS_H_
#define CACHE_UTILS_H_

	#include <libmemcached/memcached.h>
	#include <stddef.h>
	#include "fuse/packages.h"

	memcached_return 	cache_create(const char* ip, int port, int max_instances);
	void 				cache_set_block_size(int block_size);

	memcached_return 	cache_set_block(int block_number, char* data);
	char* 				cache_get_block(int block_number);
	memcached_return 	cache_delete_block(int block_number);

	memcached_return 	cache_set_operation(const char* path, t_paquete* package);
	t_paquete* 			cache_get_operation(fuseOperationsType operation, const char* path);
	memcached_return 	cache_delete_operation(fuseOperationsType operation, const char* path);

	void 				cache_destroy();

#endif /* CACHE_H_ */
