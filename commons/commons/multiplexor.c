/*
 * multiplexor.c
 *
 *  Created on: Jun 20, 2012
 *      Author: faloi
 */

#include "multiplexor.h"
#include <sys/epoll.h>
#include "syntax_sugars.h"
#include <stdlib.h>
#include <stdio.h>
#include "collections/dictionary.h"
#include <sys/inotify.h>

// El tamaño de un evento es igual al tamaño de la estructura de inotify
// mas el tamaño maximo de nombre de archivo que nosotros soportemos
// en este caso el tamaño de nombre maximo que vamos a manejar es de 24
// caracteres. Esto es porque la estructura inotify_event tiene un array
// sin dimension ( Ver C-Talks I - ANSI C ).
#define EVENT_SIZE  ( sizeof (struct inotify_event) + 250 )

// El tamaño del buffer es igual a la cantidad maxima de eventos simultaneos
// que quiero manejar por el tamaño de cada uno de los eventos. En este caso
// Puedo manejar hasta 1024 eventos simultaneos.
#define BUF_LEN     ( 1024 * EVENT_SIZE )

#define MAXEVENTS 64

// ----------------------------------------------------------
//						PRIVATE HELPERS
// ----------------------------------------------------------
static int multiplexor_add_fd(t_multiplexor* self, int fd) {
	struct epoll_event event;

	event.data.fd = fd;
	event.events = EPOLLIN | EPOLLET;

	return epoll_ctl(self->descriptor, EPOLL_CTL_ADD, fd,	&event);
}

static void multiplexor_add_socket(t_multiplexor* self, t_socket* socket) {
	int s = multiplexor_add_fd(self, socket->desc);
	if (s == -1) {
		perror("No se pudo agregar el socket al epoll");
		abort();
	}
}

static char* int_to_string(int number) {
	char* key = malloc(5);
	sprintf(key, "%d", number);

	return key;
}


// ----------------------------------------------------------
//						PUBLIC METHODS
// ----------------------------------------------------------

/*
 * @NAME: multiplexor_create
 * @DESC: Crea un contenedor de sockets
 */
t_multiplexor* multiplexor_create(char* ip, int port) {
	int epoll_descriptor = epoll_create1(0);
	if (epoll_descriptor == -1) {
		perror("No se pudo crear el multiplexor");
		return NULL;
	}

	t_socket* server = socket_createServer(ip, port);
	if (server == NULL) return NULL;

	socket_listen(server);

	t_multiplexor* self = new(t_multiplexor);
	self->descriptor = epoll_descriptor;
	self->server = server;
	self->clients = dictionary_create(socket_destroy);
	self->events = calloc(MAXEVENTS, sizeof(struct epoll_event));

	multiplexor_add_socket(self, server);

	return self;
}

/*
 * @NAME: multiplexor_add_client
 * @DESC: Agrega un nuevo cliente a la lista de sockets
 */
void multiplexor_add_client(t_multiplexor* self, t_socket* client) {
	multiplexor_add_socket(self, client);
	dictionary_put(self->clients, int_to_string(client->desc) , client);
}

/*
 * @NAME: multiplexor_add_inotify
 * @DESC: Agrega al multiplexor el monitoreo de un archivo mediante inotify
 */
void multiplexor_add_inotify(t_multiplexor* self, char* file_path) {
	int file_descriptor = inotify_init();
	inotify_add_watch(file_descriptor, file_path,
			IN_MODIFY | IN_CREATE | IN_DELETE);

	multiplexor_add_fd(self, file_descriptor);

	self->inotify_fd = file_descriptor;
	self->inotify_path = strdup(file_path);
}

/*
 * @NAME: multiplexor_wait
 * @DESC: Monitorea los sockets y ejecuta una accion dependiendo del tipo,
 * si es server agrega un cliente a la lista y si es cliente ejecuta el
 * callback especificado
 */
void multiplexor_wait(t_multiplexor* self, int (*client_callback)(t_socket*), void (*new_connection_callback)(t_socket*), void (*inotify_callback)(char*)) {
	int connectionsCount = epoll_wait(self->descriptor, self->events, MAXEVENTS, -1);

	int i;
	for (i = 0; i < connectionsCount; i++) {
		int descriptor = self->events[i].data.fd;

		if ((self->events[i].events & EPOLLERR)
				|| (self->events[i].events & EPOLLHUP)
				|| (!(self->events[i].events & EPOLLIN))) {
			/* An error has occured on this fd, or the socket is not
			 ready for reading (why were we notified then?) */
			close(descriptor);
		} else {
			if (descriptor == self->server->desc) {
				t_socket* client = socket_accept(self->server);
				if (client != NULL) {
					multiplexor_add_client(self, client);
					new_connection_callback(client);
				}
			} else if (descriptor == self->inotify_fd) {
				char buffer[BUF_LEN];
				int length = read(self->inotify_fd, buffer, BUF_LEN);

				int offset = 0;

				while (offset < length) {
					struct inotify_event *event = (struct inotify_event *) &buffer[offset];
					if (event->mask & IN_MODIFY)
						inotify_callback(self->inotify_path);

					offset += sizeof (struct inotify_event) + event->len;
				}
			} else {
				t_socket* client = dictionary_get(self->clients, int_to_string(descriptor));
				int ret = client_callback(client);

				if (ret == -1) {
					dictionary_remove(self->clients, int_to_string(descriptor));
					close(descriptor);
				}
			}
		}
	}
}

/*
 * @NAME: multiplexor_destroy
 * @DESC: Destruye la estructura y cierra los sockets
 */
void multiplexor_destroy(t_multiplexor* self) {
	socket_destroy(self->server);
	dictionary_destroy(self->clients);
	free(self->inotify_path);
	free(self);
}
