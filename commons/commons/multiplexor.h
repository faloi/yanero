/*
 * multiplexor.h
 *
 *  Created on: Jun 20, 2012
 *      Author: faloi
 */

#ifndef MULTIPLEXOR_H_
#define MULTIPLEXOR_H_

	#include "socket.h"
	#include "collections/dictionary.h"

	typedef struct {
		int						descriptor;
		t_socket*				server;
		t_dictionary*			clients;
		int 					inotify_fd;
		char*					inotify_path;
		struct epoll_event* 	events;
	} t_multiplexor;

	t_multiplexor*		multiplexor_create(char* ip, int port);
	void 				multiplexor_add_client(t_multiplexor* self, t_socket* client);
	void 				multiplexor_add_inotify(t_multiplexor* self, char* file_path);
	void 				multiplexor_wait(t_multiplexor* self, int (*client_callback)(t_socket*), void (*new_connection_callback)(t_socket*), void (*inotify_callback)(char*));
	void				multiplexor_destroy(t_multiplexor* self);

#endif /* MULTIPLEXOR_H_ */
