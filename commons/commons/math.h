/*
 * math.h
 *
 *  Created on: Jun 7, 2012
 *      Author: faloi
 */

#ifndef MATH_H_
#define MATH_H_

	int 	max(int x, int y);
	int 	min(int x, int y);

#endif /* MATH_H_ */
