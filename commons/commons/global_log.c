/*
 * global_log.c
 *
 *  Created on: Jun 26, 2012
 *      Author: faloi
 */

#ifndef LOG_MAX_LENGTH_MESSAGE
#define LOG_MAX_LENGTH_MESSAGE 1024
#endif

#define LOG_MAX_LENGTH_BUFFER LOG_MAX_LENGTH_MESSAGE + 100
#define LOG_ENUM_SIZE 5

#include <linux/stddef.h>
#include <stdbool.h>
#include <stdarg.h>
#include "log.h"
#include <stdarg.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>

t_log* global_logger;

t_log* global_log_get() {
	return global_logger;
}

static bool isEnableLevelInLogger(t_log* logger, t_log_level level) {
	return level >= logger->detail;
}

static void global_log_write_in_level(t_log_level level,
		const char* message_template, va_list list_arguments) {

	t_log* logger = global_log_get();

	if (isEnableLevelInLogger(logger, level)) {
		char *message, *time, *buffer;
		unsigned int thread_id;

		message = malloc(LOG_MAX_LENGTH_MESSAGE + 1);
		vsprintf(message, message_template, list_arguments);
		time = temporal_get_string_time();
		thread_id = pthread_self();

		buffer = malloc(LOG_MAX_LENGTH_BUFFER + 1);
		sprintf(buffer, "[%s] %s %s/(%d:%d): %s\n", log_level_as_string(level),
				time, logger->program_name, logger->pid, thread_id, message);

		if (logger->file != NULL) {
			fprintf(logger->file, "%s", buffer);
			fflush(logger->file);
		}

		if (logger->is_active_console) {
			printf("%s", buffer);
			fflush(stdout);
		}

		free(time);
		free(message);
		free(buffer);
	}
}

void global_log_set(t_log* self) {
	global_logger = self;
}

void global_log_create(char* file, char *program_name, bool is_active_console,
		t_log_level level) {
	t_log* logger = log_create(file, program_name, is_active_console, level);
	global_log_set(logger);
}

void global_log_destroy() {
	log_destroy(global_log_get());
}

/**
 * @NAME: global_log_trace
 * @DESC: Loguea un mensaje con el siguiente formato
 *
 * [TRACE] hh:mm:ss:mmmm PROCESS_NAME/(PID:TID): MESSAGE
 *
 */
void global_log_trace(const char* message_template, ...) {
	va_list arguments;
	va_start(arguments, message_template);
	global_log_write_in_level(LOG_LEVEL_TRACE, message_template, arguments);
	va_end(arguments);
}

/**
 * @NAME: global_log_debug
 * @DESC: Loguea un mensaje con el siguiente formato
 *
 * [DEBUG] hh:mm:ss:mmmm PROCESS_NAME/(PID:TID): MESSAGE
 *
 */
void global_log_debug(const char* message_template, ...) {
	va_list arguments;
	va_start(arguments, message_template);
	global_log_write_in_level(LOG_LEVEL_DEBUG, message_template, arguments);
	va_end(arguments);
}

/**
 * @NAME: global_log_info
 * @DESC: Loguea un mensaje con el siguiente formato
 *
 * [INFO] hh:mm:ss:mmmm PROCESS_NAME/(PID:TID): MESSAGE
 *
 */
void global_log_info(const char* message_template, ...) {
	va_list arguments;
	va_start(arguments, message_template);
	global_log_write_in_level(LOG_LEVEL_INFO, message_template, arguments);
	va_end(arguments);
}

/**
 * @NAME: global_log_warning
 * @DESC: Loguea un mensaje con el siguiente formato
 *
 * [WARNING] hh:mm:ss:mmmm PROCESS_NAME/(PID:TID): MESSAGE
 *
 */
void global_log_warning(const char* message_template, ...) {
	va_list arguments;
	va_start(arguments, message_template);
	global_log_write_in_level(LOG_LEVEL_WARNING, message_template, arguments);
	va_end(arguments);
}

/**
 * @NAME: global_log_error
 * @DESC: Loguea un mensaje con el siguiente formato
 *
 * [ERROR] hh:mm:ss:mmmm PROCESS_NAME/(PID:TID): MESSAGE
 *
 */
void global_log_error(const char* message_template, ...) {
	va_list arguments;
	va_start(arguments, message_template);
	global_log_write_in_level(LOG_LEVEL_ERROR, message_template, arguments);
	va_end(arguments);
}
