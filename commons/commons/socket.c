/*
 * sockets.c
 *
 *  Created on: 02/06/2012
 *      Author: vero
 */

#include "socket.h"
#include <errno.h>
#include "syntax_sugars.h"
#include <stdlib.h>
#include "fuse/serializers.h"
#include "fuse/packages_utils.h"

#define MAX_CONNECTIONS 64

// ----------------------------------------------------------
//						PRIVATE HELPERS
// ----------------------------------------------------------

t_paquete* request_handshake(t_socket* self) {
	t_paquete* handshake_request = new(t_paquete);
	handshake_request->type = HANDSHAKE;
	handshake_request->payload_len = 0;
	handshake_request->payload = NULL;

	int result = socket_send_nipc(self, handshake_request);

	if (result == -1) {
		perror("No se pudo enviar el handshake");
		return NULL;
	}

	destroy_packageNipc(handshake_request);
	return socket_recvNipc(self);
}

int answer_handshake(t_socket* client) {
	t_paquete* handshake_response = new(t_paquete);
	handshake_response->type = HANDSHAKE_RESPONSE;
	handshake_response->payload_len = 0;
	handshake_response->payload = NULL;

	int result = socket_send_nipc(client, handshake_response);
	destroy_packageNipc(handshake_response);

	return result;
}

// ----------------------------------------------------------
//						PUBLIC METHODS
// ----------------------------------------------------------

t_socket* socket_createServer(char* ip, int port) {
	t_socket* newSocket = malloc(sizeof(t_socket));
	newSocket->my_addr = malloc(sizeof(struct sockaddr_in));
	newSocket->desc = socket(AF_INET, SOCK_STREAM, 0);

	if(newSocket->desc == -1){
		perror("No se pudo abrir el socket");
	}

	int optval = 1; /*esto es para que se pueda volver a usar el puerto sin esperar,en reuse_addr me va a guardar -1 en caso de error*/
	newSocket->reuse_addr = setsockopt(newSocket->desc, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));

	newSocket->my_addr->sin_family = AF_INET;
	newSocket->my_addr->sin_addr.s_addr = inet_addr(ip);
	newSocket->my_addr->sin_port = htons(port);

	struct sockaddr* address = (struct sockaddr*) newSocket->my_addr;
	if (bind(newSocket->desc, address, sizeof(struct sockaddr_in)) != 0) {
		perror("No se pudo bindear el socket");
		close(newSocket->desc);
		return NULL;
	}
	return newSocket;
}

t_socket* socket_createClient() {
	t_socket* newSocket = malloc(sizeof(t_socket));
	newSocket->my_addr = NULL;

	newSocket->desc = socket(AF_INET, SOCK_STREAM, 0);

	if(newSocket->desc == -1){
		perror("No se pudo abrir el socket");
	}
	int optval = 1; /*esto es para que se pueda volver a usar el puerto sin esperar,en reuse_addr me va a guardar -1 en caso de error*/
	newSocket->reuse_addr = setsockopt(newSocket->desc, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));

//	make_socket_non_blocking(newSocket->desc);
	return newSocket;
}

/*Me va a devolver -1 si hubo algún error*/
int socket_connect(t_socket* self, char* ip, int port) {
	struct sockaddr_in stcRemoteAddr;
	memset((char*) &stcRemoteAddr, '\x00', sizeof(stcRemoteAddr));
	stcRemoteAddr.sin_family = AF_INET;
	stcRemoteAddr.sin_port = htons(port);
	stcRemoteAddr.sin_addr.s_addr = inet_addr(ip);

	int length = sizeof(stcRemoteAddr);
	int result = connect(self->desc, (struct sockaddr*)&stcRemoteAddr, length);

	if (result == -1) return result;

	t_paquete* response = request_handshake(self);
	return response->type == HANDSHAKE_RESPONSE? result : -1;
}

/*me devolverá -1 en caso de error, sino la cantidad de bytes enviados*/
int socket_send(t_socket* self, void* data, int length) {
	return send(self->desc, data, length, 0);
}

int socket_send_nipc(t_socket* client, t_paquete* nipc_package) {
	t_stream* stream = serial_paquete(nipc_package);
	int ret = socket_send(client, stream->data, stream->length);

	destroy_stream(stream);
	return ret;
}

t_socket_buffer* socket_recv(t_socket* self) {
	char buf[512];
	int descriptor = self->desc;

	ssize_t count = read(descriptor, buf, sizeof buf);
	if (count == -1) {
		/* If errno == EAGAIN, that means we have read all
		 data. So go back to the main loop. */
		if (errno != EAGAIN) {
			perror("read");
		}
	}

	t_socket_buffer* buffer = new(t_socket_buffer);
	buffer->size = count;
	buffer->data = malloc(count);
	strcpy(buffer->data, buf);

	return buffer;
}

t_paquete *socket_recvNipc(t_socket* self) {
	t_paquete *paquete = malloc(sizeof(t_paquete));
	t_stream *stream = malloc(sizeof(t_stream));
	uint16_t offset = 0, tmp_size = 0;

	stream->length = sizeof(fuseOperationsType) + sizeof(uint16_t);
	stream->data = malloc(stream->length);

	int ret = recv(self->desc, stream->data, stream->length, MSG_WAITALL);

	if (ret == 0)
		return NULL;

	memcpy(&paquete->type, stream->data, tmp_size =
			sizeof(fuseOperationsType));

	offset = tmp_size;
	memcpy(&paquete->payload_len, stream->data + offset, tmp_size =
			sizeof(uint16_t));

	paquete->payload = malloc(paquete->payload_len);/*testear con este cambio*/

	if (paquete->type != HANDSHAKE && paquete->type != HANDSHAKE_RESPONSE)
		recv(self->desc, paquete->payload, paquete->payload_len, MSG_WAITALL);

	return paquete;
}

/*me va a devolver -1 en caso de error*/
int socket_listen(t_socket* self) {
	//TODO: esto me suena a que viene por archivo config...
	return listen(self->desc, MAX_CONNECTIONS);
}

t_socket *socket_accept(t_socket* self) {
	t_socket* client = new(t_socket);
	client->my_addr = new(struct sockaddr);
	socklen_t addr_size = sizeof( (struct sockaddr *) client->my_addr);
	client->desc = accept(self->desc, (void*) client->my_addr, &addr_size);

	if(client->desc == -1){
		perror("No se pudo aceptar la conexion entrante");
		return NULL;
	}

	t_paquete* handshake_request = socket_recvNipc(client);
	if (handshake_request->type != HANDSHAKE) {
		destroy_packageNipc(handshake_request);
		return NULL;
	}

	destroy_packageNipc(handshake_request);

	int result = answer_handshake(client);

	if (result == -1) {
		perror("No se pudo responder el handshake");
		return NULL;
	}

	return client;
}

void socket_destroy(t_socket *self) {
	close(self->desc);
	free(self->my_addr);
	free(self);
}
