/*
 * thread.c
 *
 *  Created on: Jun 5, 2012
 *      Author: faloi
 */

#include "thread.h"
#include <pthread.h>
#include "syntax_sugars.h"
#include <stdlib.h>

void thread_create(void* (*operation)(void*), void* parameter) {
	pthread_t thread;
	pthread_create(&thread, NULL, operation, parameter);
}
