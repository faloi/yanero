/*
 * socket.h
 *
 *  Created on: 02/06/2012
 *      Author: vero
 */

#ifndef SOCKET_H_
#define SOCKET_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>

#include "fuse/packages.h"

#define DEFAULT_BUFFER_SIZE 2048
#define DEFAULT_MAX_CONEXIONS 100
#define SELECT_USEC_TIOMEOUT 500

/*Estructuras de sockets*/

typedef enum {
	SOCKETMODE_NONBLOCK = 1, SOCKETMODE_BLOCK = 2
} e_socket_mode;

typedef enum {
	SOCKETMODE_CONNECTED, SOCKETMODE_DISCONNECTED
} e_socket_state;

typedef struct {
	int desc;
	struct sockaddr_in* my_addr;
	e_socket_mode mode;
	int reuse_addr;
} t_socket;

typedef struct {
	char* data;
	int size;
} t_socket_buffer;

/*Funciones*/


t_socket* 	socket_createServer(char* ip, int port);
t_socket* 	socket_createClient();
int 		socket_connect(t_socket* self, char* ip, int port);
int 		socket_send(t_socket* self, void* data, int length);
int 		socket_send_nipc(t_socket* client, t_paquete* nipc_package);
t_paquete*	socket_recvNipc(t_socket* self);
int 		socket_listen(t_socket* self);
t_socket*	socket_accept(t_socket* self);
void 		socket_destroy(t_socket* self);


#endif /* SOCKET_H_ */
