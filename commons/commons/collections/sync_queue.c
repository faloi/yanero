/*
 * sync_queue.c
 *
 *  Created on: 26/06/2012
 *      Author: oli
 */

#include "queue.h"
#include "sync_queue.h"
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include "../syntax_sugars.h"
#include <semaphore.h>

// ----------------------------------------------------------
//						PRIVATE HELPERS
// ----------------------------------------------------------

static void lockMe(t_sync_queue* self) {
	pthread_mutex_lock(self->lock);
}

static void unlockMe(t_sync_queue* self) {
	pthread_mutex_unlock(self->lock);
}


// ----------------------------------------------------------
//						PUBLIC METHODS
// ----------------------------------------------------------

t_sync_queue* sync_queue_create() {
	t_sync_queue* self = new(t_sync_queue);
	self->queue = queue_create();
	self->lock = new(pthread_rwlock_t);
	self->semaphore = new(sem_t);
	pthread_mutex_init(self->lock, NULL);
	int ret = sem_init(self->semaphore, 0, 0);

	return self;
}

void* sync_queue_pop(t_sync_queue* self) {
	sem_wait(self->semaphore);

	lockMe(self);
	void* value = queue_pop(self->queue);
	unlockMe(self);

	return value;
}

void sync_queue_push(t_sync_queue* self, void* element) {
	lockMe(self);
	queue_push(self->queue, element);
	unlockMe(self);

	sem_post(self->semaphore);
}

void sync_queue_destroy(t_sync_queue* self) {
	queue_destroy(self->queue);
	pthread_mutex_destroy(self->lock);
	free(self);
}
