/*
 * sync_dictionary.c
 *
 *  Created on: Jun 27, 2012
 *      Author: faloi
 */

#include "sync_dictionary.h"
#include "../syntax_sugars.h"
#include <pthread.h>
#include <stdlib.h>

// ----------------------------------------------------------
//						PRIVATE HELPERS
// ----------------------------------------------------------

void writeLockMe(t_sync_dictionary* self) {
	pthread_rwlock_wrlock(self->lock);
}

void readLockMe(t_sync_dictionary* self) {
	pthread_rwlock_rdlock(self->lock);
}

void unlockMe(t_sync_dictionary* self) {
	pthread_rwlock_unlock(self->lock);
}


// ----------------------------------------------------------
//						PUBLIC METHODS
// ----------------------------------------------------------

t_sync_dictionary *sync_dictionary_create(void (*data_destroyer)(void*)) {
	t_sync_dictionary* self = new(t_sync_dictionary);
	self->dictionary = dictionary_create(data_destroyer);
	self->lock = new(pthread_rwlock_t);
	pthread_rwlock_init(self->lock, NULL);

	return self;
}

bool sync_dictionary_has_key(t_sync_dictionary* self, char* key) {
	readLockMe(self);
	bool value = dictionary_has_key(self->dictionary, key);
	unlockMe(self);

	return value;
}

void sync_dictionary_put(t_sync_dictionary* self, char* key, void* data) {
	writeLockMe(self);
	dictionary_put(self->dictionary, key, data);
	unlockMe(self);
}

void* sync_dictionary_get(t_sync_dictionary* self, char *key) {
	readLockMe(self);
	void* value = dictionary_get(self->dictionary, key);
	unlockMe(self);

	return value;
}

void* sync_dictionary_remove(t_sync_dictionary* self, char *key) {
	writeLockMe(self);
	void* item = dictionary_remove(self->dictionary, key);
	unlockMe(self);

	return item;
}

void sync_dictionary_destroy(t_sync_dictionary* self) {
	dictionary_destroy(self->dictionary);
	pthread_rwlock_destroy(self->lock);
	free(self);
}
