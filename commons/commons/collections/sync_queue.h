/*
 * sync_queue.h
 *
 *  Created on: 26/06/2012
 *      Author: oli
 */

#ifndef SYNC_QUEUE_H_
#define SYNC_QUEUE_H_

	#include "queue.h"
	#include <pthread.h>
	#include <semaphore.h>

	typedef struct {
		t_queue* 			queue;
		pthread_mutex_t*	lock;
		sem_t*				semaphore;
	} t_sync_queue;

	t_sync_queue* 	sync_queue_create();
	void* 			sync_queue_pop(t_sync_queue* self);
	void 			sync_queue_push(t_sync_queue* self, void* element);
	void 			sync_queue_destroy(t_sync_queue* self);

#endif /* SYNC_QUEUE_H_ */
