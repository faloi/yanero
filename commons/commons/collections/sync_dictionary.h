/*
 * sync_dictionary.h
 *
 *  Created on: Jun 27, 2012
 *      Author: faloi
 */

#ifndef SYNC_DICTIONARY_H_
#define SYNC_DICTIONARY_H_

	#include "dictionary.h"
	#include <stdbool.h>
	#include <sys/types.h>

	typedef struct {
		t_dictionary* 		dictionary;
		pthread_rwlock_t*	lock;
	} t_sync_dictionary;

	t_sync_dictionary 	*sync_dictionary_create(void(*data_destroyer)(void*));
	bool 		  		sync_dictionary_has_key(t_sync_dictionary *, char* key);
	void 		  		sync_dictionary_put(t_sync_dictionary *, char *key, void *data);
	void 		 		*sync_dictionary_get(t_sync_dictionary *, char *key);
	void 		 		*sync_dictionary_remove(t_sync_dictionary *, char *key);
	void 		 		sync_dictionary_destroy(t_sync_dictionary *);


#endif /* SYNC_DICTIONARY_H_ */
