/*
 * serial.h
 *
 *  Created on: 23/05/2012
 *      Author: vero
 */

#ifndef FUSESERIALIZERS_H_
#define FUSESERIALIZERS_H_


#include <stdio.h>
#include <string.h>
#include "packages.h"


t_stream *serial_paquete(t_paquete *);
t_paquete *deserial_paquete(t_stream*);

/*Serialización de Pedidos*/
t_paquete *serial_pedido_create(t_pedido_payload_create*);
t_paquete *serial_pedido_open(t_pedido_payload_open*);
t_paquete *serial_pedido_read(t_pedido_payload_read*);
t_paquete *serial_pedido_write(t_pedido_payload_write*);
t_paquete *serial_pedido_release(t_pedido_payload_release*);
t_paquete *serial_pedido_truncate(t_pedido_payload_truncate*);
t_paquete *serial_pedido_unlink(t_pedido_payload_unlink*);
t_paquete *serial_pedido_rmdir(t_pedido_payload_rmdir*);
t_paquete *serial_pedido_readdir(t_pedido_payload_readdir*);
t_paquete *serial_pedido_getattr(t_pedido_payload_getattr*);
t_paquete *serial_pedido_mkdir(t_pedido_payload_mkdir*);

/*Serialización de Respuestas*/
t_paquete *serial_respuesta_create(t_respuesta_payload_create*);
t_paquete *serial_respuesta_open(t_respuesta_payload_open*);
t_paquete *serial_respuesta_read(t_respuesta_payload_read*);
t_paquete *serial_respuesta_write(t_respuesta_payload_write*);
t_paquete *serial_respuesta_release(t_respuesta_payload_release*);
t_paquete *serial_respuesta_truncate(t_respuesta_payload_truncate*);
t_paquete *serial_respuesta_unlink(t_respuesta_payload_unlink*);
t_paquete *serial_respuesta_rmdir(t_respuesta_payload_rmdir*);
t_paquete *serial_respuesta_readdir(t_respuesta_payload_readdir*);
t_paquete *serial_respuesta_getattr(t_respuesta_payload_getattr*);
t_paquete *serial_respuesta_mkdir(t_respuesta_payload_mkdir*);

/*Deserialización de los Pedidos*/

t_pedido_payload_create *deserial_pedido_create(t_paquete*);
t_pedido_payload_open *deserial_pedido_open(t_paquete*);
t_pedido_payload_read *deserial_pedido_read(t_paquete*);
t_pedido_payload_write *deserial_pedido_write(t_paquete*);
t_pedido_payload_release *deserial_pedido_release(t_paquete*);
t_pedido_payload_truncate *deserial_pedido_truncate(t_paquete*);
t_pedido_payload_unlink *deserial_pedido_unlink(t_paquete*);
t_pedido_payload_rmdir *deserial_pedido_rmdir(t_paquete*);
t_pedido_payload_readdir *deserial_pedido_readdir(t_paquete*);
t_pedido_payload_getattr *deserial_pedido_getattr(t_paquete*);
t_pedido_payload_mkdir *deserial_pedido_mkdir(t_paquete*);

/*Deserialización de las Respuestas*/

t_respuesta_payload_create *deserial_respuesta_create(t_paquete*);
t_respuesta_payload_open *deserial_respuesta_open(t_paquete*);
t_respuesta_payload_read *deserial_respuesta_read(t_paquete*);
t_respuesta_payload_write *deserial_respuesta_write(t_paquete*);
t_respuesta_payload_release *deserial_respuesta_release(t_paquete*);
t_respuesta_payload_truncate *deserial_respuesta_truncate(t_paquete*);
t_respuesta_payload_unlink *deserial_respuesta_unlink(t_paquete*);
t_respuesta_payload_rmdir *deserial_respuesta_rmdir(t_paquete*);
t_respuesta_payload_readdir *deserial_respuesta_readdir(t_paquete*);
t_respuesta_payload_getattr *deserial_respuesta_getattr(t_paquete*);
t_respuesta_payload_mkdir *deserial_respuesta_mkdir(t_paquete*);

#endif /* SERIAL_H_ */
