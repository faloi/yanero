/*
 * packages_utils.h
 *
 *  Created on: 24/06/2012
 *      Author: vero
 */

#ifndef PACKAGES_UTILS_H_
#define PACKAGES_UTILS_H_

#include "packages.h"

void destroy_stream(t_stream *);
void destroy_packageNipc(t_paquete*);

void destroy_pedidoPayloadCreate(t_pedido_payload_create*);
void destroy_respuestaPayloadCreate(t_respuesta_payload_create*);
void destroy_pedidoPayloadGetattr(t_pedido_payload_getattr*);
void destroy_respuestaPayloadGetAttr(t_respuesta_payload_getattr*);
void destroy_pedidoPayloadMkdir(t_pedido_payload_mkdir*);
void destroy_respuestaPayloadMkdir(t_respuesta_payload_mkdir*);
void destroy_pedidoPayloadOpen(t_pedido_payload_open*);
void destroy_respuestaPayloadOpen(t_respuesta_payload_open*);
void destroy_pedidoPayloadRead(t_pedido_payload_read*);
void destroy_respuestaPayloadRead(t_respuesta_payload_read*);
void destroy_pedidoPayloadReaddir(t_pedido_payload_readdir*);
void destroy_respuestaPayloadReaddir(t_respuesta_payload_readdir*);
void destroy_pedidoPayloadRelease(t_pedido_payload_release*);
void destroy_respuestaPayloadRelease(t_respuesta_payload_release*);
void destroy_pedidoPayloadRmdir(t_pedido_payload_rmdir*);
void destroy_respuestaPayloadRmdir(t_respuesta_payload_rmdir*);
void destroy_pedidoPayloadTruncate(t_pedido_payload_truncate*);
void destroy_respuestaPayloadTruncate(t_respuesta_payload_truncate*);
void destroy_pedidoPayloadUnlink(t_pedido_payload_unlink*);
void destroy_respuestaPayloadUnlink(t_respuesta_payload_unlink*);
void destroy_pedidoPayloadWrite(t_pedido_payload_write*);
void destroy_respuestaPayloadWrite(t_respuesta_payload_write*);

char* fuse_operation_to_string(fuseOperationsType type);


#endif /* PACKAGES_UTILS_H_ */
