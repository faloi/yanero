/*
 * packages_utils.c
 *
 *  Created on: 24/06/2012
 *      Author: vero
 */
#include"packages_utils.h"
#include <stdlib.h>
#include<string.h>
#define OPERATIONS_SIZE 13

static char *fuse_operationsNames[OPERATIONS_SIZE] = {
		"HANDSHAKE",
		"HANDSHAKE_RESPONSE",
		"CREATE",
		"OPEN",
		"READ",
		"WRITE",
		"RELEASE",
		"TRUNCATE",
		"UNLINK",
		"REMOVE_DIRECTORY",
		"READ_DIRECTORY",
		"GET_ATTRIBUTES",
		"MAKE_DIRECTORY"};

char* fuse_operation_to_string(fuseOperationsType type) {
	return fuse_operationsNames[type];
}

void destroy_stream(t_stream *stream){
	free(stream->data);
	free(stream);
}

void destroy_packageNipc(t_paquete*paquete){
	free(paquete->payload);
	free(paquete);
}

void destroy_pedidoPayloadCreate(t_pedido_payload_create*payload){
	free(payload->path);
	free(payload);
}

void destroy_respuestaPayloadCreate(t_respuesta_payload_create*payload){
	free(payload);
}
void destroy_pedidoPayloadGetattr(t_pedido_payload_getattr*payload){
	free(payload->path);
	free(payload);
}
void destroy_respuestaPayloadGetAttr(t_respuesta_payload_getattr*payload){
	free(payload);
}
void destroy_pedidoPayloadMkdir(t_pedido_payload_mkdir*payload){
	free(payload->path);
	free(payload);
}
void destroy_respuestaPayloadMkdir(t_respuesta_payload_mkdir*payload){
	free(payload);
}
void destroy_pedidoPayloadOpen(t_pedido_payload_open*payload){
	free(payload->path);
	free(payload);
}
void destroy_respuestaPayloadOpen(t_respuesta_payload_open*payload){
	free(payload);
}
void destroy_pedidoPayloadRead(t_pedido_payload_read*payload){
	free(payload->path);
	free(payload);
}
void destroy_respuestaPayloadRead(t_respuesta_payload_read*payload){
	free(payload->content);
	free(payload);
}
void destroy_pedidoPayloadReaddir(t_pedido_payload_readdir*payload){
	free(payload->path);
	free(payload);
}
void destroy_respuestaPayloadReaddir(t_respuesta_payload_readdir*payload){
	int j=0;
	for (j = 0; j < payload->cant_archivos; j++){
		free(payload->arr_archivos[j]);
	}
	free(payload->arr_archivos);
	free(payload);
}
void destroy_pedidoPayloadRelease(t_pedido_payload_release*payload){
	free(payload->path);
	free(payload);
}
void destroy_respuestaPayloadRelease(t_respuesta_payload_release*payload){
	free(payload);
}
void destroy_pedidoPayloadRmdir(t_pedido_payload_rmdir*payload){
	free(payload->path);
	free(payload);
}
void destroy_respuestaPayloadRmdir(t_respuesta_payload_rmdir*payload){
	free(payload);
}
void destroy_pedidoPayloadTruncate(t_pedido_payload_truncate*payload){
	free(payload->path);
	free(payload);
}
void destroy_respuestaPayloadTruncate(t_respuesta_payload_truncate*payload){
	free(payload);
}
void destroy_pedidoPayloadUnlink(t_pedido_payload_unlink*payload){
	free(payload->path);
	free(payload);
}
void destroy_respuestaPayloadUnlink(t_respuesta_payload_unlink*payload){
	free(payload);
}
void destroy_pedidoPayloadWrite(t_pedido_payload_write*payload){
	free(payload->content);
	free(payload->path);
	free(payload);
}
void destroy_respuestaPayloadWrite(t_respuesta_payload_write*payload){
	free(payload);
}
