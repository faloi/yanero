/*
 * @NAME:serial.c
 * @DESC:Serializadores de todos los paquetes de pedido y de respuesta
 *
 *
 *  Created on: 23/05/2012
 *      Author: vero
 */

#include "serializers.h"
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "packages.h"

/*Serialización de Paquetes NIPC */

t_stream *serial_paquete(t_paquete *paquete) {

	char *data = malloc(
			sizeof(fuseOperationsType) + sizeof(uint16_t)
					+ paquete->payload_len);


	t_stream *stream = malloc(sizeof(t_stream));
	uint16_t offset = 0, tmp_size = 0;

	memcpy(data, paquete, tmp_size = sizeof(paquete->type)+sizeof(paquete->payload_len));
	offset+=tmp_size;
	memcpy(data + offset, paquete->payload, tmp_size = paquete->payload_len);

	stream->length = offset + tmp_size;
	stream->data = data;

	return stream;
}

/*Deserialización de NIPC*/

t_paquete *deserial_paquete(t_stream *stream) {
	t_paquete *paquete = malloc(sizeof(t_paquete));
	uint16_t offset = 0, tmp_size = 0;

	memcpy(&paquete->type, stream->data, tmp_size =
			sizeof (fuseOperationsType));

	offset = tmp_size;
	memcpy(&paquete->payload_len, stream->data + offset, tmp_size =
			sizeof(uint16_t));

	offset += tmp_size;
	paquete->payload = malloc(paquete->payload_len);
	memcpy(paquete->payload, stream->data + offset,
			tmp_size = paquete->payload_len);

	return paquete;
}

t_paquete *serial_pedido_create(t_pedido_payload_create* payload_create) {
	char* data = malloc(strlen(payload_create->path) + 1);

	t_paquete *pedido = malloc(sizeof(t_paquete));
	uint16_t offset = 0, tmp_size = 0;

	memcpy(data, payload_create->path,
			tmp_size = strlen(payload_create->path) + 1);

	offset = tmp_size;

	pedido->type = CREATE;
	pedido->payload_len = offset;
	pedido->payload = data;

	return pedido;
}

t_paquete *serial_pedido_open(t_pedido_payload_open* payload_open) {
	char* data = malloc(strlen(payload_open->path) + 1);

	t_paquete *pedido = malloc(sizeof(t_paquete));
	uint16_t offset = 0, tmp_size = 0;

	memcpy(data, payload_open->path, tmp_size = strlen(payload_open->path) + 1);

	offset = tmp_size;

	pedido->type = OPEN;
	pedido->payload_len = offset;
	pedido->payload = data;

	return pedido;
}

t_paquete *serial_pedido_read(t_pedido_payload_read *pedido_payload) {
	char *data = malloc(
			strlen(pedido_payload->path) + 1 + sizeof(uint64_t)
					+ sizeof(uint64_t));
	t_paquete *pedido = malloc(sizeof(t_paquete));
	uint16_t offset = 0, tmp_size = 0;

	memcpy(data, pedido_payload->path,
			tmp_size = strlen(pedido_payload->path) + 1);

	offset = tmp_size;
	memcpy(data + offset, &pedido_payload->size, tmp_size = sizeof(uint64_t));

	offset += tmp_size;
	memcpy(data + offset, &pedido_payload->offset, tmp_size = sizeof(uint64_t));

	pedido->type = READ;
	pedido->payload_len = offset + tmp_size;
	pedido->payload = data;

	return pedido;
}

t_paquete *serial_pedido_write(t_pedido_payload_write *pedido_payload) {
	char* data = malloc(
			strlen(pedido_payload->path) + 1 + sizeof(uint64_t)
					+ pedido_payload->size + sizeof(uint64_t));
	t_paquete *pedido = malloc(sizeof(t_paquete));
	uint16_t offset = 0, tmp_size = 0;

	memcpy(data, pedido_payload->path,
			tmp_size = strlen(pedido_payload->path) + 1);

	offset = tmp_size;
	memcpy(data + offset, &pedido_payload->size, tmp_size = sizeof(uint64_t));

	offset += tmp_size;
	memcpy(data + offset, pedido_payload->content,
			tmp_size = pedido_payload->size);

	offset += tmp_size;
	memcpy(data + offset, &pedido_payload->offset, tmp_size = sizeof(uint64_t));


	pedido->type = WRITE;
	pedido->payload_len = offset + tmp_size;
	pedido->payload = data;

	return pedido;
}

t_paquete *serial_pedido_release(t_pedido_payload_release* payload_release) {
	char* data = malloc(strlen(payload_release->path) + 1);

	t_paquete *pedido = malloc(sizeof(t_paquete));
	uint16_t offset = 0, tmp_size = 0;

	memcpy(data, payload_release->path,
			tmp_size = strlen(payload_release->path) + 1);

	offset = tmp_size;

	pedido->type = RELEASE;
	pedido->payload_len = offset;
	pedido->payload = data;

	return pedido;

}

t_paquete *serial_pedido_truncate(t_pedido_payload_truncate* pedido_payload) {
	char *data = malloc(strlen(pedido_payload->path) + 1 + sizeof(uint64_t));
	t_paquete *pedido = malloc(sizeof(t_paquete));
	uint16_t offset = 0, tmp_size = 0;

	memcpy(data, pedido_payload->path,
			tmp_size = strlen(pedido_payload->path) + 1);

	offset = tmp_size;
	memcpy(data + offset, &pedido_payload->size, tmp_size = sizeof(uint64_t));

	pedido->type = TRUNCATE;
	pedido->payload_len = offset + tmp_size;
	pedido->payload = data;

	return pedido;
}

t_paquete *serial_pedido_unlink(t_pedido_payload_unlink* payload_unlink) {
	char* data = malloc(strlen(payload_unlink->path) + 1);

	t_paquete *pedido = malloc(sizeof(t_paquete));
	uint16_t offset = 0, tmp_size = 0;

	memcpy(data, payload_unlink->path,
			tmp_size = strlen(payload_unlink->path) + 1);

	offset = tmp_size;

	pedido->type = UNLINK;
	pedido->payload_len = offset;
	pedido->payload = data;

	return pedido;
}

t_paquete *serial_pedido_rmdir(t_pedido_payload_rmdir* payload_rmdir) {
	char* data = malloc(strlen(payload_rmdir->path) + 1);

	t_paquete *pedido = malloc(sizeof(t_paquete));
	uint16_t offset = 0, tmp_size = 0;

	memcpy(data, payload_rmdir->path,
			tmp_size = strlen(payload_rmdir->path) + 1);

	offset = tmp_size;

	pedido->type = REMOVE_DIRECTORY;
	pedido->payload_len = offset;
	pedido->payload = data;

	return pedido;
}

t_paquete *serial_pedido_readdir(t_pedido_payload_readdir* payload_readdir) {
	char* data = malloc(strlen(payload_readdir->path) + 1);

	t_paquete *pedido = malloc(sizeof(t_paquete));
	uint16_t offset = 0, tmp_size = 0;

	memcpy(data, payload_readdir->path,
			tmp_size = strlen(payload_readdir->path) + 1);

	offset = tmp_size;

	pedido->type = READ_DIRECTORY;
	pedido->payload_len = offset;
	pedido->payload = data;

	return pedido;
}

t_paquete *serial_pedido_getattr(t_pedido_payload_getattr* payload_getattr) {
	char* data = malloc(strlen(payload_getattr->path) + 1);

	t_paquete *pedido = malloc(sizeof(t_paquete));
	uint16_t offset = 0, tmp_size = 0;

	memcpy(data, payload_getattr->path,
			tmp_size = strlen(payload_getattr->path) + 1);

	offset = tmp_size;

	pedido->type = GET_ATTRIBUTES;
	pedido->payload_len = offset;
	pedido->payload = data;

	return pedido;
}

t_paquete *serial_pedido_mkdir(t_pedido_payload_mkdir* payload_mkdir) {
	char *data = malloc(strlen(payload_mkdir->path) + 1);
	t_paquete *pedido = malloc(sizeof(t_paquete));
	uint16_t offset = 0, tmp_size = 0;

	memcpy(data, payload_mkdir->path,
			tmp_size = strlen(payload_mkdir->path) + 1);
	offset = tmp_size;
	pedido->type = MAKE_DIRECTORY;
	pedido->payload_len = offset;
	pedido->payload = data;

	return pedido;

}

t_paquete *serial_respuesta_create(t_respuesta_payload_create* payload_create) {
	char* data = malloc(sizeof(enum statusCodes));

	t_paquete *respuesta = malloc(sizeof(t_paquete));
	uint16_t offset = 0, tmp_size = 0;

	memcpy(data, &payload_create->status, tmp_size = sizeof(enum statusCodes));

	offset = tmp_size;

	respuesta->type = CREATE;
	respuesta->payload_len = offset;
	respuesta->payload = data;

	return respuesta;

}
t_paquete *serial_respuesta_open(t_respuesta_payload_open* payload_open) {
	char* data = malloc(sizeof(enum statusCodes));

	t_paquete *respuesta = malloc(sizeof(t_paquete));
	uint16_t offset = 0, tmp_size = 0;

	memcpy(data, &payload_open->status, tmp_size = sizeof(enum statusCodes));

	offset = tmp_size;

	respuesta->type = OPEN;
	respuesta->payload_len = offset;
	respuesta->payload = data;

	return respuesta;
}
t_paquete *serial_respuesta_read(t_respuesta_payload_read* payload_read) {

	char* data = malloc(
			sizeof(enum statusCodes) + sizeof(uint64_t) + payload_read->size);
	t_paquete *respuesta = malloc(sizeof(t_paquete));
	uint16_t offset = 0, tmp_size = 0;

	memcpy(data, &payload_read->size, tmp_size = sizeof(uint64_t));

	offset = tmp_size;
	memcpy(data + offset, payload_read->content, tmp_size = payload_read->size);

	offset += tmp_size;

	memcpy(data + offset, &payload_read->status, tmp_size =
			sizeof(enum statusCodes));

	respuesta->type = READ;
	respuesta->payload_len = offset + tmp_size;
	respuesta->payload = data;

	return respuesta;
}
t_paquete *serial_respuesta_write(t_respuesta_payload_write* payload_write) {
	char* data = malloc(sizeof(enum statusCodes) + 1 + sizeof(uint64_t));
	t_paquete *respuesta = malloc(sizeof(t_paquete));
	uint16_t offset = 0, tmp_size = 0;

	memcpy(data, &payload_write->requested, tmp_size = sizeof(uint64_t));

	offset = tmp_size;

	memcpy(data + offset, &payload_write->status, tmp_size =
			sizeof(enum statusCodes));

	respuesta->type = WRITE;
	respuesta->payload_len = offset + tmp_size;
	respuesta->payload = data;

	return respuesta;
}
t_paquete *serial_respuesta_release(
		t_respuesta_payload_release* payload_release) {
	char* data = malloc(sizeof(enum statusCodes));

	t_paquete *respuesta = malloc(sizeof(t_paquete));
	uint16_t offset = 0, tmp_size = 0;

	memcpy(data, &payload_release->status, tmp_size = sizeof(enum statusCodes));

	offset = tmp_size;

	respuesta->type = RELEASE;
	respuesta->payload_len = offset;
	respuesta->payload = data;

	return respuesta;
}

t_paquete *serial_respuesta_truncate(
		t_respuesta_payload_truncate*payload_truncate) {
	char* data = malloc(sizeof(enum statusCodes));

	t_paquete *respuesta = malloc(sizeof(t_paquete));
	uint16_t offset = 0, tmp_size = 0;

	memcpy(data, &payload_truncate->status, tmp_size =
			sizeof(enum statusCodes));

	offset = tmp_size;

	respuesta->type = TRUNCATE;
	respuesta->payload_len = offset;
	respuesta->payload = data;

	return respuesta;
}
t_paquete *serial_respuesta_unlink(t_respuesta_payload_unlink* payload_unlink) {
	char* data = malloc(sizeof(enum statusCodes));

	t_paquete *respuesta = malloc(sizeof(t_paquete));
	uint16_t offset = 0, tmp_size = 0;

	memcpy(data, &payload_unlink->status, tmp_size = sizeof(enum statusCodes));

	offset = tmp_size;

	respuesta->type = UNLINK;
	respuesta->payload_len = offset;
	respuesta->payload = data;

	return respuesta;
}
t_paquete *serial_respuesta_rmdir(t_respuesta_payload_rmdir* payload_rmdir) {
	char* data = malloc(sizeof(enum statusCodes));

	t_paquete *respuesta = malloc(sizeof(t_paquete));
	uint16_t offset = 0, tmp_size = 0;

	memcpy(data, &payload_rmdir->status, tmp_size = sizeof(enum statusCodes));

	offset = tmp_size;

	respuesta->type = REMOVE_DIRECTORY;
	respuesta->payload_len = offset;
	respuesta->payload = data;

	return respuesta;
}

t_paquete *serial_respuesta_readdir(
		t_respuesta_payload_readdir* payload_readdir) {

	uint16_t cant_letras = 0, j = 0, offset = 0, tmp_size = 0;

	/*este for me guarda la cantidad de letras por palabra y los centinela*/
	for (tmp_size = 0; tmp_size < (payload_readdir->cant_archivos);
			tmp_size++) {
		cant_letras += strlen(payload_readdir->arr_archivos[tmp_size]) + 1;
	}

	char* data = malloc(
			sizeof(uint16_t) + sizeof(enum statusCodes) + cant_letras);

	t_paquete *respuesta = malloc(sizeof(t_paquete));

	memcpy(data, &payload_readdir->cant_archivos, tmp_size = sizeof(uint16_t));

	offset = tmp_size;

	// tenes que ir memcpy(..) elemento por elemento del array
	for (j = 0; j < payload_readdir->cant_archivos; j++) {
		int size = strlen(payload_readdir->arr_archivos[j]) + 1;
		memcpy(data + offset, payload_readdir->arr_archivos[j], size);
		offset += size;
	}

	memcpy(data + offset, &payload_readdir->status, tmp_size =
			sizeof(enum statusCodes));

	offset += tmp_size;
	respuesta->type = READ_DIRECTORY;
	respuesta->payload_len = offset;
	respuesta->payload = data;

	return respuesta;
}

t_paquete *serial_respuesta_getattr(t_respuesta_payload_getattr*payload_getattr) {
	char*data = malloc(
			sizeof(enum getattrMode) + sizeof(uint64_t)
					+ sizeof(enum statusCodes));
	t_paquete *respuesta = malloc(sizeof(t_paquete));
	int offset = 0, tmp_size = 0;

	memcpy(data, &payload_getattr->mode, tmp_size = sizeof(enum getattrMode));

	offset += tmp_size;
	memcpy(data+offset, &payload_getattr->size, tmp_size = sizeof(uint64_t));

	offset += tmp_size;
	memcpy(data+offset, &payload_getattr->status, tmp_size = sizeof(enum statusCodes));

	offset += tmp_size;
	respuesta->type = GET_ATTRIBUTES;
	respuesta->payload_len = offset;
	respuesta->payload = data;

	return respuesta;
}

t_paquete *serial_respuesta_mkdir(t_respuesta_payload_mkdir* payload_mkdir) {
	char* data = malloc(sizeof(enum statusCodes));

	t_paquete *respuesta = malloc(sizeof(t_paquete));
	uint16_t offset = 0, tmp_size = 0;

	memcpy(data, &payload_mkdir->status, tmp_size = sizeof(enum statusCodes));

	offset = tmp_size;
	respuesta->type = MAKE_DIRECTORY;
	respuesta->payload_len = offset;
	respuesta->payload = data;

	return respuesta;
}

/*Deserializaciones*/

t_pedido_payload_create *deserial_pedido_create(t_paquete* paquete) {
	t_pedido_payload_create *pedido;
	pedido = malloc(sizeof(t_pedido_payload_create));

	pedido->path = malloc(paquete->payload_len);
	memcpy(pedido->path, paquete->payload, paquete->payload_len);

	return pedido;
}

t_pedido_payload_open *deserial_pedido_open(t_paquete* paquete) {
	t_pedido_payload_open *pedido;
	pedido = malloc(sizeof(t_pedido_payload_open));

	pedido->path = malloc(paquete->payload_len);
	memcpy(pedido->path, paquete->payload, paquete->payload_len);

	return pedido;
}

t_pedido_payload_read *deserial_pedido_read(t_paquete* paquete) {
	t_pedido_payload_read *pedido;
	pedido = malloc(sizeof(t_pedido_payload_read));
	uint16_t offset = 0, tmp_size = 0;

	for (tmp_size = 1; paquete->payload[tmp_size - 1] != '\0'; tmp_size++)
		;
	pedido->path = malloc(tmp_size);
	memcpy(pedido->path, paquete->payload, tmp_size);

	offset = tmp_size;
	memcpy(&pedido->size, paquete->payload + offset, tmp_size =
			sizeof(uint64_t));

	offset += tmp_size;
	memcpy(&pedido->offset, paquete->payload + offset, tmp_size =
			sizeof(uint64_t));

	return pedido;
}

t_pedido_payload_write *deserial_pedido_write(t_paquete* paquete) {
	t_pedido_payload_write *pedido;
	pedido = malloc(sizeof(t_pedido_payload_write));
	uint16_t offset = 0, tmp_size = 0;

	for (tmp_size = 1; paquete->payload[tmp_size - 1] != '\0'; tmp_size++);
	pedido->path = malloc(tmp_size);
	memcpy(pedido->path, paquete->payload, tmp_size);

	offset = tmp_size;
	memcpy(&pedido->size, paquete->payload + offset, tmp_size =
			sizeof(uint64_t));

	offset += tmp_size;

	pedido->content = malloc(pedido->size);
	memcpy(pedido->content, paquete->payload + offset, tmp_size = pedido->size);

	offset += tmp_size;
	memcpy(&pedido->offset, paquete->payload + offset, tmp_size =
			sizeof(uint64_t));

	return pedido;
}

t_pedido_payload_release *deserial_pedido_release(t_paquete* paquete) {
	t_pedido_payload_release *pedido;
	pedido = malloc(sizeof(t_pedido_payload_release));

	pedido->path = malloc(paquete->payload_len);
	memcpy(pedido->path, paquete->payload, paquete->payload_len);

	return pedido;
}

t_pedido_payload_truncate *deserial_pedido_truncate(t_paquete* paquete) {
	t_pedido_payload_truncate *pedido;
	pedido = malloc(sizeof(t_pedido_payload_truncate));
	uint16_t offset = 0, tmp_size = 0;

	for (tmp_size = 1; paquete->payload[tmp_size - 1] != '\0'; tmp_size++)
		;
	pedido->path = malloc(tmp_size);
	memcpy(pedido->path, paquete->payload, tmp_size);

	offset = tmp_size;
	memcpy(&pedido->size, paquete->payload + offset, tmp_size =
			sizeof(uint64_t));

	return pedido;
}

t_pedido_payload_unlink *deserial_pedido_unlink(t_paquete*paquete) {
	t_pedido_payload_unlink *pedido;
	pedido = malloc(sizeof(t_pedido_payload_unlink));

	pedido->path = malloc(paquete->payload_len);
	memcpy(pedido->path, paquete->payload, paquete->payload_len);

	return pedido;

}

t_pedido_payload_rmdir *deserial_pedido_rmdir(t_paquete*paquete) {
	t_pedido_payload_rmdir *pedido;
	pedido = malloc(sizeof(t_pedido_payload_rmdir));

	pedido->path = malloc(paquete->payload_len);
	memcpy(pedido->path, paquete->payload, paquete->payload_len);

	return pedido;
}

t_pedido_payload_readdir *deserial_pedido_readdir(t_paquete* paquete) {
	t_pedido_payload_readdir *pedido;
	pedido = malloc(sizeof(t_pedido_payload_readdir));

	pedido->path = malloc(paquete->payload_len);
	memcpy(pedido->path, paquete->payload, paquete->payload_len);

	return pedido;
}

t_pedido_payload_getattr *deserial_pedido_getattr(t_paquete* paquete) {
	t_pedido_payload_getattr *pedido;
	pedido = malloc(sizeof(t_pedido_payload_getattr));

	pedido->path = malloc(paquete->payload_len);
	memcpy(pedido->path, paquete->payload, paquete->payload_len);

	return pedido;
}

t_pedido_payload_mkdir *deserial_pedido_mkdir(t_paquete* paquete) {
	t_pedido_payload_mkdir *pedido;
	pedido = malloc(sizeof(t_pedido_payload_mkdir));

	pedido->path = malloc(paquete->payload_len);
	memcpy(pedido->path, paquete->payload, paquete->payload_len);

	return pedido;
}

t_respuesta_payload_create *deserial_respuesta_create(t_paquete *paquete) {
	t_respuesta_payload_create *respuesta;
	respuesta = malloc(sizeof(t_respuesta_payload_create));
	uint16_t tmp_size = 0;

	memcpy(&respuesta->status, paquete->payload, tmp_size =
			sizeof(enum statusCodes));

	return respuesta;
}

t_respuesta_payload_open *deserial_respuesta_open(t_paquete* paquete) {
	t_respuesta_payload_open *respuesta;
	respuesta = malloc(sizeof(t_respuesta_payload_open));
	uint16_t tmp_size = 0;

	memcpy(&respuesta->status, paquete->payload, tmp_size =
			sizeof(enum statusCodes));

	return respuesta;

}

t_respuesta_payload_read *deserial_respuesta_read(t_paquete* paquete) {
	t_respuesta_payload_read *respuesta;
	respuesta = malloc(sizeof(t_respuesta_payload_read));
	uint16_t offset = 0, tmp_size = 0;

	memcpy(&respuesta->size, paquete->payload, tmp_size = sizeof(uint64_t));

	offset = tmp_size;
	respuesta->content = malloc(respuesta->size);
	memcpy(respuesta->content, paquete->payload + offset,
			tmp_size = respuesta->size);

	offset += tmp_size;
	memcpy(&respuesta->status, paquete->payload + offset, tmp_size =
			sizeof(enum statusCodes));

	return respuesta;
}

t_respuesta_payload_write *deserial_respuesta_write(t_paquete* paquete) {
	t_respuesta_payload_write *respuesta;
	respuesta = malloc(sizeof(t_respuesta_payload_write));
	uint16_t offset = 0, tmp_size = 0;

	memcpy(&respuesta->requested, paquete->payload, tmp_size =
			sizeof(uint64_t));

	offset = tmp_size;
	memcpy(&respuesta->status, paquete->payload + offset, tmp_size =
			sizeof(enum statusCodes));

	return respuesta;
}

t_respuesta_payload_release *deserial_respuesta_release(t_paquete *paquete) {
	t_respuesta_payload_release *respuesta;
	respuesta = malloc(sizeof(t_respuesta_payload_release));
	uint16_t tmp_size = 0;

	memcpy(&respuesta->status, paquete->payload, tmp_size =
			sizeof(enum statusCodes));

	return respuesta;
}

t_respuesta_payload_truncate *deserial_respuesta_truncate(t_paquete *paquete) {
	t_respuesta_payload_truncate *respuesta;
	respuesta = malloc(sizeof(t_respuesta_payload_truncate));
	uint16_t tmp_size = 0;

	memcpy(&respuesta->status, paquete->payload, tmp_size =
			sizeof(enum statusCodes));

	return respuesta;
}

t_respuesta_payload_unlink *deserial_respuesta_unlink(t_paquete *paquete) {
	t_respuesta_payload_unlink *respuesta;
	respuesta = malloc(sizeof(t_respuesta_payload_unlink));
	uint16_t tmp_size = 0;

	memcpy(&respuesta->status, paquete->payload, tmp_size =
			sizeof(enum statusCodes));

	return respuesta;
}

t_respuesta_payload_rmdir *deserial_respuesta_rmdir(t_paquete *paquete) {
	t_respuesta_payload_rmdir *respuesta;
	respuesta = malloc(sizeof(t_respuesta_payload_rmdir));
	uint16_t tmp_size = 0;

	memcpy(&respuesta->status, paquete->payload, tmp_size =
			sizeof(enum statusCodes));

	return respuesta;
}

t_respuesta_payload_readdir *deserial_respuesta_readdir(t_paquete*paquete) {
	t_respuesta_payload_readdir *respuesta;
	respuesta = malloc(sizeof(t_respuesta_payload_readdir));
	uint16_t offset = 0, i = 0, tmp_size = 0;

	memcpy(&respuesta->cant_archivos, paquete->payload, tmp_size =
			sizeof(uint16_t));

	offset = tmp_size;

	respuesta->arr_archivos = malloc(respuesta->cant_archivos*sizeof(char*));

	for (i = 0; i < (respuesta->cant_archivos); i++) {
		for (tmp_size = 0; (paquete->payload + offset)[tmp_size] != '\0'; tmp_size++)			;
		respuesta->arr_archivos[i] = malloc(tmp_size + 1);
		memcpy(respuesta->arr_archivos[i], paquete->payload + offset, tmp_size + 1);
		offset += tmp_size + 1;
	}

	memcpy(&respuesta->status, paquete->payload + offset, tmp_size =
			sizeof(enum statusCodes));

	return respuesta;

}

t_respuesta_payload_getattr *deserial_respuesta_getattr(t_paquete* paquete) {
	t_respuesta_payload_getattr *respuesta;
	respuesta = malloc(sizeof(t_respuesta_payload_getattr));
	int offset = 0, tmp_size = 0;

	memcpy(&respuesta->mode, paquete->payload, tmp_size =
			sizeof(enum getattrMode));

	offset = tmp_size;
	memcpy(&respuesta->size, paquete->payload + offset, tmp_size =
			sizeof(uint64_t));

	offset += tmp_size;
	memcpy(&respuesta->status, paquete->payload + offset, tmp_size =
			sizeof(enum statusCodes));

	return respuesta;
}

t_respuesta_payload_mkdir *deserial_respuesta_mkdir(t_paquete *paquete) {
	t_respuesta_payload_mkdir *respuesta;
	respuesta = malloc(sizeof(t_respuesta_payload_mkdir));
	uint16_t tmp_size = 0;

	memcpy(&respuesta->status, paquete->payload, tmp_size =
			sizeof(enum statusCodes));

	return respuesta;
}

