/*
 * FusePackages.h
 *
 *  Created on: 23/05/2012
 *      Author: vero
 */

#ifndef FUSEPACKAGES_H_
#define FUSEPACKAGES_H_

#include <stdint.h>

typedef enum {
	HANDSHAKE,
	HANDSHAKE_RESPONSE,
	CREATE ,
	OPEN,
	READ,
	WRITE,
	RELEASE,
	TRUNCATE,
	UNLINK,
	REMOVE_DIRECTORY,
	READ_DIRECTORY,
	GET_ATTRIBUTES,
	MAKE_DIRECTORY
} fuseOperationsType;

enum getattrMode {
	REGULAR_FILE ,
	DIRECTORY
};

enum statusCodes {
	OPERATION_ERROR,
	OPERATION_FILE_NOT_FOUND,
	OPERATION_FILE_IN_USE,
	OPERATION_PERMISSION_DENIED,
	OPERATION_NOT_A_DIRECTORY,
	OPERATION_IS_A_DIRECTORY,
	OPERATION_OK
};

/* Estructura del protocolo NIPC */
typedef struct {
	fuseOperationsType type;
	uint16_t payload_len;
	char *payload;
} t_paquete;

/*Estructura de Serialización */

typedef struct {
	uint16_t length ;
	char *data;
} t_stream;

/*Payload Pedido Create*/
typedef struct{
	char* path;
} t_pedido_payload_create;

/*Payload Respuesta Create*/
typedef struct{
	enum statusCodes status;
} t_respuesta_payload_create;

/*Payload Pedido Open*/
typedef struct{
	char* path;
} t_pedido_payload_open;

/*Payload Respuesta Open*/
typedef struct{
	enum statusCodes status;
} t_respuesta_payload_open;

/*Payload Pedido Read*/
typedef struct {
	char *path;
	uint64_t size;
	uint64_t offset;
} t_pedido_payload_read;

/*Payload Respuesta Read*/
typedef struct {
	uint64_t size;
	char* content;
	enum statusCodes status;
} t_respuesta_payload_read;

/*Payload Pedido Write*/
typedef struct {
	char *path;
	uint64_t size;
	char *content;
	uint64_t offset;
} t_pedido_payload_write;

/*Payload Respuesta Write*/
typedef struct {
	uint64_t requested;
	enum statusCodes status;
} t_respuesta_payload_write;

/*Payload Pedido Release*/
typedef struct{
	char* path;
} t_pedido_payload_release;

/*Payload Respuesta Release*/
typedef struct{
	enum statusCodes status;
} t_respuesta_payload_release;

/*Payload Pedido Truncate*/
typedef struct {
	char *path;
	uint64_t size;
} t_pedido_payload_truncate;

/*Payload Respuesta Truncate*/
typedef struct{
	enum statusCodes status;
} t_respuesta_payload_truncate;

/*Payload Pedido Unlink*/
typedef struct{
	char* path;
} t_pedido_payload_unlink;

/*Payload Respuesta Unlink*/
typedef struct{
	enum statusCodes status;
} t_respuesta_payload_unlink;

/*Payload Pedido Rmdir*/
typedef struct{
	char* path;
} t_pedido_payload_rmdir;

/*Payload Respuesta Rmdir*/
typedef struct{
	enum statusCodes status;
} t_respuesta_payload_rmdir;

/*Payload Pedido Readdir*/
typedef struct{
	char* path;
} t_pedido_payload_readdir;

/*Payload Respuesta Readdir*/
typedef struct {
	uint16_t cant_archivos;
	char** arr_archivos;
	enum statusCodes status;
} t_respuesta_payload_readdir;

/*Payload Pedido Getattr*/
typedef struct{
	char* path;
} t_pedido_payload_getattr;

/*Payload Respuesta Getattr*/

typedef struct {
	enum getattrMode mode;
	uint64_t size;
	enum statusCodes status;
} t_respuesta_payload_getattr;

/*Payload pedido mkdir*/
typedef struct {
	char *path;
} t_pedido_payload_mkdir;

/*Payload Respuesta mkdir*/
typedef struct{
	enum statusCodes status;
} t_respuesta_payload_mkdir;

typedef struct {
	char* directorioPadre;
	char* fileName;
}t_pathAbsoluto;



#endif /* FUSEPACKAGES_H_ */
