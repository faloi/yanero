/*
 * global_log.h
 *
 *  Created on: Jun 26, 2012
 *      Author: faloi
 */

#ifndef GLOBAL_LOG_H_
#define GLOBAL_LOG_H_

	#include <stdbool.h>
	#include "log.h"

	void 		global_log_create(char* file, char *program_name, bool is_active_console, t_log_level level);
	void 		global_log_destroy();

	void 		global_log_trace(const char* message, ...);
	void 		global_log_debug(const char* message, ...);
	void 		global_log_info(const char* message, ...);
	void 		global_log_warning(const char* message, ...);
	void 		global_log_error(const char* message, ...);

#endif /* GLOBAL_LOG_H_ */
