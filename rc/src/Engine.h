/*
 * Engine.h
 *
 *  Created on: 07/05/2012
 *      Author: utnso
 */
//
#ifndef ENGINE_H_
#define ENGINE_H_
/*
 * Engine.h
 *
 *  Created on: 05/05/2012
 *      Author: Federico Quintas
 */
	#include <memcached/engine.h>
	#include <memcached/util.h>
	#include <memcached/visibility.h>

MEMCACHED_PUBLIC_API ENGINE_ERROR_CODE create_instance(uint64_t interface, GET_SERVER_API get_server_api, ENGINE_HANDLE **handle);
#define LIBRE 1 //para item.Libre
#define OCUPADO 0//para item.Libre
#define STORED 1 //para item.stored
#define NO_STORED 0 //para item.stored
#define FIRST_FIT 1
#define BEST_FIT 0
#define COMPACTACION 1
#define BUDDY_SYSTEM 0
#define LRU 1
#define FIFO 0


#define LONGITUD_MAX_KEY 40

typedef struct {
		   size_t cache_max_size;
		   size_t block_size_max;
		   size_t chunk_size;
		}t_motor_ng_config;

typedef struct {
				ENGINE_HANDLE_V1 engine;
				GET_SERVER_API get_server_api;
				t_motor_ng_config config;
			}t_motor_ng;

	typedef struct _t_Item {
				char stored; //stored==1 es que memcache lo almacenó totalmente.
				char* final;
				uint64_t exptime;
				char key[LONGITUD_MAX_KEY];
				uint64_t tam_data;
				uint64_t tam_disco;
				int long_key;
				uint64_t id;
				uint64_t Cant_Antecesores;
				uint64_t* Padres;
				char* data;
				char Libre; //Libre == 1 es que está totalmente desocupado. (Por el doble trabajo del _allocate)
			} t_Item; // Cada partición tiene su ítem con la información detallada en los campos. Cada ítem apunta con el puntero data
// a la ubicación real de memoria que le corresponde.
//
void compactacion_Inicializar();
void buddy_Inicializar();
t_Item* buddy_allocate(ENGINE_HANDLE* handler, const void* cookie,const char* key,const size_t nbytes, int nkey);
t_Item* compactacion_allocate(ENGINE_HANDLE* handler, const void* cookie,const char* key,const size_t nbytes, int nkey);
t_Item* algoritmo_Allocate_Elegido (ENGINE_HANDLE* handler, const void* cookie,const char* key,const size_t nbytes,t_Item* (*p_funcion_allocate) (ENGINE_HANDLE*, const void*,const char*,const size_t, int), int nkey);
void eliminar_Particion(ENGINE_HANDLE* handler, const char* cookie);
void cargar_Datos_Item(char* key,int comienzo, int final, uint64_t  exptime, int  tam_data);
t_Item* first_fit(uint64_t  tamanio, int Tamanio_Menor_Chunk);
t_Item* best_fit(uint64_t  tamanio, int Tamanio_Menor_Chunk);
t_Item* algoritmo_Elegido (uint64_t  Tamanio_Exigido,int Tamanio_Menor, t_Item* (*p_funcion_fit) (uint64_t, int ));
 //Algoritmo_Elegido es una función que recibe un puntero a una
//función que recibe un int y devuelve un puntero a un t_Item*, para que después lo devuelva Algoritmo_Elegido.
t_Item* buscar_Item_Libre();
t_Item* buscar_Primer_Item_Ocupado();
t_Item* buscar_Item_A_Primera_Particion();
t_Item* buscar_Siguiente(t_Item** item);
t_Item* buscar_Anterior(t_Item** p_item) ;
void intentar_Unir_Libres(t_Item** item);
unsigned long int potencia(int base, int exponente);
t_Item* buscar_Item_Libre_y_sin_Apuntar();
void buddy_release(t_Item** p_item);
void compactar();

void algoritmo_Compactacion(t_Item** Item);

t_Item* buscar_Item_con_Key(char* key, int nkey);
uint64_t contar_Items_Ocupados();
#endif /* ENGINE_H_ */
