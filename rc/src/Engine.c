/*
 * Engine.c
 *
 *  Created on: 05/05/2012
 *      Author: Federico Quintas
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <commons/config.h>
#include <signal.h>
#include <commons/log.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>
#include <malloc.h>
#include <mcheck.h>
#include <math.h>
#include <sys/uio.h>

#include "Engine.h"

// Este include es necesario para que el include <memcached/config_parser.h> no tire error de compilación
#include <stdbool.h>
// Aca estan las tools de memcached para levantar la configuración provista por el usuario en los parametros de ejecución
#include <memcached/config_parser.h>

/*
 * Estas son las funciones estaticas necesarias para que el engine funcione
 */
static ENGINE_ERROR_CODE motor_initialize(ENGINE_HANDLE*,const char* config_str);
static void motor_destroy(ENGINE_HANDLE*, const bool force);
static ENGINE_ERROR_CODE motor_allocate(ENGINE_HANDLE*, const void* cookie,	item **item, const void* key, const size_t nkey, const size_t nbytes,
		const int flags, const rel_time_t exptime);
static bool motor_get_item_info(ENGINE_HANDLE *, const void *cookie,const item* item, item_info *item_info);
static ENGINE_ERROR_CODE motor_store(ENGINE_HANDLE*, const void *cookie,item* item, uint64_t *cas, ENGINE_STORE_OPERATION operation,
		uint16_t vbucket);
static void motor_item_release(ENGINE_HANDLE*, const void *cookie, item* item);
static ENGINE_ERROR_CODE motor_get(ENGINE_HANDLE*, const void* cookie,item** item, const void* key, const int nkey, uint16_t vbucket);
static ENGINE_ERROR_CODE motor_flush(ENGINE_HANDLE*, const void* cookie,time_t when);
static ENGINE_ERROR_CODE motor_item_delete(ENGINE_HANDLE*, const void* cookie,	const void* key, const size_t nkey, uint64_t cas, uint16_t vbucket);

/* ************************** Dummy Functions **************************
 * Estas funciones son dummy, son necesarias para que el engine las tengas
 * pero no tienen logica alguna y no seran necesarias implementar
 */
 static const engine_info* motor_get_info(ENGINE_HANDLE* );
 static ENGINE_ERROR_CODE motor_get_stats(ENGINE_HANDLE* , const void* cookie, const char* stat_key, int nkey, ADD_STAT add_stat);
 static void motor_reset_stats(ENGINE_HANDLE* , const void *cookie);
 static ENGINE_ERROR_CODE motor_unknown_command(ENGINE_HANDLE* , const void* cookie, protocol_binary_request_header *request, ADD_RESPONSE response);
 static void motor_item_set_cas(ENGINE_HANDLE *, const void *cookie, item* item, uint64_t val);
 /***************************************************************************/
//DECLARACIONES
/*
 * Esta función es la que va a ser llamada cuando se reciba una signal
 */

 char* Ultimo_Byte;
int Tam_Chunk;
uint64_t cantidad, Tam_Cache_Max, Cant_Max_Padres, Items_Ocupados, Proximo_Id;


uint64_t Contador_Global;
t_Item* Primer_Item_Ocupado;
t_Item* Vector_Items;
char* RAM;
char Primera_Union;
char Algoritmo_Eliminacion;
char Algoritmo_Allocacion;
char Esquema_Administracion_Memoria;
int Frecuencia_Compactacion;
int Fallas;
char Viene_del_Dummp;
t_Item* (*p_first_fit)(uint64_t,int);
t_Item* (*p_best_fit)(uint64_t,int);
t_Item* (*algoritmo_fit_elegido)(uint64_t, int);
t_config *config;
char Primer_Release;
void (*p_Buddy_Inicializar)(void);
void (*p_Compactacion_Inicializar)(void);
void (*p_Estructura_A_Inicializar)(void);



FILE* Archivo_Dummp;

t_Item* (*p_Buddy_Allocate)(ENGINE_HANDLE*,	const void*,  const char*,const size_t, int);
t_Item* (*p_Compactacion_Allocate)(ENGINE_HANDLE*,	const void* , const char* ,const size_t, int);
t_Item* (*p_Esquema_Allocate)(ENGINE_HANDLE*,	const void* , const char*,const size_t, int );

t_log* Archivo_Log;
char* path;
char Lockear;

int contador_Lock;

uint64_t* Antecesores;
pthread_rwlock_t Lockeo = PTHREAD_RWLOCK_INITIALIZER;


// Esta es la función que va a llamar memcached para instanciar nuestro engine
void dummp(int signal);

MEMCACHED_PUBLIC_API ENGINE_ERROR_CODE create_instance(uint64_t interface,	GET_SERVER_API get_server_api, ENGINE_HANDLE **handle) {
/******************************************************************************************
 *
 * Pongo el path, instancio el archivo de configuración y el engine, el tema de la interfaz.
 *
 ******************************************************************************************/
	contador_Lock=0;
	Viene_del_Dummp=1;



	mtrace();
	//Chequeo interfaz.
		if (interface == 0) {
				return (ENGINE_ENOTSUP);
		}

	Contador_Global=1 ;

	path = "rc.conf";

	config = config_create(path);

	char* Nombre_Del_Programa  = "Engine.c";
	char* Nombre_Archivo_Log = "Engine.log";

	Archivo_Log = log_create(Nombre_Del_Programa, Nombre_Archivo_Log , true, 1);



	Ultimo_Byte=NULL;


		//Allocate memory for the engine descriptor.
			t_motor_ng *engine = calloc(1, sizeof(t_motor_ng));
			if (engine == NULL) {
				return (ENGINE_ENOMEM);
			}

			/*
					 * We're going to implement the first version of the engine API, so
					 * we need to inform the memcached core what kind of structure it should
					 * expect
					 */
				 engine->engine.interface.interface = 1;


/******************************************************************************************
*
* Mapeo las funciones del engine a las que definí.
*
*******************************************************************************************/

//We're going to implement the first version of the engine API

	engine->engine.initialize = motor_initialize; //Devuelve bien el Buddy por lo menos.
	engine->engine.destroy = motor_destroy;
	engine->engine.allocate = motor_allocate; //Tiene concurrencia
	engine->engine.remove = motor_item_delete;
	engine->engine.release = motor_item_release;
	engine->engine.get = motor_get; //Devuelve, hay que ver eso de los tipos.
	engine->engine.store = motor_store;
	engine->engine.flush = motor_flush; //Devuelve bien.
	engine->get_server_api = get_server_api;
	engine->engine.get_item_info = motor_get_item_info;
	engine->engine.get_info = motor_get_info; //No importa
	engine->engine.get_stats = motor_get_stats; //No importa
	engine->engine.reset_stats = motor_reset_stats; //No importa
	engine->engine.unknown_command = motor_unknown_command; //No importa
	engine->engine.item_set_cas = motor_item_set_cas; //No importa

	/*
	 * memcached solo sabe manejar la estructura ENGINE_HANDLE
	 * el cual es el primer campo de nuestro t_motor_
	 * El puntero de engine es igual a &engine->engine
	 *
	 * Retornamos nuestro engine a traves de la variable handler
	 */
	*handle = (ENGINE_HANDLE*) engine;

	return (ENGINE_SUCCESS);
}

static ENGINE_ERROR_CODE motor_initialize(ENGINE_HANDLE* handle,const char* config_str) {
	/*
	 * En todas las funciones vamos a recibir un ENGINE_HANDLE* handle, pero en realidad
	 * el puntero handler es nuestra estructura t_dummy_ng
	 */

	t_motor_ng *engine = (t_motor_ng*) handle;
	/*
	 * En la variable config_str nos llega la configuración en el formato:
	 *
	 * 	cache_size=1024;chunk_size=48; etc ...
	 *
	 * La función parse_config recibe este string y una estructura que contiene
	 * las keys que debe saber interpretar, los tipos y los punteros de las variables donde tiene
	 * que almacenar la información.
	 *
	 * Más abajo viene lo de traer del archivo de configuración mío.
	 */
	if (config_str != NULL) {
		struct config_item items[] = {
				{ .key = "cache_size", .datatype = DT_SIZE, .value.dt_size = &engine->config.cache_max_size},
				{ .key = "chunk_size", .datatype = DT_SIZE, .value.dt_size = &engine->config.chunk_size},
				{ .key = "item_size_max", .datatype = DT_SIZE, .value.dt_size =	&engine->config.block_size_max },
				{ .key = NULL }
			};

		parse_config(config_str, items, NULL);
	}

	if (engine->config.cache_max_size != 0){
		Tam_Cache_Max =(uint64_t) engine->config.cache_max_size;

		printf ("\nTamaño Cache: %lld\n", Tam_Cache_Max);
	}else{

		Tam_Cache_Max =engine->config.block_size_max;
		printf ("\nTamaño Cache: %lld\n", Tam_Cache_Max);
	}
	Tam_Chunk= (uint64_t) engine->config.chunk_size;

	printf ("\nTamaño Chunk: %d\n", Tam_Chunk);




	if (config != NULL) {
		if (config_has_property(config, "Algoritmo_Busqueda")) {
			Algoritmo_Allocacion = config_get_int_value(config, "Algoritmo_Busqueda");
		}
		if (config_has_property(config, "Frecuencia_Compactacion")) {
			Frecuencia_Compactacion = config_get_int_value(config,	"Frecuencia_Compactacion");
		}
		if (config_has_property(config, "Administracion_Memoria")) {
			Esquema_Administracion_Memoria = config_get_int_value(config,"Administracion_Memoria");
		}
		if (config_has_property(config, "Algoritmo_Eliminacion")) {
			Algoritmo_Eliminacion = config_get_int_value(config, "Algoritmo_Eliminacion");
		}
	}
	if (Algoritmo_Allocacion == 1){
		log_debug(Archivo_Log,"Algoritmo de Allocacion: First Fit");
	}else{
		log_debug(Archivo_Log,"Algoritmo de Allocacion: Best Fit");
	}

	log_debug(Archivo_Log,"Frecuencia Compactacion: %d", Frecuencia_Compactacion);

	if (Esquema_Administracion_Memoria == 1){

			log_debug(Archivo_Log,"Esquema Administración: Compactación");
	}else{
			log_debug(Archivo_Log,"Esquema Administración: Buddy System");
	}

	if (Algoritmo_Eliminacion == 1){

		log_debug(Archivo_Log,"Algoritmo_Eliminación: FIFO");
	}else{
		log_debug(Archivo_Log,"Algoritmo_Eliminación: LRU");
	}


	/***********************************************************************************
	 *
	 * Según es buddy o compactación, inicializa (allocación inicial) de distinta forma.
	 *
	 ***********************************************************************************/

	p_Compactacion_Inicializar = &compactacion_Inicializar;
	p_Buddy_Inicializar = &buddy_Inicializar;
	p_Estructura_A_Inicializar = Esquema_Administracion_Memoria ?   p_Compactacion_Inicializar  : p_Buddy_Inicializar;


		RAM = malloc(Tam_Cache_Max); //Independiente de la estructura.

		mlock(RAM, Tam_Cache_Max);


		p_Estructura_A_Inicializar();
	/*
	 * Registro de la SIGUSR1. El registro de signals debe ser realizado en la función initialize
	 */
	signal(SIGUSR1, dummp);

	return (ENGINE_SUCCESS);
}

/*
 * ************************************* Funciones Dummy *************************************
 */

static ENGINE_ERROR_CODE motor_get_stats(ENGINE_HANDLE* handle, const void* cookie, const char* stat_key, int nkey, ADD_STAT add_stat) {

//log_debug(Archivo_Log,"Entra a motor_get_stats");

	return (ENGINE_SUCCESS);
}

static void motor_reset_stats(ENGINE_HANDLE* handle, const void *cookie) {

//log_debug(Archivo_Log,"Entra a motor_reset__stats");


}

static ENGINE_ERROR_CODE motor_unknown_command(ENGINE_HANDLE* handle, const void* cookie, protocol_binary_request_header *request, ADD_RESPONSE response) {

//log_debug(Archivo_Log,"Entra a motor_unkwnown_command");
	return (ENGINE_ENOTSUP);
}

static void motor_item_set_cas(ENGINE_HANDLE *handle, const void *cookie, item* item, uint64_t val) {

}

/*
 * Esta función es la que se llama cuando el engine es destruido
 */
static void motor_destroy(ENGINE_HANDLE* handle, const bool force) {
//log_debug(Archivo_Log,"Busca entrar al lock de motor_destroy");
	//pthread_rwlock_wrlock(&Lockeo);
//log_debug(Archivo_Log,"Ha entrado al lock de motor_destroy");

	if (Esquema_Administracion_Memoria == 0){
		free(Antecesores);
	}
	free(RAM);
	free(Vector_Items);
	free(handle);
	free(config);
//log_debug(Archivo_Log,"Busca salir del lock de motor_destroy");
	//pthread_rwlock_unlock(&Lockeo);
	//printf("\nContador_Lock: %d",--contador_Lock);
//log_debug(Archivo_Log,"Ha salido del lock de motor_destroy");
}

static ENGINE_ERROR_CODE motor_allocate(ENGINE_HANDLE *handler,	const void* cookie, item **iteme, const void* key, const size_t nkey,
		const size_t nbytes, const int flags, const rel_time_t exptime) {
	//log_debug(Archivo_Log,"ALLOCATE: Se busca conseguir el lock");
	pthread_rwlock_wrlock(&Lockeo); //Lockeo 1
	//log_debug(Archivo_Log,"ALLOCATE: Se consiguió el lock");
	uint64_t Tam = (uint64_t) nbytes;
	int Longitud_Key = (int) nkey;

	t_Item* Item_Elegido = NULL;
	log_debug(Archivo_Log,"Operación SET: La key es %s", key);
				if (Tam > Tam_Cache_Max) {
					log_debug(Archivo_Log,"Operación SET: El tamaño pedido es mayor a la capacidad total disponible");
						return (ENGINE_E2BIG);
					}
	p_Buddy_Allocate = &buddy_allocate;
	p_Compactacion_Allocate = &compactacion_allocate;

	p_Esquema_Allocate = Esquema_Administracion_Memoria == COMPACTACION ?   p_Compactacion_Allocate  : p_Buddy_Allocate;
	Item_Elegido= algoritmo_Allocate_Elegido(handler, cookie, key,nbytes, p_Esquema_Allocate, Longitud_Key);


	*iteme = (item *)  Item_Elegido;
	Fallas=0;

	//log_debug(Archivo_Log,"SET: Se busca liberar el lock del allocate");
	//	pthread_rwlock_unlock(&Lockeo) ;
	//	log_debug(Archivo_Log,"SET: Se libera lock del allocate");

	return (ENGINE_SUCCESS);
}

static ENGINE_ERROR_CODE motor_store(ENGINE_HANDLE *handle, const void *cookie,	item* item, uint64_t *cas, ENGINE_STORE_OPERATION operation,uint16_t vbucket) {
	//log_debug(Archivo_Log,"Entra del Store");

	t_Item *Actual = (t_Item*) item;
	Actual->stored = STORED;

	//log_debug(Archivo_Log,"En Store se busca liberar el lock del allocate");
	pthread_rwlock_unlock(&Lockeo) 	; //Deslockeo 4.c, 70.b y 90.b
//			printf("\n Store: Contador_Lock: %d\n",--contador_Lock);
	//log_debug(Archivo_Log,"En Store se libera lock del allocate");
	return (ENGINE_SUCCESS);
}
static ENGINE_ERROR_CODE motor_flush(ENGINE_HANDLE* handle, const void* cookie,	time_t when) {

//log_debug(Archivo_Log,"Busca entrar al lock del flush");
pthread_rwlock_wrlock(&Lockeo);
//printf("\nFlush: Contador_Lock: %d\n",++contador_Lock);
//log_debug(Archivo_Log,"Ha entrado al lock del flush");*/
	uint64_t i = 0;
	//limpio toda la cache
	if (Esquema_Administracion_Memoria == 0){
		while (i <= cantidad - 1) {
				Vector_Items[i].data = NULL;
				Vector_Items[i].Libre = LIBRE;
				Vector_Items[i].exptime = 0;
				Vector_Items[i].stored = NO_STORED;
				Vector_Items[i].tam_data = Tam_Chunk;
				Vector_Items[i].tam_disco = Tam_Chunk;
				Vector_Items[i].long_key = 0;
				memset(Vector_Items[i].key, '\0', 40);
				Vector_Items[i].data = NULL;
				Vector_Items[i].id = 0;
				Vector_Items[i].Cant_Antecesores = 0;
				__sync_fetch_and_add(&i,1);
				}


		Vector_Items[0].id = 1;
		Vector_Items[0].Cant_Antecesores = 0;
		}else{

		while (i <= cantidad - 1) {

		Vector_Items[i].data = NULL;
		Vector_Items[i].Libre = LIBRE;
		Vector_Items[i].exptime = 0;
		Vector_Items[i].tam_disco = 0;
		Vector_Items[i].long_key = 0;
		Vector_Items[i].stored = NO_STORED;
		Vector_Items[i].tam_data = Tam_Chunk;
		memset(Vector_Items[i].key, '\0', 40);
		Vector_Items[i].data = NULL;
		__sync_fetch_and_add(&i,1);
		 }
		}

	Items_Ocupados = 0;
	Primer_Item_Ocupado = NULL;
		Vector_Items[0].data = RAM;
		Vector_Items[0].tam_data = Tam_Cache_Max;
		Vector_Items[0].final = RAM + Tam_Cache_Max-1;
		Vector_Items[0].stored = NO_STORED;
		Vector_Items[0].exptime = 0;
		Vector_Items[0].Libre = LIBRE;
		memset(Vector_Items[0].key, '\0', 40);
		Vector_Items[0].tam_disco = Tam_Cache_Max;
		Vector_Items[0].long_key = 0;

		log_debug(Archivo_Log,"Operación FLUSH concretada.");


//log_debug(Archivo_Log,"Busca salir del lock del flush");
	pthread_rwlock_unlock(&Lockeo);
	//printf("\nContador_Lock: %d",--contador_Lock);
//log_debug(Archivo_Log,"Ha salido del lock del flush");*/
	return (ENGINE_SUCCESS);
}

static ENGINE_ERROR_CODE motor_item_delete(ENGINE_HANDLE* handle,	const void* cookie, const void* key, const size_t nkey, uint64_t cas,
		uint16_t vbucket) {

	
		//log_debug(Archivo_Log,"Busca entrar al lock de motor_delete");

		pthread_rwlock_wrlock(&Lockeo) ;
	log_debug(Archivo_Log,"Operación DELETE: La key es %s", key);
		//printf("\nContador_Lock: %d",++contador_Lock);
		//log_debug(Archivo_Log,"Ha entrado al lock de motor_delete");


	key = (char*) key;
    int	Longitud_Key = (int) nkey;
    uint64_t i = 0;



	int key_mayor=0;
	while (i <= cantidad - 1) {
		if (Vector_Items[i].long_key < Longitud_Key){
			key_mayor= 	Longitud_Key;
		}else{
			key_mayor= 	Vector_Items[i].long_key;
		}

			if ((memcmp(Vector_Items[i].key, key, key_mayor) == 0)) {
			break;
			}
			__sync_fetch_and_add(&i,1);
		}

	if (i == cantidad ) {

		log_debug(Archivo_Log,"Operación DELETE: No encontró ítems con esa key");

		//log_debug(Archivo_Log,"Busca salir del lock de motor_delete");
		pthread_rwlock_unlock(&Lockeo);

		//log_debug(Archivo_Log,"Ha salido del lock de motor_delete");



		return (ENGINE_KEY_ENOENT);
	}

	Vector_Items[i].stored = NO_STORED;
	t_Item* Item = &Vector_Items[i];
	t_Item** p_Item = &Item;
	Item->Libre = LIBRE;

		Items_Ocupados--;
		if (Primer_Item_Ocupado == Item) {

			if (contar_Items_Ocupados() != 0){
			Primer_Item_Ocupado = buscar_Primer_Item_Ocupado();
			}
		}

		if (Esquema_Administracion_Memoria == 0){
		Primer_Release = 1;
		buddy_release(p_Item);


		memset((*p_Item)->key, '\0', LONGITUD_MAX_KEY);
        (*p_Item)->long_key=0;



		//log_debug(Archivo_Log,"Busca salir del lock de motor_delete");

		pthread_rwlock_unlock(&Lockeo) ;

		//log_debug(Archivo_Log,"Ha salido del lock de motor_delete");


			return(ENGINE_SUCCESS);
		}


		Primera_Union=1;



		intentar_Unir_Libres(p_Item);
		memset((*p_Item)->key, '\0', LONGITUD_MAX_KEY);
                (*p_Item)->long_key=0;

                


		//log_debug(Archivo_Log,"Busca salir del lock de motor_delete");
		pthread_rwlock_unlock(&Lockeo) ;
		//printf("\nContador_Lock: %d",--contador_Lock);
		//log_debug(Archivo_Log,"Ha salido del lock de motor_delete");


	return (ENGINE_SUCCESS);

}

static void motor_item_release(ENGINE_HANDLE *handler, const void *cookie,item* item) {



}

/*
 * Esto retorna algo de información la cual se muestra en la consola
 */
static const engine_info* motor_get_info(ENGINE_HANDLE* handle) {



	static engine_info info = {
	          .description = "Engine de Grupo Pio ",
	          .num_features = 0,
	          .features = {
	               [0].feature = ENGINE_FEATURE_LRU,
	               [0].description = ""
	           }
	};

	return (&info);
}

static bool motor_get_item_info(ENGINE_HANDLE *handler, const void *cookie,	const item* item, item_info *item_info) {
	// casteamos de item*, el cual es la forma generica en la cual memcached trata a nuestro tipo de item, al tipo
	// correspondiente que nosotros utilizamos

	//log_debug(Archivo_Log,"Entra al motor_get_item_info");


	t_Item *it = (t_Item*) item;

	if (item_info->nvalue < 1) {
		return (false);
	}

	item_info->cas = 0; /* Not supported */
	item_info->clsid = 0; /* Not supported */
	item_info->exptime = 0;
	item_info->flags = 0;
	item_info->key = it->key;
	item_info->nkey = it->long_key;
	item_info->nbytes = it->tam_data; /* Total length of the items data */
	item_info->nvalue = 1; /* Number of fragments used ( Default ) */
	item_info->value[0].iov_base = it->data; /* Hacemos apuntar item_info al comienzo de la info */
	item_info->value[0].iov_len = it->tam_data; /* Le seteamos al item_info el tamaño de la información */

	//log_debug(Archivo_Log,"Sale del motor_get_item_info");

	return (true);
}


static ENGINE_ERROR_CODE motor_get(ENGINE_HANDLE *handle, const void* cookie,item** iteme, const void* key, const int nkey, uint16_t vbucket) {

	//log_debug(Archivo_Log,"GET: Se busca conseguir el lock");
	pthread_rwlock_wrlock(&Lockeo) ;
	//log_debug(Archivo_Log,"GET: Se consiguió el lock");
	char* mi_key = (char*) key;

	t_Item* Resultado;




	log_debug(Archivo_Log,"Operación GET: La key es %s", mi_key);


	int Tamanio_Key = nkey;

	Resultado = buscar_Item_con_Key(mi_key, Tamanio_Key);



	if (Resultado == NULL) {

                log_debug(Archivo_Log,"Operación GET: No hay items con la key solicitada");
          //      log_debug(Archivo_Log,"GET: Se busca liberar el lock");
            	pthread_rwlock_unlock(&Lockeo) ;
            //	log_debug(Archivo_Log,"GET: Se liberó el lock");
        return (ENGINE_KEY_ENOENT);
        }

	if (Algoritmo_Eliminacion == 0) {
		    __sync_fetch_and_add(&Contador_Global,1);
		    Resultado->exptime = Contador_Global;
		}
	//log_debug(Archivo_Log,"GET: Se busca liberar el lock");
	pthread_rwlock_unlock(&Lockeo) ;
	//log_debug(Archivo_Log,"GET: Se liberó el lock");

	*iteme = (item *) Resultado;
	return (ENGINE_SUCCESS);

}

t_Item* first_fit(uint64_t Tamanio_Exigido, int Tamanio_Menor_A_Chunk) {
	uint64_t i = 0;

	t_Item *Libre;

	while (i <= cantidad - 1) {
	if (Vector_Items[i].Libre == LIBRE	&& Vector_Items[i].tam_disco >= Tamanio_Exigido && Vector_Items[i].data != NULL) {
			break;
		}
	__sync_fetch_and_add(&i,1);

	}

	if (i <= cantidad - 1) { //Se encontró alguno que satisface.
		Vector_Items[i].Libre = OCUPADO;

		Libre = buscar_Item_Libre();
		if (Libre == NULL) {
			__sync_fetch_and_add(&Contador_Global,1);
			Vector_Items[i].exptime = Contador_Global;

		return (&Vector_Items[i]);	}

		Libre->tam_data = Vector_Items[i].tam_disco - Tamanio_Exigido;
		Libre->tam_disco =Libre->tam_data;


		Vector_Items[i].tam_disco = Tamanio_Exigido;
		if (Tamanio_Menor_A_Chunk != 0){
			Vector_Items[i].tam_data =Tamanio_Menor_A_Chunk;
		}else{
			Vector_Items[i].tam_data =Tamanio_Exigido;
		}

		Vector_Items[i].final = Vector_Items[i].data + Vector_Items[i].tam_disco-1;

		__sync_fetch_and_add(&Contador_Global,1);
		Vector_Items[i].exptime = Contador_Global;
		Libre->data = Vector_Items[i].final + 1;
		if (Libre->tam_disco !=0){
		Libre->final = Libre->data + Libre->tam_disco -1 ;}else{Libre->data= NULL;Libre->final = NULL ; }
	} else {

		log_debug(Archivo_Log,"Operación Set: Ninguna partición satisface el pedido.");
		return (NULL);
	}

	if (Libre->tam_disco < Tam_Chunk && Items_Ocupados == cantidad - 3) {
				compactar();

				Items_Ocupados = contar_Items_Ocupados();
				log_info(Archivo_Log,"Se hace compactación excepcional para evitar la escasez de ítems libres.");
			}
	return (&Vector_Items[i]);
}


t_Item* best_fit(uint64_t Tamanio_Exigido, int Tamanio_Menor_A_Chunk) {
	uint64_t Menor;
	t_Item *Libre, *Mejor_Eleccion;
	uint64_t i = 0;
	Menor = Tam_Cache_Max;
	Mejor_Eleccion = NULL;

	while (i <= cantidad - 1) {

	if (Vector_Items[i].Libre == LIBRE && Vector_Items[i].data != NULL	&& Vector_Items[i].tam_disco >= Tamanio_Exigido	&& Vector_Items[i].tam_disco <= Menor) {
			Mejor_Eleccion = &Vector_Items[i];
			Menor = Vector_Items[i].tam_disco;
		}
	__sync_fetch_and_add(&i,1);
	}

	if (Mejor_Eleccion == NULL) {
		log_debug(Archivo_Log,"Operación Set: Ninguna partición satisface el pedido.");
		return (NULL);} else{

		Mejor_Eleccion->Libre = OCUPADO;

		/*if (Mejor_Eleccion->final == Ultimo_Byte && Mejor_Eleccion->tam_disco == Tamanio_Exigido ){
				Mejor_Eleccion->exptime = Contador_Global++;

		}else{*/

		Libre = buscar_Item_Libre();

		if (Libre == NULL ) {
			__sync_fetch_and_add(&Contador_Global,1);
			Mejor_Eleccion->exptime = Contador_Global;
			return (Mejor_Eleccion);}

		Libre->tam_data = Mejor_Eleccion->tam_disco - Tamanio_Exigido;
		Libre->tam_disco=Libre->tam_data;

		Mejor_Eleccion->tam_disco = Tamanio_Exigido;
		if ( Tamanio_Menor_A_Chunk != 0 ){
		Mejor_Eleccion->tam_data = Tamanio_Menor_A_Chunk;
		}else{
		Mejor_Eleccion->tam_data = Tamanio_Exigido;
		}

		Mejor_Eleccion->final = Mejor_Eleccion->data + Mejor_Eleccion->tam_disco- 1;
		Libre->data = Mejor_Eleccion->final + 1;
		 __sync_fetch_and_add(&Contador_Global,1);
		Mejor_Eleccion->exptime =Contador_Global;
		if (Libre->tam_disco !=0){
			Libre->final = Libre->data + Libre->tam_disco -1 ;
			}else{
				Libre->data = NULL;
				Libre->final = NULL ;
		}
	if (Libre->tam_data < Tam_Chunk && Items_Ocupados == cantidad - 3) {
		log_info(Archivo_Log,"Operación Set: Compactación excepcional para evitar la escasez de ítems libres.");
				compactar();Items_Ocupados = contar_Items_Ocupados();	}
	}


	return (Mejor_Eleccion);
}

t_Item* buscar_Item_Libre() {
	uint64_t i = 0;
		t_Item* Libre;
		while (i <= cantidad - 1) {
			if (Vector_Items[i].Libre == LIBRE && Vector_Items[i].data == NULL ) {

				break;
			}
			__sync_fetch_and_add(&i,1);
		}
		if (i == cantidad) {

			return (NULL);
		}
		Libre = &Vector_Items[i];
		return (Libre);
	}

void eliminar_Particion(ENGINE_HANDLE* handler, const char* cookie) {
	uint64_t i;


t_Item* mas_Antiguo;
mas_Antiguo=NULL;

	uint64_t Menor_Valor=Contador_Global + 10;

	i = 0;
	while (i <= cantidad - 1) {
		if (Vector_Items[i].exptime <= Menor_Valor&& Vector_Items[i].exptime != 0 && Vector_Items[i].Libre == OCUPADO) {
			mas_Antiguo = &Vector_Items[i];
			Menor_Valor = mas_Antiguo->exptime;
		}
		__sync_fetch_and_add(&i,1);
	}
	if (Algoritmo_Eliminacion == 1){
	log_debug(Archivo_Log,"Elimina Partición según algoritmo de reemplazo FIFO. La víctima es %s", mas_Antiguo->key);
	}else{
	log_debug(Archivo_Log,"Elimina Partición según algoritmo de reemplazo LRU. La víctima es %s", mas_Antiguo->key);
	}
	//log_debug(Archivo_Log,"Eliminar Partición manda a %s", mas_Antiguo->key);
	pthread_rwlock_unlock(&Lockeo);
	motor_item_delete(handler, cookie,mas_Antiguo->key, mas_Antiguo->long_key, 0, 0);


}
t_Item* algoritmo_Allocate_Elegido (ENGINE_HANDLE* handler, const void* cookie,const char* key,const size_t nbytes, t_Item* (*p_funcion_allocate) (ENGINE_HANDLE*, const void*,const char*,const size_t, int), int nkey){

	//log_debug(Archivo_Log,"Entra a algoritmo_Allocate_Elegido");


	t_Item* Item_Elegido;
	Item_Elegido = ((*p_funcion_allocate)(handler, cookie, key, nbytes,nkey ));

	//log_debug(Archivo_Log,"Sale del algoritmo_Allocate_Elegido");

	return (Item_Elegido);
}
t_Item* algoritmo_Elegido(uint64_t Tamanio_Exigido, int Tamanio_Menor_A_Chunk, t_Item* (*p_funcion_fit)(uint64_t, int)) {

	t_Item* Item_Elegido = ((*p_funcion_fit)(Tamanio_Exigido, Tamanio_Menor_A_Chunk));

	return (Item_Elegido);
}

void intentar_Unir_Libres(t_Item** p_item) {

	//log_debug(Archivo_Log,"Entra a intentar_Unir_Libres");


	t_Item *Siguiente, *Anterior;

	Anterior = buscar_Anterior(p_item);

	if (Anterior != NULL && Anterior->Libre == LIBRE && Anterior->data!= NULL) {
		(*p_item)->tam_disco = Anterior->tam_disco + (*p_item)->tam_disco;
		(*p_item)->data = Anterior->data;
		(*p_item)->tam_data =(*p_item)->tam_disco;
		(*p_item)->final = (*p_item)->data + (*p_item)->tam_disco - 1;
		Anterior->data = NULL;
		Anterior->tam_data = Tam_Chunk;
		Anterior->tam_disco = Tam_Chunk;
		Anterior->long_key = 0;
		Anterior->final = NULL;
		Anterior->stored = NO_STORED;
		Anterior->Libre = LIBRE;
		Anterior->exptime = 0;

		Primera_Union=0;

	}else{
	Siguiente = buscar_Siguiente(p_item);

if (Siguiente != NULL && Siguiente->Libre == LIBRE && Siguiente->data!= NULL) {
			(*p_item)->tam_disco = Siguiente->tam_disco + (*p_item)->tam_disco;
			(*p_item)->tam_data =(*p_item)->tam_disco;
			(*p_item)->final = (*p_item)->data + (*p_item)->tam_disco - 1;
			Siguiente->tam_data = Tam_Chunk;
			Siguiente->final = NULL;
			Siguiente->stored = NO_STORED;
			Siguiente->Libre = LIBRE;
			Siguiente->exptime = 0;
			Siguiente->tam_disco = Tam_Chunk;
			Siguiente->long_key = 0;
			Siguiente->data = NULL;
			Primera_Union=0;

	}else{
		if (Primera_Union == 1){
			(*p_item)->stored = NO_STORED;
			(*p_item)->Libre = LIBRE;
			(*p_item)->exptime = 0;

				}
						return;
			}}
intentar_Unir_Libres(p_item);
}
void buddy_release(t_Item** p_item) {

	//log_debug(Archivo_Log,"Entra a buddy_release");


	t_Item *Siguiente, *Anterior;

t_Item* Elegido;

	Siguiente = buscar_Siguiente(p_item);
	if (Siguiente != NULL && Siguiente->Libre==LIBRE && Siguiente->tam_disco == (*p_item)->tam_disco && *(Siguiente->Padres + Siguiente->Cant_Antecesores - 1)== *((*p_item)->Padres + (*p_item)->Cant_Antecesores - 1)) {
		(*p_item)->tam_disco= Siguiente->tam_disco+ (*p_item)->tam_disco;
		(*p_item)->tam_data =0;
		(*p_item)->final = Siguiente->final;
		(*p_item)->id = *((*p_item)->Padres + (*p_item)->Cant_Antecesores - 1);
		(*p_item)->Cant_Antecesores--;


		Siguiente->id = 0;
		Siguiente->tam_data = 0;
		Siguiente->tam_disco =  0;
		Siguiente->long_key =  0;
		Siguiente->data = NULL;
		Siguiente->final = NULL;
		Siguiente->stored = NO_STORED;
		Siguiente->Libre = LIBRE;
		Siguiente->exptime = 0;
		Siguiente->Cant_Antecesores = 0;
		Elegido= (*p_item);
		if (Elegido->Cant_Antecesores == 0 ) {
						*(Elegido->Padres)= -1;
					}
		Primer_Release= 0;
	} else {

		Anterior = buscar_Anterior(p_item);
		if (Anterior != NULL && Anterior->Libre==LIBRE && Anterior->tam_disco == (*p_item)->tam_disco && *(Anterior->Padres + Anterior->Cant_Antecesores - 1)	== *((*p_item)->Padres + (*p_item)->Cant_Antecesores - 1)) {

			(*p_item)->tam_disco  = Anterior->tam_disco  + (*p_item)->tam_disco ;
			(*p_item)->data = Anterior->data;
			(*p_item)->id = *((*p_item)->Padres + (*p_item)->Cant_Antecesores - 1);
			(*p_item)->Cant_Antecesores--;
			(*p_item)->tam_data =0;
			Anterior->id = 0;
			Anterior->tam_data = 0;
			Anterior->tam_disco =  0;
			Anterior->long_key =  0;
			Anterior->data = NULL;
			Anterior->final = NULL;
			Anterior->stored = NO_STORED;
			Anterior->Libre = LIBRE;
			Anterior->exptime = 0;
			Anterior->Cant_Antecesores = 0;

			Elegido= (*p_item);
			if (Elegido->Cant_Antecesores == 0 ) {
				*(Elegido->Padres)= -1;
			}
			Primer_Release= 0;
		} else {
			if (Primer_Release == 1){

				(*p_item)->stored = NO_STORED;
				(*p_item)->Libre = LIBRE;
				(*p_item)->exptime = 0;

			}
					return;
		}
	}

	buddy_release(p_item);

	return;
}
t_Item* buscar_Item_A_Primera_Particion() {

	//log_debug(Archivo_Log,"Entra a buscar_Item...");

	uint64_t i = 0;
	while (i <= cantidad - 1) {
		if (Vector_Items[i].data == RAM) {

			break;
		}
		__sync_fetch_and_add(&i,1);
	}
	if (cantidad == i) {
		return (NULL);
	}
	return (&Vector_Items[i]);
}

t_Item* buscar_Primer_Item_Ocupado(){
	//log_debug(Archivo_Log,"Entra a buscar_Primer...");

	uint64_t j = 0;
	while (j <= cantidad-1) {
		if (Vector_Items[j].data != NULL  && Vector_Items[j].Libre == OCUPADO ){
		break;
		}
		__sync_fetch_and_add(&j,1);
	}
	if (j == cantidad){
		return (NULL);
	}

	return (&Vector_Items[j]);
}
t_Item* buscar_Siguiente(t_Item** p_item) {

	//log_debug(Archivo_Log,"Entra a buscar_Siguiente");

	uint64_t j = 0;
	t_Item* Libre = (*p_item);
	if (Esquema_Administracion_Memoria == BUDDY_SYSTEM && Viene_del_Dummp == 0){
	while (j <= cantidad-1) {
		if (Vector_Items[j].data == (*p_item)->final + 1 && Vector_Items[j].id!=0 ) {
			break;}
		__sync_fetch_and_add(&j,1);
	}
	}else{
		while (j <= cantidad-1) {
			if (Vector_Items[j].data == (*p_item)->final + 1 ) {
				break;	}
			__sync_fetch_and_add(&j,1);
		}
	}
	if (j == cantidad) {
		return (NULL);
	}
	(Libre) = &Vector_Items[j];
	return (Libre);
}
t_Item* buscar_Anterior(t_Item** p_item) {

	//log_debug(Archivo_Log,"Entra a buscar_Anterior");

	uint64_t j = 0;
	t_Item* Libre = (*p_item);
	if (Esquema_Administracion_Memoria == BUDDY_SYSTEM){
		while (j <= cantidad) {
			if (Vector_Items[j].final == (*p_item)->data - 1 && Vector_Items[j].id!=0 ) {
				break;
			}
			__sync_fetch_and_add(&j,1);
		}
		}else{
			while (j <= cantidad) {
				if (Vector_Items[j].final == (*p_item)->data - 1 ) {

				break;
				}
				__sync_fetch_and_add(&j,1);
			}
		}
		if (j == cantidad) {
		return (NULL);
		}
		(Libre) = &Vector_Items[j];
		return (Libre);
	}


void compactar() {

	//log_debug(Archivo_Log,"Entra a compactar");


	t_Item** p_Primera, **p_Siguiente, **p_Anterior;
	t_Item *Siguiente, *Anterior, *Primera;
	p_Primera = &Primera;

	Anterior = Primera = buscar_Item_A_Primera_Particion();

	if (Primera == NULL) { //Si no hay ninguno apuntando a la primera, le asigno algún ítem que apunte.
		Primera = &Vector_Items[cantidad];
		Primera->Libre = LIBRE;
		Primera->data = RAM;
		Primera->tam_disco = Primer_Item_Ocupado->data - RAM;
		Primera->tam_data = Primera->tam_disco;
		Primera->final = Primera->data + Primera->tam_disco - 1;
		p_Primera = &Primera;
		algoritmo_Compactacion(p_Primera);
	}

	Anterior = Primera;
	Siguiente = Primera;
	p_Siguiente = &Siguiente;
	p_Anterior = &Anterior;
	Siguiente = buscar_Siguiente(p_Anterior);


	if ((*p_Siguiente)->Libre == LIBRE && (*p_Anterior)->Libre == LIBRE) { //Si el primero está libre y el siguiente también, los uno.
		intentar_Unir_Libres(p_Anterior);

		Siguiente = Anterior;

		Siguiente = buscar_Siguiente(p_Anterior); //Ahora busco el siguiente de lo unido.
	}

	while (Siguiente != NULL) { //Empieza while para ver hasta dónde hay ítems activos.
		if ((*p_Siguiente)->Libre == LIBRE) { //Abre una de las posibilidades, el Siguiente está libre.
			if ((*p_Anterior)->Libre == LIBRE) { //Son 2 posibilidades, empezamos con que la anterior estuviese libre.
				intentar_Unir_Libres(p_Anterior); //Los une y busca el siguiente
				Siguiente = Anterior;

				Siguiente = buscar_Siguiente(p_Anterior);
				if (Siguiente == NULL) {
					break;
				}
			} else { //Si el anterior estaba ocupado, no hay que unir nada, se compacta (intercambian lugar)
				Anterior = Siguiente;
				Siguiente = buscar_Siguiente(p_Anterior); //Busca el que viene.
			}
		}
		if ( Siguiente != NULL){
		if ((*p_Siguiente)->Libre != LIBRE) { //Qué pasa cuando el siguiente está ocupado.
			if ((*p_Anterior)->Libre == LIBRE) { //Si el anterior estaba libre, intercambian.
				algoritmo_Compactacion(p_Anterior);

				Siguiente = buscar_Siguiente(p_Anterior);
			} else {
				Anterior = Siguiente;
				Siguiente = buscar_Siguiente(p_Anterior);
			}
		}
	}
	}
}
void algoritmo_Compactacion(t_Item** p_Siguiente) {

	//log_debug(Archivo_Log,"Entra a algoritmo_Compactacion");

	t_Item* Siguiente = (*p_Siguiente);
	t_Item* Libre = Siguiente;
	t_Item** p_Libre = &Libre;
	p_Siguiente = &Siguiente;
	Siguiente = buscar_Siguiente(p_Libre);
	if ((*p_Libre)->tam_disco <= (*p_Siguiente)->tam_disco) {
		memcpy((*p_Libre)->data, (*p_Siguiente)->data, (*p_Libre)->tam_disco);
		memcpy((*p_Siguiente)->data,(*p_Siguiente)->data + (*p_Libre)->tam_disco,(*p_Siguiente)->tam_disco- (*p_Libre)->tam_disco);
		(*p_Siguiente)->data = (*p_Libre)->data;
		(*p_Siguiente)->final = (*p_Siguiente)->data + (*p_Siguiente)->tam_disco	- 1;
	} else {
		memcpy((*p_Libre)->data, (*p_Siguiente)->data,	(*p_Siguiente)->tam_disco);
		(*p_Siguiente)->data = (*p_Libre)->data;
		(*p_Siguiente)->final = (*p_Siguiente)->data + (*p_Siguiente)->tam_disco	- 1;
	}
	(*p_Libre)->data = (*p_Siguiente)->final + 1;
	(*p_Libre)->final = (*p_Libre)->data + (*p_Libre)->tam_disco - 1;
}

t_Item* buscar_Item_con_Key(char* key, int nkey) {

	//log_debug(Archivo_Log,"Entra a buscar_Item_con_Key");

	uint64_t i;
	i = 0;


	int key_mayor=0;
	while (i <= cantidad - 1) {

		if (Vector_Items[i].long_key < nkey){
			key_mayor= 	nkey;
		}else{
			key_mayor= 	Vector_Items[i].long_key;
		}

		if (Vector_Items[i].Libre == OCUPADO && (memcmp(Vector_Items[i].key, key, key_mayor) == 0)&& Vector_Items[i].long_key == nkey ) {

			return (&Vector_Items[i]);

		}
		__sync_fetch_and_add(&i,1);
	}

	if (i == cantidad) {

		return NULL;

	}

	return (&Vector_Items[i]);
}

uint64_t contar_Items_Ocupados() {
	uint64_t i = 0;
	uint64_t numero = 0;

	for (i = 0; i <= cantidad - 1; __sync_fetch_and_add(&i,1)) {
		if (Vector_Items[i].data != NULL && Vector_Items[i].Libre == OCUPADO) {
			__sync_fetch_and_add(&numero,1);
		}
	}

	return (numero);
}

void dummp(int signal) { //Tiene que escribir en un archivo.
	Viene_del_Dummp =1;

	log_debug(Archivo_Log,"Operación Dummp.");
	uint64_t i = 0;
	t_Item* Siguiente;
	t_Item** p_Siguiente;
	Siguiente = buscar_Item_A_Primera_Particion();
	p_Siguiente = &Siguiente;
	char* Estado;
	Archivo_Dummp = fopen ( "Archivo_Dummp.log", "w" );
	while (Siguiente != NULL) {
		if (Siguiente->Libre ? LIBRE : OCUPADO) {
			Estado = "Libre";
		} else {
			Estado = "Ocupado";
		}


		fprintf(Archivo_Dummp, "\nPartición %lld: %p - %p. %s. Size: %lld bytes. Key: '%s'. Tamaño real: %lld", i,	Siguiente->data, Siguiente->final, Estado, Siguiente->tam_disco,Siguiente->key, Siguiente->tam_data);
		Siguiente = buscar_Siguiente(p_Siguiente);
		__sync_fetch_and_add(&i,1);

	}
	fclose ( Archivo_Dummp );
}
t_Item* compactacion_allocate(ENGINE_HANDLE* handler, const void* cookie,const char* key,const size_t nbytes, int nkey){

	//log_debug(Archivo_Log,"Entra a compactacion_allocate");
	int Tamanio_Menor_A_Chunk = 0;


	uint64_t Tamanio_Exigido = (uint64_t) nbytes;
	if (Tam_Chunk > Tamanio_Exigido){
			Tamanio_Menor_A_Chunk =Tamanio_Exigido;
			Tamanio_Exigido = Tam_Chunk;

			}

	char *mi_key = (char*) key;
	t_Item *Resultado, *Item_Elegido;


	int Tamanio_Key = nkey;

	//log_debug(Archivo_Log,"Busca entrar al primer lock de compactacion_allocate");

	//printf("\n El inicial del compactacion_allocate: Contador_Lock: %d",++contador_Lock);
	//log_debug(Archivo_Log,"Ha entrado al primer lock de compactacion_allocate");*/
	Resultado = buscar_Item_con_Key(mi_key, Tamanio_Key);

	if (Resultado != NULL ) { //Si hay alguno con esa key, la idea es sobreescribirlo.
		if (Resultado->tam_data == Tamanio_Exigido) { //Si es del mismo tamaño, se sobreescribe.
			 __sync_fetch_and_add(&Contador_Global,1);
			Resultado->exptime = Contador_Global;

			log_debug(Archivo_Log,"Operación Set: Encontró un ítem con su key y mismo tamaño. Lo reemplaza");
///			log_debug(Archivo_Log,"Busca salir del primer lock de compactacion_allocate");

	//		printf("\nContador_Lock: %d",--contador_Lock);
		//	log_debug(Archivo_Log,"Ha salido del primer lock de compactacion_allocate");*/
			return(Resultado);
		} else { //Si no es del mismo tamaño, se agarra otro que se acerque a ese tamaño y se borra el anterior.

			//log_debug(Archivo_Log,"Busca salir del primer lock de compactacion_allocate");
			log_debug(Archivo_Log,"Operación Set: Encontró un ítem con su key y distinto tamaño");


			log_debug(Archivo_Log,"SET: Se libera lock del allocate para que lo tome el delete");
			pthread_rwlock_unlock(&Lockeo);
			log_debug(Archivo_Log,"Operación SET: Envía a DELETE La key es %s", Resultado->key);
			motor_item_delete(handler, cookie,Resultado->key, Tamanio_Key, 0, 0);
			log_debug(Archivo_Log,"SET: Se intenta tomar lock del allocate post delete");
			pthread_rwlock_wrlock(&Lockeo);
			log_debug(Archivo_Log,"SET: Se vuelve a tomar lock del allocate post delete");

			}
	}
	char Ya_Compacto=0;

			Item_Elegido = algoritmo_Elegido(Tamanio_Exigido, Tamanio_Menor_A_Chunk,	algoritmo_fit_elegido);

			while (Item_Elegido == NULL) {
				__sync_fetch_and_add(&Fallas,1);

				if (Fallas == Frecuencia_Compactacion && Frecuencia_Compactacion != -1 && Ya_Compacto==0) {


					log_info(Archivo_Log,"Operación Set: Es necesario compactar");
					compactar();
					Primer_Item_Ocupado = buscar_Primer_Item_Ocupado();
					Items_Ocupados = contar_Items_Ocupados();

					Ya_Compacto=1;
					Fallas = 0 ;

					Item_Elegido = algoritmo_Elegido(Tamanio_Exigido, Tamanio_Menor_A_Chunk,algoritmo_fit_elegido);

			}else {


					log_debug(Archivo_Log,"Operación Set: Es necesario eliminar una partición");



					eliminar_Particion(handler,cookie);
					log_debug(Archivo_Log,"SET: Se intenta tomar lock del allocate post delete");
					pthread_rwlock_wrlock(&Lockeo);
					log_debug(Archivo_Log,"SET: Se vuelve a tomar lock del allocate post delete");

					Primer_Item_Ocupado = buscar_Primer_Item_Ocupado();

					log_info(Archivo_Log,"Operación Set: Vuelve a buscar un item tras eliminar particion");
					Ya_Compacto=0;
					Item_Elegido = algoritmo_Elegido(Tamanio_Exigido,Tamanio_Menor_A_Chunk,algoritmo_fit_elegido);
					}

			}

			memcpy(Item_Elegido->key, key, Tamanio_Key);
			Item_Elegido->long_key = Tamanio_Key;

			log_info(Archivo_Log,"Operación Set: Consiguió ítem satisfactorio");

			__sync_fetch_and_add(&Items_Ocupados,1);

			Primer_Item_Ocupado = buscar_Primer_Item_Ocupado();

return(Item_Elegido);
}



t_Item* buddy_allocate(ENGINE_HANDLE* handler, const void* cookie,const char* key,const size_t nbytes, int nkey){


	uint64_t Tamanio_Exigido = (uint64_t) nbytes;

	char* mi_key = (char*) key;

	uint64_t* Aux2, *Aux3;

	int Tamanio_Menor_A_Chunk = 0;

	uint64_t contador;
	uint64_t i = 0;

	t_Item *Elegido, *Resultado, *Colega;

	int Tamanio_Key = nkey;

	if (Tam_Chunk > Tamanio_Exigido){
		Tamanio_Menor_A_Chunk =Tamanio_Exigido;
		Tamanio_Exigido = Tam_Chunk;

		}

	Resultado = buscar_Item_con_Key(mi_key, Tamanio_Key);

	if (Resultado != NULL) { //Encontró uno con su key

		if (Resultado->tam_data == Tamanio_Exigido) { //Miden lo mismo, puedo usar ese

			if (Tamanio_Menor_A_Chunk !=0 ){
				Resultado->tam_data = Tamanio_Menor_A_Chunk;
			}else{
				Resultado->tam_data = Tamanio_Exigido;
			}
			__sync_fetch_and_add(&Contador_Global,1);
			Resultado->exptime = Contador_Global;

			return (Resultado);
		} else {

			if (Resultado->Cant_Antecesores == 0 ) {
						*(Resultado->Padres)= -1;
					}

			}

			log_debug(Archivo_Log,"SET: Se libera lock del allocate para que lo tome el delete");
			pthread_rwlock_unlock(&Lockeo);
			log_debug(Archivo_Log,"Operación SET: Envía a DELETE La key es %s", Resultado->key);
			motor_item_delete(handler, cookie,Resultado->key, Tamanio_Key, 0, 0);
			log_debug(Archivo_Log,"SET: Se intenta tomar lock del allocate post delete");
			pthread_rwlock_wrlock(&Lockeo);
			log_debug(Archivo_Log,"SET: Se vuelve a tomar lock del allocate post delete");




	}else{
	log_debug(Archivo_Log,"Operación Set: No encontró ítems con su key");

	}
	Elegido = NULL;

	while (potencia(2, i) < Tamanio_Exigido) {
		__sync_fetch_and_add(&i,1);
	}



	t_Item* Siguiente;
	t_Item** p_Siguiente= &Siguiente;
	Siguiente=buscar_Item_A_Primera_Particion();
	log_debug(Archivo_Log,"Llega a buscar item a primera partición");

	while (Siguiente != NULL) {


					if (Siguiente->Libre == LIBRE	&& Siguiente->tam_disco >= potencia(2, i) && Siguiente->data != NULL ) { //El tamaño es más grande que el Exigido.

						Elegido = Siguiente;
						break;
					}
					Siguiente = buscar_Siguiente(p_Siguiente);
			}


	if (Elegido == NULL) { //Si no encontró, devuelve Null

		eliminar_Particion(handler,cookie);
		log_debug(Archivo_Log,"SET: Se intenta tomar lock del allocate pre delete");
		pthread_rwlock_wrlock(&Lockeo);
		log_debug(Archivo_Log,"SET: Se vuelve a tomar lock del allocate post delete");
		Primer_Item_Ocupado = buscar_Primer_Item_Ocupado();

		Elegido = buddy_allocate(handler, cookie,mi_key, nbytes, nkey);
		return (Elegido);
	}

	Elegido->Libre = OCUPADO;
	if (Elegido->Cant_Antecesores == 0 ) {
			*(Elegido->Padres)= -1;
		}

	if (Elegido->tam_disco == potencia(2, i) ) { //Si encontró uno que encaja justo, lo devuelve y termina.

	memcpy(Elegido->key, mi_key, Tamanio_Key);
	Elegido->long_key = Tamanio_Key;
	__sync_fetch_and_add(&Contador_Global,1);
	Elegido->exptime = Contador_Global;

	if (Tamanio_Menor_A_Chunk !=0 ){
					Elegido->tam_data = Tamanio_Menor_A_Chunk;
				}else{
					Elegido->tam_data = Tamanio_Exigido;
				}


	__sync_fetch_and_add(&Items_Ocupados,1);
	return(Elegido);
	}
//	log_debug(Archivo_Log,"Inicio del ciclo: Elegido: Empieza en %p, termina en %p, tam_disco: %d, tam_data :%d", Elegido->data,Elegido->final,Elegido->tam_disco,Elegido->tam_data);
	while (Elegido->tam_disco != potencia(2, i)) {


		Elegido->tam_disco = Elegido->tam_disco / 2;
		Aux2 = Elegido->Padres + Elegido->Cant_Antecesores;
		*Aux2 = Elegido->id;

		__sync_fetch_and_add(&Proximo_Id,1);
		Elegido->id = Proximo_Id;
		__sync_fetch_and_add(&(Elegido->Cant_Antecesores),1);


		memcpy(Elegido->key, mi_key, Tamanio_Key);
		Elegido->long_key = Tamanio_Key;


		Colega = buscar_Item_Libre_y_sin_Apuntar();

		Colega->final = Elegido->final;
		Elegido->final = Elegido->data + Elegido->tam_disco - 1;
		Colega->data = Elegido->final + 1;
		Colega->tam_disco= Elegido->tam_disco;
		__sync_fetch_and_add(&Proximo_Id,1);
		Colega->id = Proximo_Id;

		contador = 0;
		for (contador = 0; contador < Elegido->Cant_Antecesores; __sync_fetch_and_add(&contador,1)) {
			Aux2 = Elegido->Padres + contador;
			Aux3 = Colega->Padres + contador;
			memcpy(Aux3, Aux2, sizeof(uint64_t));

		}


		Colega->Cant_Antecesores = Elegido->Cant_Antecesores;
		//log_debug(Archivo_Log,"Fin del ciclo: Elegido: Empieza en %p, termina en %p, tam_disco: %d, tam_data :%d", Elegido->data,Elegido->final,Elegido->tam_disco,Elegido->tam_data);
	//	log_debug(Archivo_Log,"Fin del ciclo: Colega: Empieza en %p, termina en %p, tam_disco: %d, tam_data :%d", Colega->data,Colega->final,Colega->tam_disco,Colega->tam_data);
		}

	if (Tamanio_Menor_A_Chunk !=0 ){
						Elegido->tam_data = Tamanio_Menor_A_Chunk;
					}else{
						Elegido->tam_data = Tamanio_Exigido;
					}
	__sync_fetch_and_add(&Contador_Global,1);
	Elegido->exptime = Contador_Global;

	__sync_fetch_and_add(&Items_Ocupados,1);
		Primer_Item_Ocupado= buscar_Primer_Item_Ocupado();


	return (Elegido);

}

t_Item* buscar_Item_Libre_y_sin_Apuntar() {


	uint64_t i = 0;
	t_Item* Libre;
	while (i <= cantidad - 1) {
		if (Vector_Items[i].Libre == LIBRE && Vector_Items[i].data == NULL) {

			break;
		}
		__sync_fetch_and_add(&i,1);
	}
	if (i == cantidad) {

		return (NULL);
	}
	Libre = &Vector_Items[i];
	return (Libre);
}

unsigned long int potencia(int base, int exponente) {


	unsigned long int i = 0;

	unsigned long int resultado = 1;

	for (i = 0; i < exponente; __sync_fetch_and_add(&i,1)) {
		resultado *= base;
	}

	return (resultado);
}

void buddy_Inicializar (){

	uint64_t i=0;

		while (potencia(2, i) < Tam_Cache_Max) {
			__sync_fetch_and_add(&i,1);
		}

		cantidad= potencia(2,i);

		Cant_Max_Padres = i;


		Vector_Items = malloc(sizeof(t_Item) * cantidad + 1);

		Antecesores = malloc(sizeof(uint64_t) * Cant_Max_Padres * cantidad);

		log_debug(Archivo_Log,"Vector_Items apunta a %p", Vector_Items);
		log_debug(Archivo_Log,"Antecesores apunta a %p", Antecesores);
		mlock(Vector_Items, cantidad + 1);
		mlock(Antecesores, cantidad * Cant_Max_Padres);


	Primer_Item_Ocupado = &Vector_Items[0];


	Vector_Items[0].data = RAM;

	Vector_Items[0].tam_data = Tam_Cache_Max;
	Vector_Items[0].tam_disco = Tam_Cache_Max;
	Vector_Items[0].long_key = 0;
	Vector_Items[0].final = RAM + Tam_Cache_Max - 1;
	Vector_Items[0].stored = NO_STORED;
	Vector_Items[0].exptime = 0;
	Vector_Items[0].Libre = LIBRE;
	Vector_Items[0].id = 1;
	Vector_Items[0].Cant_Antecesores = 0;
	Vector_Items[0].Padres = &Antecesores[0];
	memset(Vector_Items[0].key, '\0', 40) ;
	Items_Ocupados = 0;
	uint64_t Pos = Cant_Max_Padres;
	i = 1;
	for (i = 1; i <= cantidad - 1; __sync_fetch_and_add(&i,1)) {
		Vector_Items[i].exptime = 0;
		Vector_Items[i].data = NULL;
		Vector_Items[i].Libre = LIBRE;
		Vector_Items[i].tam_data = 0;
		Vector_Items[i].tam_disco = Tam_Chunk;
		Vector_Items[i].long_key = 0;
		Vector_Items[i].stored = NO_STORED;
		Vector_Items[i].Padres = &Antecesores[Pos];
		Vector_Items[i].Cant_Antecesores = 0;
		Pos = Pos + Cant_Max_Padres;
		memset(Vector_Items[i].key, '\0', 40) ;

	}


	log_debug(Archivo_Log,"Supera al for");

}
void compactacion_Inicializar(){
	//log_debug(Archivo_Log,"Entra a compactacion_Inicializar");
	cantidad = (Tam_Cache_Max / Tam_Chunk)+1 ;
	p_first_fit = &first_fit;
	p_best_fit = &best_fit;
	Fallas = 0;
	Vector_Items = malloc(sizeof(t_Item) * cantidad + 1);
	algoritmo_fit_elegido =	Algoritmo_Allocacion == FIRST_FIT ? p_first_fit : p_best_fit;
	mlock(Vector_Items, cantidad + 1);
	Primer_Item_Ocupado = NULL;
	Vector_Items[0].data = RAM;
	Vector_Items[0].tam_data = Tam_Cache_Max;
	Vector_Items[0].tam_disco = Tam_Cache_Max;
	Vector_Items[0].long_key = 0;
	Vector_Items[0].final = RAM + Vector_Items[0].tam_disco - 1;
	Vector_Items[0].stored = NO_STORED;
	Vector_Items[0].exptime = 0;
	Vector_Items[0].Libre = LIBRE;
	memset(Vector_Items[0].key, '\0', 40) ;
	Items_Ocupados = 0;

	Vector_Items[0].id = 0;
		Vector_Items[0].Cant_Antecesores = 0;
		Vector_Items[0].Padres = NULL;


	Ultimo_Byte=Vector_Items[0].final;
	uint64_t i = 1;
	for (i = 1; i <= cantidad - 1; __sync_fetch_and_add(&i,1)) {
		Vector_Items[i].exptime = 0;
		Vector_Items[i].data = NULL;
		Vector_Items[i].Libre = LIBRE;
		Vector_Items[i].tam_data = Tam_Chunk;
		Vector_Items[i].tam_disco = Tam_Chunk;
		Vector_Items[i].long_key = 0;
		Vector_Items[i].stored = NO_STORED;
		Vector_Items[i].id = 0;
		Vector_Items[i].Cant_Antecesores = 0;
		Vector_Items[i].Padres = NULL;
		memset(Vector_Items[i].key, '\0', 40);
	}



}
