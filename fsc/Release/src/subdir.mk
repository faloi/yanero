################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/fsc_operations.c \
../src/fsc_utils.c \
../src/log_helpers.c \
../src/sockets_pool.c 

OBJS += \
./src/fsc_operations.o \
./src/fsc_utils.o \
./src/log_helpers.o \
./src/sockets_pool.o 

C_DEPS += \
./src/fsc_operations.d \
./src/fsc_utils.d \
./src/log_helpers.d \
./src/sockets_pool.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DFUSE_USE_VERSION=27 -D_FILE_OFFSET_BITS=64 -I"/home/utnso/git/2012-1c-pio/commons" -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


