/*
 * sockets_pool.h
 *
 *  Created on: Jul 19, 2012
 *      Author: faloi
 */

#ifndef SOCKETS_POOL_H_
#define SOCKETS_POOL_H_

	#include <commons/socket.h>

	void 			sockets_pool_create(char* rfs_ip, int rfs_port, int rfs_max_connections);
	t_socket* 		sockets_pool_get();
	void 			sockets_pool_release(t_socket* used_socket);

#endif /* SOCKETS_POOL_H_ */
