/*
 * fsc_operations.c
 *
 *  Created on: 13/05/2012
 *      Author: vero
 */
/**/

#include <commons/fuse/packages.h>
#include <commons/fuse/serializers.h>
#include <commons/socket.h>
#include <commons/log.h>
#include <fuse/fuse.h>
#include <errno.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <commons/config.h>
#include "fsc_operations.h"
#include "libmemcached/memcached.h"
#include <commons/syntax_sugars.h>
#include <commons/fuse/packages_utils.h>
#include <commons/cache_utils.h>
#include "fsc_utils.h"
#include <commons/global_log.h>
#include "log_helpers.h"
#include <commons/collections/sync_queue.h>
#include "sockets_pool.h"
#include <time.h>

//creo variable global para que la pueda usar cada función de fuse

#define PATH_CONFIG "fsc.config"
#define PATH_LOG "log_fsc.log"

t_datosConfig *datos_config;

/**
 @DESC: Crear y abrir un archivo
 @PARAMETROS:DE
 path - El path es relativo al punto de montaje y es la forma mediante la cual debemos
 encontrar el archivo o directorio que nos solicitan
 fi - es una estructura que contiene la metadata del archivo indicado en el path
 **/
int fsc_create(const char *path, mode_t mode, struct fuse_file_info *fi) {
	if (!path_valido(path))
	        return -EXIT_FAILURE;

	t_paquete *paquete = create_packageCreate(path);
	t_paquete *respuesta =sendAndRecvData(datos_config,paquete);

	t_respuesta_payload_create *respuesta_payload = deserial_respuesta_create(respuesta);

	if(respuesta_payload->status== OPERATION_ERROR){
		destroy_packageNipc(paquete);
		destroy_respuestaPayloadCreate(respuesta_payload);
		destroy_packageNipc(respuesta);
		return -EXIT_FAILURE;
	}

	else if(respuesta_payload->status== OPERATION_FILE_NOT_FOUND){
		destroy_packageNipc(paquete);
		destroy_respuestaPayloadCreate(respuesta_payload);
		destroy_packageNipc(respuesta);
		return -ENOENT;
	}else{
		if (datos_config->cache_on == 1) {
			modify_CacheValueReaddir(path);
		}
		destroy_packageNipc(paquete);
		destroy_respuestaPayloadCreate(respuesta_payload);
		destroy_packageNipc(respuesta);
		return EXIT_SUCCESS;
	}

}

/**
 @DESC:Abrir un archivo.Esta función va a ser llamada cuando a la biblioteca de FUSE le llege un pedido
 para tratar de abrir un archivo

 @PARAMETROS
 path - El path es relativo al punto de montaje y es la forma
 mediante la cual debemos encontrar el archivo o directorio que nos solicitan
 fi - es una estructura que contiene la metadata del archivo indicado en el path

 @RETURN: O archivo fue encontrado. -EACCES archivo no es accesible
 */
int fsc_open_return(t_respuesta_payload_open* respuesta, int status_code) {
	destroy_respuestaPayloadOpen(respuesta);
	return status_code;
}

int fsc_open(const char *path, struct fuse_file_info *fi) {
	if (!path_valido(path))
	        return -EXIT_FAILURE;

	t_paquete *pedido = create_packageOpen(path);

	t_paquete *respuesta = sendAndRecvData(datos_config, pedido);
	destroy_packageNipc(pedido);

	t_respuesta_payload_open *respuesta_payload = deserial_respuesta_open(respuesta);
	destroy_packageNipc(respuesta);

	int status_code = 0;
	switch (respuesta_payload->status) {
	case OPERATION_ERROR:
		status_code = -EXIT_FAILURE;
		break;
	case OPERATION_FILE_NOT_FOUND:
		status_code = -ENOENT;
		break;
	case OPERATION_OK:
		status_code = EXIT_SUCCESS;
		break;
	}

	return fsc_open_return(respuesta_payload, status_code);
}

/**
 @DESC:Leer datos de un archivo abierto
 @DESC_TECNICA:Read size bytes from the given file into the buffer buf, beginning offset
 bytes into the file.Returns the number of bytes transferred,
 or 0 if offset was at or beyond the end of the file. Required for any sensible filesystem.
 @PARAMETROS:
 path - El path es relativo al punto de montaje y es la forma
 mediante la cual debemos encontrar el archivo o directorio que nos solicitan
 fi - es una estructura que contiene la metadata del archivo indicado en el path
 **/
int fsc_read_failure(t_respuesta_payload_read* payload) {

	int status_code = 0;
	switch (payload->status) {
		case OPERATION_ERROR:
			status_code = -EXIT_FAILURE;
			break;
		case OPERATION_FILE_NOT_FOUND:
			status_code = -ENOENT;
			break;
		case OPERATION_FILE_IN_USE:
			status_code = -EPERM;
			break;
	}

	destroy_respuestaPayloadRead(payload);
	return status_code;
}

int fsc_read(const char *path, char *buf, size_t size, off_t offset,struct fuse_file_info *fi) {
	if (!path_valido(path))
	        return -EXIT_FAILURE;

	t_paquete *pedido = create_packageRead(path,size,offset);

	t_paquete *respuesta = sendAndRecvData(datos_config, pedido);
	destroy_packageNipc(pedido);

	t_respuesta_payload_read *respuesta_payload = deserial_respuesta_read(respuesta);
	destroy_packageNipc(respuesta);

	if (respuesta_payload->status != OPERATION_OK)
		return fsc_read_failure(respuesta_payload);

	memcpy(buf,respuesta_payload->content, respuesta_payload->size);

	size = respuesta_payload->size;
	destroy_respuestaPayloadRead(respuesta_payload);

	return size;
}

/**
 @DESC: Escribir datos de un archivo abierto
 @PARAMETROS
 path - El path es relativo al punto de montaje y es la forma mediante la cual debemos
 encontrar el archivo o directorio que nos solicitan
 buf - Este es un buffer donde se colocaran los nombres de los archivos y directorios
 que esten dentro del directorio indicado por el path
 fi - es una estructura que contiene la metadata del archivo indicado en el path
 @RETURN:

 **/

int fsc_write(const char *path, const char *buf, size_t size, off_t offset,
		struct fuse_file_info *fi){
	if (!path_valido(path))
	        return -EXIT_FAILURE;

	t_paquete *pedido = create_packageWrite(path,buf,size,offset);
	t_paquete *respuesta = sendAndRecvData(datos_config, pedido);
	t_respuesta_payload_write *respuesta_payload= deserial_respuesta_write(respuesta);

	   if(respuesta_payload->status== OPERATION_ERROR){
			destroy_packageNipc(pedido);
			destroy_respuestaPayloadWrite(respuesta_payload);
			destroy_packageNipc(respuesta);
			return -EXIT_FAILURE;
		}
		else if(respuesta_payload->status== OPERATION_FILE_NOT_FOUND){
			destroy_packageNipc(pedido);
			destroy_respuestaPayloadWrite(respuesta_payload);
			destroy_packageNipc(respuesta);
			return -ENOENT;
		}else{
		if (datos_config->cache_on == 1) {
			cache_delete_operation(GET_ATTRIBUTES, path);
		}
			size= respuesta_payload->requested;
			destroy_packageNipc(pedido);
			destroy_respuestaPayloadWrite(respuesta_payload);
			destroy_packageNipc(respuesta);

			return size;
		}


}
/**
 @DESC: Cerrar archivo abierto
 @DESC _TECNICA:This is the only FUSE function that doesn't have a directly corresponding
 system call, although close(2) is related. Release is called when FUSE is completely done
 with a file; at that point, you can free up any temporarily allocated data structures.There
 is exactly one release per open.
 @PARAMETROS:
 path - El path es relativo al punto de montaje y es la forma
 mediante la cual debemos encontrar el archivo o directorio que nos solicitan
 fi - es una estructura que contiene la metadata del archivo indicado en el path

 **/

int fsc_release(const char *path, struct fuse_file_info *fi){
	if (!path_valido(path))
	        return -EXIT_FAILURE;

	t_paquete *pedido = create_packageRelease(path);
	t_paquete *respuesta = sendAndRecvData(datos_config, pedido);

	t_respuesta_payload_release *respuesta_payload = deserial_respuesta_release(respuesta);
	 if(respuesta_payload->status== OPERATION_ERROR){
			destroy_packageNipc(pedido);
			destroy_respuestaPayloadRelease(respuesta_payload);
			destroy_packageNipc(respuesta);
			return -EXIT_FAILURE;

			}
			else if(respuesta_payload->status== OPERATION_FILE_NOT_FOUND){
				destroy_packageNipc(pedido);
				destroy_respuestaPayloadRelease(respuesta_payload);
				destroy_packageNipc(respuesta);
				return -ENOENT;
			}else{
				destroy_packageNipc(pedido);
				destroy_respuestaPayloadRelease(respuesta_payload);
				destroy_packageNipc(respuesta);
				return EXIT_SUCCESS;
	}
}

/**
 @DESC:Cambiar el tamaño de un archivo(truncar)
 @PARAMETROS:
 path - El path es relativo al punto de montaje y es la forma mediante la cual debemos
 encontrar el archivo o directorio que nos solicitan
 fi - es una estructura que contiene la metadata del archivo indicado en el path
 **/
int fsc_truncate(const char *path, off_t size) {
	if (!path_valido(path))
	        return -EXIT_FAILURE;

	t_paquete *pedido = create_packageTruncate(path,size);
	t_paquete *respuesta = sendAndRecvData(datos_config, pedido);
	t_respuesta_payload_truncate *respuesta_payload=deserial_respuesta_truncate(respuesta);

	 if(respuesta_payload->status== OPERATION_ERROR){
			destroy_packageNipc(pedido);
			destroy_respuestaPayloadTruncate(respuesta_payload);
			destroy_packageNipc(respuesta);
			return -EXIT_FAILURE;

			}
			else if(respuesta_payload->status== OPERATION_FILE_NOT_FOUND){
				destroy_packageNipc(pedido);
				destroy_respuestaPayloadTruncate(respuesta_payload);
				destroy_packageNipc(respuesta);
				return -ENOENT;
			}else{
		if (datos_config->cache_on == 1) {
			cache_delete_operation(GET_ATTRIBUTES, path);
		}
				destroy_packageNipc(pedido);
				destroy_respuestaPayloadTruncate(respuesta_payload);
				destroy_packageNipc(respuesta);
				return EXIT_SUCCESS;
	}
}

/**
 @DESC: Borrar un archivo
 @PARAMETROS:
 path - El path es relativo al punto de montaje y es la forma mediante la cual debemos
 encontrar el archivo o directorio que nos solicitan
 **/
int fsc_unlink(const char *path) {
	if (!path_valido(path))
	        return -EXIT_FAILURE;

	t_paquete *pedido = create_packageUnlink(path);
	t_paquete *respuesta = sendAndRecvData(datos_config, pedido);
	t_respuesta_payload_unlink *respuesta_payload= deserial_respuesta_unlink(respuesta);

	 if(respuesta_payload->status== OPERATION_ERROR){
			destroy_packageNipc(pedido);
			destroy_respuestaPayloadUnlink(respuesta_payload);
			destroy_packageNipc(respuesta);
			return -EXIT_FAILURE;

			}
			else if(respuesta_payload->status== OPERATION_FILE_NOT_FOUND){
				destroy_packageNipc(pedido);
				destroy_respuestaPayloadUnlink(respuesta_payload);
				destroy_packageNipc(respuesta);
				return -ENOENT;
			} else if(respuesta_payload->status == OPERATION_FILE_IN_USE) {
				destroy_packageNipc(pedido);
				destroy_respuestaPayloadUnlink(respuesta_payload);
				destroy_packageNipc(respuesta);
				return -EBUSY;
			} else if(respuesta_payload->status == OPERATION_IS_A_DIRECTORY) {
				destroy_packageNipc(pedido);
				destroy_respuestaPayloadUnlink(respuesta_payload);
				destroy_packageNipc(respuesta);
				return -EISDIR;
			}else{
		if (datos_config->cache_on == 1) {
			cache_delete_operation(GET_ATTRIBUTES, path);
			modify_CacheValueReaddir(path);
		}
				destroy_packageNipc(pedido);
				destroy_respuestaPayloadUnlink(respuesta_payload);
				destroy_packageNipc(respuesta);
				return EXIT_SUCCESS;
	}

}

/**
 @DESC:Remover un directorio
 @PARAMETROS:
 path - El path es relativo al punto de montaje y es la forma mediante la cual debemos
 encontrar el archivo o directorio que nos solicitan
 **/
int fsc_rmdir(const char *path) {
	if (!path_valido(path))
	        return -EXIT_FAILURE;

	t_paquete *pedido = create_packageRmdir(path);
	t_paquete *respuesta = sendAndRecvData(datos_config, pedido);
	t_respuesta_payload_rmdir * respuesta_payload = deserial_respuesta_rmdir(respuesta);

	 if(respuesta_payload->status== OPERATION_ERROR){
			destroy_packageNipc(pedido);
			destroy_respuestaPayloadRmdir(respuesta_payload);
			destroy_packageNipc(respuesta);
			return -EXIT_FAILURE;

			}
			else if(respuesta_payload->status== OPERATION_FILE_NOT_FOUND){
				destroy_packageNipc(pedido);
				destroy_respuestaPayloadRmdir(respuesta_payload);
				destroy_packageNipc(respuesta);
				return -ENOENT;
			}else if(respuesta_payload->status== OPERATION_NOT_A_DIRECTORY){
				destroy_packageNipc(pedido);
				destroy_respuestaPayloadRmdir(respuesta_payload);
				destroy_packageNipc(respuesta);
				return -ENOTDIR;
			}else{
		if (datos_config->cache_on == 1) {
			cache_delete_operation(GET_ATTRIBUTES, path);
			modify_CacheValueReaddir(path);
		}
				destroy_packageNipc(pedido);
				destroy_respuestaPayloadRmdir(respuesta_payload);
				destroy_packageNipc(respuesta);
				return EXIT_SUCCESS;
	}
}

/**(Esta función debe ser cacheada)
 @DESC: Leer un directorio.Esta función va a ser llamada cuando a la biblioteca de FUSE le llege unpedido
 para obtener la lista de archivos o directorios que se encuentra dentro de un directorio
 @PARAMETROS
 path - El path omo tengo la cabes relativo al punto de montaje y es la forma mediante la cual debemos
 encontrar el archivo o directorio que nos solicitan
 buf - Este es un buffer donde se colocaran los nombres de los archivos y directorios
 que estenomo tengo la cab dentro del directorio indicado por el path
 filler - Este es un puntero a una función, la cual sabe como guardar una cadena dentro
 del campo buf
 *@RETURN:  O directorio fue encontrado. -ENOENT directorio no encontrado
 */
void fsc_readdir_success(t_respuesta_payload_readdir* answer, void* buf, fuse_fill_dir_t filler) {

	int i;
	for (i = 0; i < (answer->cant_archivos); i++) {
		char* file = answer->arr_archivos[i];
		filler(buf, file, NULL, 0);
	}

	destroy_respuestaPayloadReaddir(answer);
}

int fsc_readdir_failure(t_paquete* respuesta, t_respuesta_payload_readdir* payload, int return_code) {
	destroy_respuestaPayloadReaddir(payload);
	destroy_packageNipc(respuesta);

	return return_code;
}

int fsc_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
	if (!path_valido(path))
	        return -EXIT_FAILURE;
	if (datos_config->cache_on == 1) {
		t_paquete *respuesta_cache = cache_get_operation(READ_DIRECTORY, path);
		loguear_get_cache(respuesta_cache, READ_DIRECTORY, path);

		if (respuesta_cache != NULL) {
			t_respuesta_payload_readdir *respuesta_payloadCache =
					deserial_respuesta_readdir(respuesta_cache);
			fsc_readdir_success(respuesta_payloadCache, buf, filler);
			destroy_packageNipc(respuesta_cache);

			return EXIT_SUCCESS;
		}
	}
	t_paquete *pedido = create_packageReaddir(path);

	t_paquete *respuesta = sendAndRecvData(datos_config, pedido);
	t_respuesta_payload_readdir *respuesta_payload = deserial_respuesta_readdir(respuesta);

	destroy_packageNipc(pedido);

	if (respuesta_payload->status == OPERATION_ERROR)
		return fsc_readdir_failure(respuesta, respuesta_payload, -EXIT_FAILURE);
	if (respuesta_payload->status == OPERATION_FILE_NOT_FOUND)
		return fsc_readdir_failure(respuesta, respuesta_payload, -ENOENT);
	if(respuesta_payload->status == OPERATION_PERMISSION_DENIED )
		return fsc_readdir_failure(respuesta, respuesta_payload, -EACCES);
	if (datos_config->cache_on == 1) {
		memcached_return result_cache = cache_set_operation(path, respuesta); //lo guardo en e¡la cache
		loguear_set_cache(result_cache, READ_DIRECTORY, path);
	}

	fsc_readdir_success(respuesta_payload, buf, filler);

	destroy_packageNipc(respuesta);

	return EXIT_SUCCESS;
}

/*(Esta función debe ser cacheada)
 @DESC :Obtener atributos de un archivo o directorio.Esta función va a ser llamada cuando a la biblioteca de FUSE le llege un pedido para obtener
 la metadata de un archivo/directorio. Esto puede ser tamaño, tipo,permisos, dueño, etc ...
 @PARAMETROS
 path - El path es relativo al punto de montaje y es la forma mediante la cual debemos
 encontrar el archivo o directorio que nos solicitan
 stbuf - Esta esta estructura es la que debemos completar
 @RETURN
 O archivo/directorio fue encontrado. -ENOENT archivo/directorio no encontrado
 */

void fsc_getattr_success(t_respuesta_payload_getattr* answer, struct stat* stbuff) {

	stbuff->st_size = answer->size;
	stbuff->st_mode = answer->mode == REGULAR_FILE ? S_IFREG | 0444 : S_IFDIR | 0755;
	stbuff->st_nlink = answer->mode == REGULAR_FILE? 1 : 2;

	time_t dummy_time = 0;
	struct timespec curr_tim = {
		.tv_sec = dummy_time,
		.tv_nsec = 0
	};

	stbuff->st_atim = curr_tim;
	stbuff->st_ctim = curr_tim;
	stbuff->st_mtim = curr_tim;

	destroy_respuestaPayloadGetAttr(answer);
}

int fsc_gettatr_failure(t_paquete* respuesta, t_respuesta_payload_getattr* payload, int return_code) {
	destroy_packageNipc(respuesta);
	destroy_respuestaPayloadGetAttr(payload);

	return return_code;
}

int fsc_getattr(const char *path, struct stat *stbuff) {
	if (!path_valido(path))
	        return -EXIT_FAILURE;
	if (datos_config->cache_on == 1) {
		t_paquete *respuesta_cache = cache_get_operation(GET_ATTRIBUTES, path);
		loguear_get_cache(respuesta_cache, GET_ATTRIBUTES, path);

		if (respuesta_cache != NULL) {
			t_respuesta_payload_getattr *respuesta_payloadCache =
					deserial_respuesta_getattr(respuesta_cache);
			fsc_getattr_success(respuesta_payloadCache, stbuff);
			destroy_packageNipc(respuesta_cache);

			return EXIT_SUCCESS;
		}
	}
	t_paquete *pedido = create_packageGetAttr(path);

	t_paquete *respuesta = sendAndRecvData(datos_config, pedido);
	t_respuesta_payload_getattr *respuesta_payload = deserial_respuesta_getattr(respuesta);

	destroy_packageNipc(pedido);

	if (respuesta_payload->status == OPERATION_ERROR)
		return fsc_gettatr_failure(respuesta, respuesta_payload, -EXIT_FAILURE);

	if (respuesta_payload->status == OPERATION_FILE_NOT_FOUND)
		return fsc_gettatr_failure(respuesta, respuesta_payload, -ENOENT);
	if (datos_config->cache_on == 1) {
		memcached_return result = cache_set_operation(path, respuesta);
		loguear_set_cache(result, GET_ATTRIBUTES, path);
	}

	fsc_getattr_success(respuesta_payload, stbuff);

	destroy_packageNipc(respuesta);
	return EXIT_SUCCESS;
}


/**
 @DESC:Crear un directorio
 @PARAMETROS:
 path - El path es relativo al punto de montaje y es la forma mediante la cual debemos
 encontrar el archivo o directorio que nos solicitan
 **/
int fsc_mkdir_return(t_respuesta_payload_mkdir* answer, int status_code) {
	destroy_respuestaPayloadMkdir(answer);
	return status_code;
}

int fsc_mkdir(const char *path, mode_t mode) {
	if (!path_valido(path))
	        return -EXIT_FAILURE;

	t_paquete *pedido = create_packageMkdir(path);
	t_paquete *respuesta = sendAndRecvData(datos_config, pedido);
	destroy_packageNipc(pedido);

	t_respuesta_payload_mkdir *respuesta_payload = deserial_respuesta_mkdir(respuesta);
	destroy_packageNipc(respuesta);

	int status_code = 0;
    switch (respuesta_payload->status) {
    case OPERATION_ERROR:
    	status_code = -EXIT_FAILURE;
    	break;
    case OPERATION_FILE_NOT_FOUND:
		status_code = -ENOENT;
		break;
    case OPERATION_OK:
		if (datos_config->cache_on == 1) {
			modify_CacheValueReaddir(path);
		}
		status_code = -EXIT_SUCCESS;
		break;
    }

    return fsc_mkdir_return(respuesta_payload, status_code);
}


/*
 * Esta es la estructura principal de FUSE con la cual nosotros le decimos a
 * biblioteca que funciones tiene que invocar segun que se le pida a FUSE.
 * Como se observa la estructura contiene punteros a funciones.
 */

static struct fuse_operations fsc_operations = {
		.getattr = fsc_getattr,
		.readdir = fsc_readdir,
		.open = fsc_open,
		.release =fsc_release,
		.read = fsc_read,
		.write = fsc_write,
		.create = fsc_create,
		.unlink = fsc_unlink,
		.truncate = fsc_truncate,
		.rmdir = fsc_rmdir,
		.mkdir = fsc_mkdir, };

/* Parameteros del fuse_main:
 argc 	the argument counter passed to the main() function
 argv 	the argument vector passed to the main() function
 op 	the file system operation
 user_data 	user data supplied in the context during the init() method
 */

int main(int argc, char *argv[]) {
	int ret = 0;

	datos_config = get_datosConfig(PATH_CONFIG);
	global_log_create(PATH_LOG, "fsc", true, 0);
	if(datos_config->cache_on==1){
		cache_create(datos_config->ip_cache, datos_config->port_cache, datos_config->cache_max_connections);
	}
	sockets_pool_create(datos_config->ip_server, datos_config->port_server, datos_config->rfs_max_connections);

	ret = fuse_main(argc, argv, &fsc_operations, NULL);
	if(datos_config->cache_on==1){
	cache_destroy();
	}
	destroy_datosConfig(datos_config);
	global_log_destroy();

	return ret;
}

