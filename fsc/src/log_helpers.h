/*
 * log_helpers.h
 *
 *  Created on: Jul 18, 2012
 *      Author: faloi
 */

#ifndef LOG_HELPERS_H_
#define LOG_HELPERS_H_

	void 	loguear_set_cache(memcached_return result, fuseOperationsType type, const char* path);
	void	loguear_get_cache(t_paquete* respuesta, fuseOperationsType type, const char* path);
	void 	loguear_solicitud_fuse(fuseOperationsType type, const char* path);
	void 	loguear_recepcion_cache(fuseOperationsType type, const char* path);
	void 	loguear_envio_rfs(fuseOperationsType type, const char* path);

#endif /* LOG_HELPERS_H_ */
