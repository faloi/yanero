/*
 * fsc_utils.h
 *
 *  Created on: 26/06/2012
 *      Author: vero
 */

#ifndef FSC_UTILS_H_
#define FSC_UTILS_H_

#include <commons/fuse/packages.h>
#include "fsc_operations.h"
#include <fuse/fuse.h>

t_datosConfig *get_datosConfig();
void destroy_datosConfig(t_datosConfig *datos_config);
int fsc_handshakeClient(t_socket *);
int path_valido(const char* path);
t_paquete *sendAndRecvData(t_datosConfig *datos_config,t_paquete *pedido_serial);
t_paquete *create_packageCreate(const char* );
t_paquete *create_packageOpen(const char*);
t_paquete *create_packageRead(const char *,size_t ,off_t);
t_paquete *create_packageWrite(const char *, const char *, size_t , off_t );
t_paquete *create_packageRelease(const char*);
t_paquete *create_packageTruncate(const char*, off_t size);
t_paquete *create_packageUnlink(const char* );
t_paquete *create_packageRmdir(const char *);
t_paquete *create_packageReaddir(const char*);
t_paquete *create_packageGetAttr(const char*);
t_paquete *create_packageMkdir(const char* );

void eliminar_pathDeArrayArchivos(
		t_respuesta_payload_readdir* respuesta_payload, char*fileName);

void agregar_pathArrayArchivos(t_respuesta_payload_readdir* respuesta_payload, char*fileName);

void modify_CacheValueReaddir(const char* path);

#endif /* FSC_UTILS_H_ */
