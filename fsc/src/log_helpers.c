/*
 * log_helpers.c
 *
 *  Created on: Jul 18, 2012
 *      Author: faloi
 */
#include <libmemcached/memcached.h>
#include <commons/fuse/packages.h>
#include <commons/global_log.h>
#include <commons/fuse/packages_utils.h>

void loguear_set_cache(memcached_return result, fuseOperationsType type, const char* path) {
	if (result == MEMCACHED_SUCCESS)
		global_log_debug("Valor almacenado en la cache: operacion %s, path %s",
				fuse_operation_to_string(type), path);
	else
		global_log_warning("No se pudo almacenar en la cache: operacion %s, path %s",
				fuse_operation_to_string(type), path);
}

void loguear_get_cache(t_paquete* respuesta, fuseOperationsType type, const char* path) {
	if (respuesta != NULL)
		global_log_debug("Valor traido de la cache: operacion %s, path %s",
				fuse_operation_to_string(type), path);
	else
		global_log_debug("No se encontro en la cache: operacion %s, path %s",
				fuse_operation_to_string(type), path);
}

void loguear_solicitud_fuse(fuseOperationsType type, const char* path) {
	global_log_debug("Recibida solicitud de %s para el path \"%s\"",
			fuse_operation_to_string(type),
			path);
}

void loguear_recepcion_cache(fuseOperationsType type, const char* path) {
	global_log_debug("Recibida respuesta de la cache, operacion: %s - path \"%s\"",
			fuse_operation_to_string(type),
			path);
}

void loguear_envio_rfs(fuseOperationsType type, const char* path) {
	global_log_debug("Enviando al rfs pedido de %s para el path \"%s\"",
			fuse_operation_to_string(type),
			path);
}

