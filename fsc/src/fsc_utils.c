/*
 * fsc_utils.c
 *
 *  Created on: 26/06/2012
 *      Author: vero
 */


#include <commons/fuse/packages.h>
#include <commons/fuse/serializers.h>
#include <commons/socket.h>
#include <fuse/fuse.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include<commons/config.h>
#include <commons/syntax_sugars.h>
#include <commons/fuse/packages_utils.h>
#include "fsc_utils.h"
#include "libmemcached/memcached.h"
#include <commons/cache_utils.h>
#include <commons/global_log.h>
#include "log_helpers.h"
#include "sockets_pool.h"
#include "commons/collections/list.h"
#include "commons/string.h"
static t_config* config_file;

#define CONFIG_KEYS_AMOUNT 7

t_list* get_path_list(const char* path){
	t_list* list_path = list_create();
	char* newPath;
	int i, last;

	for (i = 0; i < strlen(path); i++)
		if (path[i] == '/')
			last = i;

	if (last == 0)
		newPath = string_truncate(path, last + 1);
	else
		newPath = string_truncate(path, last);

	for (i = 0; i < last + 1; i++)
		path++;

	char* newDir = strdup(path);
	list_add(list_path, newPath);
	list_add(list_path, newDir);

	return list_path;
}

t_datosConfig *get_datosConfig(char* PATH_CONFIG){

	t_datosConfig *datos_config = new(t_datosConfig);
	t_config *config_file = config_create(PATH_CONFIG);

	if (config_keys_amount(config_file) != CONFIG_KEYS_AMOUNT) {
		perror("Error en el Config file\n");
		exit(EXIT_FAILURE);
	}

	if (config_has_property(config_file, "PORT_SERVER")) {
		datos_config->port_server = config_get_int_value(config_file,
				"PORT_SERVER");
	} else {
		printf("config_file no tiene PORT_SERVER\n");
	}
	if (config_has_property(config_file, "IP_SERVER")) {
		datos_config->ip_server = config_get_string_value(
				config_file, "IP_SERVER");
	} else {
		printf("config_file no tiene IP_SERVER\n");
	}
	if (config_has_property(config_file, "CACHE_ON")) {
		datos_config->cache_on = config_get_int_value(config_file,
				"CACHE_ON");
	} else {
		printf("config_file no tiene CACHE_ON\n");
	}
	if (config_has_property(config_file, "PORT_CACHE")) {
		datos_config->port_cache = config_get_int_value(config_file,
				"PORT_CACHE");
	} else {
		printf("config_file no tiene PORT_CACHE\n");
	}
	if (config_has_property(config_file, "IP_CACHE")) {
		datos_config->ip_cache = config_get_string_value(
				config_file, "IP_CACHE");
	} else {
		printf("config_file no tiene IP_CACHE\n");
	}

	if (config_has_property(config_file, "MAX_CACHE_CONNECTIONS"))
		datos_config->cache_max_connections = config_get_int_value(config_file, "MAX_CACHE_CONNECTIONS");
	else
		printf("config_file no tiene MAX_CACHE_CONNECTIONS\n");

	if (config_has_property(config_file, "MAX_RFS_CONNECTIONS"))
		datos_config->rfs_max_connections = config_get_int_value(config_file, "MAX_RFS_CONNECTIONS");
	else
		printf("config_file no tiene MAX_RFS_CONNECTIONS\n");

	return datos_config;
}


void destroy_datosConfig(t_datosConfig *datos_config){
	config_destroy(config_file);
	free(datos_config);
}

int path_valido(const char* path) {
        return strlen(path) <= 40;
}

/* Una vez que se conecctó un cliente con un servidor, se produce el handshake.Se envía
 * un paquete NIPC de tipo HANDSHAKE, y debe volver otro conn tipo HANDSHAKE_RESPONSE.
 * La función devuelve -1 si hubo algún error*/

t_paquete *sendAndRecvData(t_datosConfig *datos_config,t_paquete *pedido_serial ){
	t_socket* client = sockets_pool_get();

	global_log_info("Enviando pedido de %s al rfs", fuse_operation_to_string(pedido_serial->type));
	socket_send_nipc(client, pedido_serial);

	t_paquete *respuesta = socket_recvNipc(client);
	global_log_info("Recibida respuesta de %s", fuse_operation_to_string(pedido_serial->type));

	sockets_pool_release(client);

	return respuesta;
}

t_paquete *create_packageCreate(const char*path){
	t_pedido_payload_create *pedido_payload = new(t_pedido_payload_create);
	pedido_payload->path =strdup(path);

	t_paquete *pedido_serial = serial_pedido_create(pedido_payload);
	destroy_pedidoPayloadCreate(pedido_payload);

	return pedido_serial;

}

t_paquete *create_packageOpen(const char*path){
	t_pedido_payload_open *pedido_payload= new(t_pedido_payload_open);
	pedido_payload->path =strdup(path);

	t_paquete *pedido_serial = serial_pedido_open(pedido_payload);

	return pedido_serial;
}

t_paquete *create_packageRead(const char *path,size_t size,off_t offset){
	t_pedido_payload_read *pedido_payload = new(t_pedido_payload_read);
	pedido_payload->path = strdup(path);
	pedido_payload->size = size;
	pedido_payload->offset = offset;

	t_paquete *pedido_serial = serial_pedido_read(pedido_payload);
	return pedido_serial;
}
t_paquete *create_packageWrite(const char *path, const char *buf, size_t size, off_t offset){
	t_pedido_payload_write *pedido_payload = new(t_pedido_payload_write);
	pedido_payload->path = strdup(path);
	pedido_payload->size = size;
	pedido_payload->content = malloc(pedido_payload->size);
	memcpy(pedido_payload->content, buf, size);
	pedido_payload->offset=offset;

	t_paquete *pedido_serial= serial_pedido_write(pedido_payload);
	return pedido_serial;
}

t_paquete *create_packageRelease(const char* path){
	t_pedido_payload_release *pedido_payload =new(t_pedido_payload_release);
	pedido_payload->path =strdup(path);

	t_paquete *pedido_serial= serial_pedido_release(pedido_payload);

	return pedido_serial;
}

t_paquete *create_packageTruncate(const char*path, off_t size){
	t_pedido_payload_truncate *pedido_payload = new(t_pedido_payload_truncate);
	pedido_payload->path = strdup( path);
	pedido_payload->size = size;

	t_paquete *pedido_serial = serial_pedido_truncate(pedido_payload);

	return pedido_serial;
}

t_paquete *create_packageUnlink(const char*path ){
	t_pedido_payload_unlink *pedido_payload =new(t_pedido_payload_unlink);
	pedido_payload->path =strdup(path);

	t_paquete *pedido_serial = serial_pedido_unlink(pedido_payload);

	return pedido_serial;
}

t_paquete *create_packageRmdir(const char *path){
	t_pedido_payload_rmdir *pedido_payload  =new(t_pedido_payload_rmdir);
	pedido_payload->path =strdup( path);

	t_paquete *pedido_serial=serial_pedido_rmdir(pedido_payload);

	return pedido_serial;
}

t_paquete *create_packageReaddir(const char*path){
	t_pedido_payload_readdir *pedido_payload =new(t_pedido_payload_readdir);
	pedido_payload->path =strdup(path);

	t_paquete * pedido_serial = serial_pedido_readdir(pedido_payload);

	return pedido_serial;
}

t_paquete *create_packageGetAttr(const char*path){

	t_pedido_payload_getattr *pedido_payload=new(t_pedido_payload_getattr);
	pedido_payload->path =strdup(path);

	t_paquete *pedido_serial = serial_pedido_getattr(pedido_payload);

	return pedido_serial;

}

t_paquete *create_packageMkdir(const char* path){
	t_pedido_payload_mkdir *pedido_payload = new(t_pedido_payload_mkdir);
	pedido_payload->path = strdup(path);

	t_paquete *pedido_serial= serial_pedido_mkdir(pedido_payload);

	return pedido_serial;
}


void modify_CacheValueReaddir(const char* path) {
	t_list* directorioSeparado = get_path_list(path);
	cache_delete_operation(READ_DIRECTORY, list_get(directorioSeparado, 0));
}


