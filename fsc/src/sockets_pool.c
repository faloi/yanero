/*
 * sockets_pool.c
 *
 *  Created on: Jul 19, 2012
 *      Author: faloi
 */

#include <commons/socket.h>
#include <commons/collections/sync_queue.h>
#include <stdio.h>
#include <stdlib.h>

static t_sync_queue* sockets;

void sockets_pool_create(char* rfs_ip, int rfs_port, int rfs_max_connections) {
	sockets = sync_queue_create();

	int i;
	for (i = 0; i < rfs_max_connections; i++) {
		t_socket* new_socket = socket_createClient();
		int ret_value = socket_connect(new_socket, rfs_ip, rfs_port);

		if (ret_value == -1) {
			perror("No se pudo conectar con el rfs");
			exit(EXIT_FAILURE);
		}

		sync_queue_push(sockets, new_socket);
	}
}

t_socket* sockets_pool_get() {
	return sync_queue_pop(sockets);
}

void sockets_pool_release(t_socket* used_socket) {
	sync_queue_push(sockets, used_socket);
}
