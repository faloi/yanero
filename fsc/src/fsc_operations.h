/*
 * fsc_operations.h
 *
 *  Created on: 13/05/2012
 *      Author: vero
 */


#ifndef FSC_OPERATIONS_H_
#define FSC_OPERATIONS_H_

#include <fuse/fuse.h>

typedef struct{
	int port_server;
	int port_cache;
	int port_client;
	int cache_max_connections;
	int rfs_max_connections;
	int cache_on;
	char* ip_server;
	char* ip_client;
	char* ip_cache;
}t_datosConfig;



/*Funciones a implementar con FUSE*/
int fsc_create(const char *, mode_t, struct fuse_file_info *);
int fsc_open(const char *, struct fuse_file_info *);
int fsc_read(const char *, char *, size_t, off_t, struct fuse_file_info *);
int fsc_write(const char *, const char *, size_t, off_t,struct fuse_file_info *);
int fsc_release(const char *, struct fuse_file_info *);
int fsc_truncate(const char *, off_t);
int fsc_unlink(const char *);
int fsc_rmdir(const char *);
int fsc_readdir(const char *, void *, fuse_fill_dir_t, off_t,struct fuse_file_info *);
int fsc_getattr(const char *, struct stat *);
int fsc_mkdir(const char *, mode_t);


#endif /* FSC_OPERATIONS_H_ */

