/*
 * test.c
 *
 *  Created on: May 7, 2012
 *      Author: faloi
 */

#include <stdlib.h>
#include <stdio.h>
#include "../src/lib/ext2.h"
#include "../src/lib/inode.h"
#include "../src/lib/ext2_operations.h"
#include "tests.h"
#include <commons/thread.h>
#include <commons/fuse/packages.h>
#include <commons/syntax_sugars.h>
#include "commons/config.h"
#include "../src/lib/inode_repository.h"
#include "../src/lib/rfs_config.h"

#define MENU_EXIT 13
#define CONFIG_FILE_PATH "rfs.config"

char* status_code_to_string(enum statusCodes code) {
	switch (code) {
	case OPERATION_OK:
		return "OK";
	case OPERATION_FILE_NOT_FOUND:
		return "FILE NOT FOUND";
	case OPERATION_ERROR:
		return "ERROR";
	default:
		return "UNKNOWN CODE";
	}
}

void cls() {
	int i;
	for (i = 0; i < 50; i++)
		puts("");
}

int displayMenu() {
	puts("<---OPERATIONS--->");
	puts("1_ CREATE");
	puts("2_ OPEN");
	puts("3_ READ");
	puts("4_ WRITE");
	puts("5_ RELEASE");
	puts("6_ TRUNCATE");
	puts("7_ UNLINK");
	puts("8_ REMOVE_DIRECTORY");
	puts("9_ READ_DIRECTORY");
	puts("10_ GET_ATTRIBUTES");
	puts("11_ MAKE_DIRECTORY");
	puts("12_ EXIT");
	puts("");

	int aNumber;

	char* path = malloc(EXT2_PATH_MAX_LENGTH);

	scanf("%d", &aNumber);
	aNumber++;
	if (aNumber != MENU_EXIT) {
		printf("Path: ");
		scanf("%s", path);
	}

	switch (aNumber) {
	case CREATE:{
		t_pedido_payload_create* request = new(t_pedido_payload_create);
		request->path=path;
		t_respuesta_payload_create* answer = ext2_create_file(request);
		break;
	}
	case OPEN: {
		t_pedido_payload_open* request = new(t_pedido_payload_open);
		request->path = path;
		t_respuesta_payload_open* answer = ext2_open_file(request);
		puts(status_code_to_string(answer->status));
		break;
	}
	case READ: {
		printf("Offset: ");
		int offset;
		scanf("%d", &offset);

		printf("Size: ");
		int size;
		scanf("%d", &size);

		t_pedido_payload_read* request = new(t_pedido_payload_read);
		request->path = path;
		request->offset = offset;
		request->size = size;

		t_respuesta_payload_read* answer = ext2_read_file(request);

		puts(status_code_to_string(answer->status));
		if (answer->status == OPERATION_OK) {
			printf("Read %d bytes.\n", answer->size);
			printf("%s", answer->content);
		}

		break;
	}
	case WRITE:{
		printf("Offset: ");
		int offset;
		scanf("%d", &offset);

		printf("Size: ");
		int size;
		scanf("%d", &size);

		printf("Content: ");
		char* content = malloc(size + 1);
		scanf("%s", content);

		t_pedido_payload_write* request= new(t_pedido_payload_write);
		request->path=path;
		request->content=content;
		request->size=size;
		request->offset=offset;

		ext2_write_file(request);

		break;
	}
	case UNLINK:{
		t_pedido_payload_unlink* request =new(t_pedido_payload_unlink);
		request->path=path;
		t_respuesta_payload_unlink* answer=ext2_unlink_file(request);
		break;
	}
	case TRUNCATE:{
		printf("Size: ");
		int size;
		scanf("%d", &size);
		t_pedido_payload_truncate* request = new(t_pedido_payload_truncate);
		request->path=path;
		request->size=size;
		t_respuesta_payload_truncate* answer =ext2_truncate_file(request);
		break;
	}
	case RELEASE: {
		t_pedido_payload_release* request = new(t_pedido_payload_release);
		request->path = path;

		t_respuesta_payload_release* answer = ext2_release_file(request);
		puts(status_code_to_string(answer->status));
		break;
	}
	case REMOVE_DIRECTORY:{
		t_pedido_payload_rmdir* request =new(t_pedido_payload_rmdir);
		request->path=path;
		t_respuesta_payload_rmdir* answer=ext2_remove_directory(request);
		if (answer->status==OPERATION_FILE_NOT_FOUND)
			printf("\n%s\n",status_code_to_string(OPERATION_FILE_NOT_FOUND));
		if (answer->status==OPERATION_ERROR)
			printf("\n%s\n",status_code_to_string(OPERATION_ERROR));
		break;
	}
	case READ_DIRECTORY:{
	 	printf("Directory: %s\n", path);
		t_pedido_payload_readdir* request = new(t_pedido_payload_readdir);
		request->path=path;
		t_respuesta_payload_readdir* answer = ext2_read_directory(request);
		if (answer->status==OPERATION_FILE_NOT_FOUND){
			printf("\nFILE NOT FOUND\n");
			break;
		}else if(answer->status==OPERATION_ERROR){
			printf("\nERROR\n");
			break;
		}else{
			char** arr=answer->arr_archivos;
			int max=answer->cant_archivos;
			int i;
			for (i = 0; i <= max; i++) {
				char* dir = arr[i];
				if (dir != NULL)
					printf("%s\t",arr[i]);
			}
			printf("\n\n");
			break;
		}
	}
	case GET_ATTRIBUTES: {
		t_pedido_payload_getattr* request = new(t_pedido_payload_getattr);
		request->path = path;
		t_respuesta_payload_getattr* answer = ext2_get_attributes(request);
		if (answer->mode == DIRECTORY) {
			printf("\nFILE TYPE: DIRECTORY");
		} else {
			printf("\nFILE TYPE: REGULAR FILE");
		}
		printf("\nSIZE: %d\n", answer->size);
		break;
	}
	case MAKE_DIRECTORY:{
		t_pedido_payload_mkdir* request=new(t_pedido_payload_mkdir);
		request->path=path;
		t_respuesta_payload_mkdir* answer = ext2_make_directory(request);
		break;
	}


	}

	puts("");
	sleep(2);

	return aNumber;
}

int maino(int argc, char **argv) {
	rfs_config* config = rfs_config_create(CONFIG_FILE_PATH);
	ext2_open(config->disk_path);

	inode_repository_create();

	int menuOption = 0;
	while (menuOption != MENU_EXIT)
		menuOption = displayMenu();

	rfs_config_destroy(config);
	ext2_destroy();

	return EXIT_SUCCESS;
}
