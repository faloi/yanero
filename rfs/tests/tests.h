/*
 * tests.h
 *
 *  Created on: May 29, 2012
 *      Author: faloi
 */

#ifndef TESTS_H_
#define TESTS_H_

	void 	groupDescriptorTest();
	void 	superblockTest();
	void 	inodeTest();
	void	inodeFind(char* path);
	void 	bitmapsTest();
	void 	readBlocksTest();
	void 	directoryTest();

#endif /* TESTS_H_ */
