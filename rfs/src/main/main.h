/*
 * main.h
 *
 *  Created on: Jul 8, 2012
 *      Author: faloi
 */

#ifndef MAIN_H_
#define MAIN_H_

	#include <commons/collections/sync_queue.h>
	#include <semaphore.h>
	#include <commons/socket.h>

	typedef struct {
		t_socket*		client;
		t_paquete*		package;
	} t_fuse_request;

	void receive_fuse_request(t_sync_queue* queue);

#endif /* MAIN_H_ */
