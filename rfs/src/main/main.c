#include <stdlib.h>
#include <commons/config.h>
#include <commons/fuse/packages.h>
#include <commons/fuse/packages_utils.h>
#include <commons/multiplexor.h>
#include <commons/global_log.h>
#include <semaphore.h>
#include "main.h"
#include "../lib/ext2.h"
#include <commons/collections/sync_queue.h>
#include <commons/syntax_sugars.h>
#include <arpa/inet.h>
#include "../lib/rfs_config.h"
#include <string.h>
#include <errno.h>
#include <commons/thread.h>
#include "../lib/inode_repository.h"
#include <signal.h>
#include "commons/cache_utils.h"

#define CONFIG_FILE_PATH "rfs.config"

t_sync_queue* requests;
static t_multiplexor* epoll;
static rfs_config* config;

void log_new_connection(t_socket* client) {
	global_log_info("Se ha conectado un nuevo cliente en el socket %d", client->desc);
}

int listen_nipc(t_socket* client) {
	t_paquete* package = socket_recvNipc(client);

	if (package == NULL) {
		global_log_info("Se ha desconectado el cliente del socket %d", client->desc);
		return -1;
	}

	global_log_trace("Recibido pedido de %s en el socket: %d",
			fuse_operation_to_string(package->type),
			client->desc);

	t_fuse_request* request = new(t_fuse_request);
	request->client = client;
	request->package = package;

	sync_queue_push(requests, request);

	return 0;
}

void reload_config(char* path) {
	rfs_config_destroy();
	config = rfs_config_create(CONFIG_FILE_PATH);
	global_log_info("Actualizados los parametros de configuracion (retardo %dus / %dms)", config->operation_lag, config->operation_lag / 1000);
}

void rfs_destroy() {
	global_log_info("Se ha detenido el proceso");

	multiplexor_destroy(epoll);
	global_log_destroy();
	rfs_config_destroy();
	ext2_destroy();

	exit(EXIT_SUCCESS);
}

int main(int argc, char **argv) {
	config = rfs_config_create(CONFIG_FILE_PATH);

	ext2_open(config->disk_path);

	global_log_create(config->log_path, "rfs", config->show_log_messages, config->log_detail);
	global_log_info("Se ha iniciado el proceso");

	requests = sync_queue_create();

	epoll = multiplexor_create(config->rfs_ip, config->rfs_port);
	if (epoll == NULL) {
		global_log_error("No se pudo crear el multiplexor, corrija el error y vuelva a iniciar el proceso. %s", strerror(errno));
		return EXIT_FAILURE;
	}

	multiplexor_add_inotify(epoll, CONFIG_FILE_PATH);

	global_log_info("Trabajando con el disco %s", config->disk_path);
	global_log_info("Aceptando conexiones en el puerto %d", config->rfs_port);

	int i;
	for (i = 1; i <= config->max_threads; i++)
		thread_create(receive_fuse_request, requests);

	inode_repository_create();

	cache_create(config->cache_ip, config->cache_port, config->max_cache_connections);
	cache_set_block_size(ext2_get()->blockSize);

	signal(SIGINT, rfs_destroy);

	while (1)
		multiplexor_wait(epoll, listen_nipc, log_new_connection, reload_config);

	rfs_destroy();

	return EXIT_SUCCESS;
}
