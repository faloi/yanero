#include <commons/fuse/packages.h>
#include "main.h"
#include <commons/collections/sync_queue.h>
#include <commons/socket.h>
#include <commons/global_log.h>
#include <commons/fuse/packages_utils.h>
#include <unistd.h>
#include "../lib/rfs_config.h"
#include <commons/fuse/serializers.h>
#include "../lib/ext2_operations.h"

void log_operation_process(fuseOperationsType type, char* path) {
	global_log_debug("Procesando %s para el path \"%s\"",
			fuse_operation_to_string(type),
			path);
}

void log_rw_process(fuseOperationsType type, char* path, uint64_t size, uint64_t offset) {
	global_log_debug("Procesando %s para el path \"%s\" offset %lld - size %lld",
			fuse_operation_to_string(type),
			path,
			offset,
			size);
}


t_paquete* process_fuse_request(t_paquete* request_package) {
	switch (request_package->type) {
	case CREATE: {
		t_pedido_payload_create* request = deserial_pedido_create(request_package);
		log_operation_process(CREATE, request->path);

		t_respuesta_payload_create* answer = ext2_create_file(request);
		t_paquete* ret = serial_respuesta_create(answer);

		destroy_respuestaPayloadCreate(answer);
		return ret;
	}
	case OPEN: {
		t_pedido_payload_open* request = deserial_pedido_open(request_package);
		log_operation_process(OPEN, request->path);

		t_respuesta_payload_open* answer = ext2_open_file(request);
		t_paquete* ret = serial_respuesta_open(answer);

		destroy_respuestaPayloadOpen(answer);
		return ret;
	}
	case READ: {
		t_pedido_payload_read* request = deserial_pedido_read(request_package);
		log_rw_process(READ, request->path, request->size, request->offset);

		t_respuesta_payload_read* answer = ext2_read_file(request);
		t_paquete* ret = serial_respuesta_read(answer);

		destroy_respuestaPayloadRead(answer);
		return ret;
	}
	case WRITE:{
		t_pedido_payload_write* request = deserial_pedido_write(request_package);
		log_rw_process(WRITE, request->path, request->size, request->offset);

		t_respuesta_payload_write* answer = ext2_write_file(request);
		t_paquete* ret = serial_respuesta_write(answer);

		destroy_respuestaPayloadWrite(answer);
		return ret;
	}
	case UNLINK: {
		t_pedido_payload_unlink* request = deserial_pedido_unlink(request_package);
		log_operation_process(UNLINK, request->path);

		t_respuesta_payload_unlink* answer = ext2_unlink_file(request);
		t_paquete* ret = serial_respuesta_unlink(answer);

		destroy_respuestaPayloadUnlink(answer);
		return ret;
	}
	case TRUNCATE:{
		t_pedido_payload_truncate* request = deserial_pedido_truncate(request_package);
		log_operation_process(TRUNCATE, request->path);

		t_respuesta_payload_truncate* answer = ext2_truncate_file(request);
		t_paquete* ret = serial_respuesta_truncate(answer);

		destroy_respuestaPayloadTruncate(answer);
		return ret;
	}
	case RELEASE: {
		t_pedido_payload_release* request = deserial_pedido_release(request_package);
		log_operation_process(RELEASE, request->path);

		t_respuesta_payload_release* answer = ext2_release_file(request);
		t_paquete* ret = serial_respuesta_release(answer);

		destroy_respuestaPayloadRelease(answer);
		return ret;
	}
	case REMOVE_DIRECTORY: {
		t_pedido_payload_rmdir* request = deserial_pedido_rmdir(request_package);
		log_operation_process(REMOVE_DIRECTORY, request->path);

		t_respuesta_payload_rmdir* answer = ext2_remove_directory(request);
		t_paquete* ret = serial_respuesta_rmdir(answer);

		destroy_respuestaPayloadRmdir(answer);
		return ret;
	}
	case READ_DIRECTORY: {
		t_pedido_payload_readdir* request = deserial_pedido_readdir(request_package);
		log_operation_process(READ_DIRECTORY, request->path);

		t_respuesta_payload_readdir* answer = ext2_read_directory(request);
		t_paquete* ret = serial_respuesta_readdir(answer);

		destroy_respuestaPayloadReaddir(answer);
		return ret;
	}
	case GET_ATTRIBUTES: {
		t_pedido_payload_getattr* request = deserial_pedido_getattr(request_package);
		log_operation_process(GET_ATTRIBUTES, request->path);

		t_respuesta_payload_getattr* answer = ext2_get_attributes(request);
		t_paquete* ret = serial_respuesta_getattr(answer);

		destroy_respuestaPayloadGetAttr(answer);
		return ret;
	}
	case MAKE_DIRECTORY: {
		t_pedido_payload_mkdir* request = deserial_pedido_mkdir(request_package);
		log_operation_process(MAKE_DIRECTORY, request->path);

		t_respuesta_payload_mkdir* answer = ext2_make_directory(request);
		t_paquete* ret = serial_respuesta_mkdir(answer);

		destroy_respuestaPayloadMkdir(answer);
		return ret;
	}
	default:
		return NULL;
	}
}

void receive_fuse_request(t_sync_queue* queue) {
	while(1) {
		t_fuse_request* new_request = sync_queue_pop(queue);

		t_paquete* return_package = process_fuse_request(new_request->package);

		global_log_trace("Enviando respuesta de %s para el socket %d",
				fuse_operation_to_string(return_package->type),
				new_request->client->desc);

		socket_send_nipc(new_request->client, return_package);

		destroy_packageNipc(return_package);
	}
}
