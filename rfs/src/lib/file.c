/*
 * file.c
 *
 *  Created on: Jul 14, 2012
 *      Author: faloi
 */
#include <commons/fuse/packages.h>
#include <commons/syntax_sugars.h>
#include <commons/collections/list.h>
#include <commons/fuse/packages.h>
#include <string.h>
#include "inode_repository.h"
#include "ext2_def.h"
#include "ext2.h"
#include "inode.h"
#include <stdlib.h>
#include "../lib/rfs_config.h"
#include <commons/global_log.h>
#include "commons/cache_utils.h"
#include "file.h"
#include "directory.h"
#include <stdbool.h>

//PRIVATE FUNCTIONS
void file_complete_with_zeros(int offset_block,int bytesToAdd, char* block){
	memset(block + offset_block, 0, bytesToAdd);
}

// PUBLIC FUNCTIONS



void file_kill(int inode_number, char* path) {
	t_list* fileNames = get_path_list(path);
	char* pathFather = list_get(fileNames, 0);

	int inodeNumberFather = inode_number_get_from_path(pathFather);
	list_destroy(fileNames);

	t_inode* inodeFather = inode_repository_add_and_get(inodeNumberFather);
	inode_repository_lock_write(inodeNumberFather);
	inodeFather->links_count--;
	inode_repository_unlock(inodeNumberFather);
	inode_repository_release(inodeNumberFather);

	t_inode* inode = inode_repository_get(inode_number);

	int blockGroupNumberFile_Inode = (inode_number - 1) / ext2_get()->superblock->inodes_per_group;

	t_group_descriptor actualGroupDescriptor = ext2_get()->groupDescriptors[blockGroupNumberFile_Inode];
	actualGroupDescriptor.free_inodes_count++;
	ext2_get()->superblock->free_inodes_count++;

	t_bitarray* inode_bitmap = group_descriptor_get_inode_bitmap(blockGroupNumberFile_Inode);
	off_t inode_bitmap_offset = ((inode_number - 1) - blockGroupNumberFile_Inode * ext2_get()->superblock->inodes_per_group);
	bitarray_clean_bit(inode_bitmap, inode_bitmap_offset);

	file_truncate(inode, 0);
	t_list* list_path = get_path_list(path);
	char* listPath = list_get(list_path, 0);
	char* file = list_get(list_path, 1);
	directory_delete_entry(listPath, file);

	list_destroy(list_path);
}

int file_write(t_inode* inode, uint64_t offset, uint64_t size, char* content) {
	int newSize = offset + size;

	if (inode->size < newSize)
		file_truncate(inode, newSize); //157286400

	int* aNumber = new(int);
	*aNumber = size;
	t_list* blocks = inode_get_block_list(inode, offset, aNumber, true, false);

	int written_count = 0;
	int blockSize = ext2_get()->blockSize;
	int blockOffset = 0, blockLength;
	int i;
	for (i = 0; i < blocks->elements_count; i++) {
		blockLength = blockSize;
		blockOffset = 0;

		if (i == 0) {
			blockOffset = offset % blockSize;
			blockLength = blockSize - blockOffset;
		}

		if (i == blocks->elements_count - 1) 
			blockLength = size - written_count;

		int* aux = list_get(blocks, i);
		int blockNumber = *aux;
		char* block = ext2_get_block(blockNumber);

		memcpy(block + blockOffset, content + written_count, blockLength);
		written_count += blockLength;

		if (rfs_config_get()->cache_enabled) {
			global_log_trace("Guardando en la cache el bloque %d", blockNumber);
			cache_set_block(blockNumber, block);
		}
	}

	return written_count;
}

bool file_truncate(t_inode* inode, uint64_t size) {
	int* lengthInode = new(int);
	int initialSizeBlockList;
	*lengthInode = inode->size;
	t_list* blocks =list_create();
	if(inode->size!=0){
		initialSizeBlockList=inode->size/ext2_get()->blockSize;
		if(inode->size%ext2_get()->blockSize!=0)
			initialSizeBlockList++;
	}else{
		initialSizeBlockList=0;
	}
	if(size==0){
		inode->size=size;
		int i=0;
		int blockGroupNumberFile_Block;
		while(i<=list_size(blocks)-1){
			int* pointerToNumber = list_get(blocks,i);
			int blockNumber = *pointerToNumber;
			blockGroupNumberFile_Block = blockNumber / ext2_get()->superblock->blocks_per_group;

			t_bitarray* block_bitmap = group_descriptor_get_block_bitmap(blockGroupNumberFile_Block);
			off_t block_bitmap_offset = (blockNumber - blockGroupNumberFile_Block * ext2_get()->superblock->blocks_per_group)-1;
			bitarray_clean_bit(block_bitmap,block_bitmap_offset);

			t_group_descriptor actualGroupDescriptor = ext2_get()->groupDescriptors[blockGroupNumberFile_Block];
			actualGroupDescriptor.free_block_count++;
			ext2_get()->superblock->free_blocks_count++;

			i++;
		}
		inode->blocks=0;
	}else if(size > inode->size){
		int bytesToAdd =size - inode->size;
		int last_block_number;
		if(inode->size==0){
			last_block_number=ext2_get_free_block_number();
			inode->block_pointers[0]=last_block_number;
			initialSizeBlockList=1;
		}else{
			blocks=inode_get_block_list(inode, (initialSizeBlockList-1)*ext2_get()->blockSize, lengthInode,true,false);
			int* block = list_get(blocks,0);
			last_block_number = *block;
		}
		char* block = ext2_get_block(last_block_number);
		int offset_last_block = inode->size % ext2_get()->blockSize;
		int required_blocks;
		if(ext2_get()->blockSize-offset_last_block>=bytesToAdd){
			if(offset_last_block==0){
				required_blocks=1;
			}else{
				required_blocks=0;
				inode->size=size;
				file_complete_with_zeros(offset_last_block,ext2_get()->blockSize-offset_last_block,block);
			}
		}else{
			required_blocks=(bytesToAdd - ext2_get()->blockSize+offset_last_block)/ ext2_get()->blockSize ;
			if ((bytesToAdd - ext2_get()->blockSize+offset_last_block)% ext2_get()->blockSize !=0)
				required_blocks++;
		}
		if(required_blocks>ext2_get()->superblock->free_blocks_count)
			return false;
		if(bytesToAdd+offset_last_block<=ext2_get()->blockSize && offset_last_block != 0){
			file_complete_with_zeros(offset_last_block,bytesToAdd,block);
			inode->size=size;
		}
		if(required_blocks>=1){
			inode->size=size;

			int i;
			for (i=0;i<required_blocks;i++){
				inode_add_block(inode);
				*lengthInode = inode->size;
//				if(initialSizeBlockList==1 && i==0){
				blocks=inode_get_block_list(inode, (initialSizeBlockList+i)*ext2_get()->blockSize, lengthInode,true,false);
//				}else{
//					blocks=inode_get_block_list(inode, (initialSizeBlockList+i-1)*ext2_get()->blockSize, lengthInode,true,false);
//				}
				int* aux = list_get(blocks,0);
				int blockNumberData = *aux;
				char* blockToTruncate = ext2_get_block(blockNumberData);
				if(i==required_blocks-1)
					file_complete_with_zeros(0,(bytesToAdd - (ext2_get()->blockSize-offset_last_block)) %ext2_get()->blockSize, blockToTruncate);
				else
					file_complete_with_zeros(0, ext2_get()->blockSize, blockToTruncate);

				if (rfs_config_get()->cache_enabled) {
					global_log_trace("Guardando en la cache el bloque %d", blockNumberData);
					cache_set_block(blockNumberData, blockToTruncate);
				}
			}
		}
	}else if(size < inode->size){
		int bytesToErase = inode->size - size;
		int offset_last_block = inode->size % ext2_get()->blockSize;
		int offsetToGetBlockList;
		if(size%1024 != 0){
			offsetToGetBlockList=size+ext2_get()->blockSize-offset_last_block;
		}else{
			offsetToGetBlockList=size;
		}
		blocks=inode_get_block_list(inode,offsetToGetBlockList, lengthInode,true,false);
		if(bytesToErase>=offset_last_block){
			int blocks_to_erase = (bytesToErase-offset_last_block)/ext2_get()->blockSize;
			if((bytesToErase-offset_last_block)%ext2_get()->blockSize!=0)
				blocks_to_erase++;
			int blockCount = list_size(blocks);
			int i;
			for(i=0;i<=list_size(blocks)-1;i++){
				int* aux = list_get(blocks,i);
				int blockNumber = *aux;
				int blockGroupNumberFile_Block = blockNumber / ext2_get()->superblock->blocks_per_group;
				t_bitarray* block_bitmap = group_descriptor_get_block_bitmap(blockGroupNumberFile_Block);
				off_t block_bitmap_offset = (blockNumber - blockGroupNumberFile_Block * ext2_get()->superblock->blocks_per_group)-1;
				bitarray_clean_bit(block_bitmap,block_bitmap_offset);

				t_group_descriptor actualGroupDescriptor = ext2_get()->groupDescriptors[blockGroupNumberFile_Block];
				actualGroupDescriptor.free_block_count++;
				ext2_get()->superblock->free_blocks_count++;
			}
			inode->size=size;
		}
	}
	int physicalBlocks_count = inode->size / SECTOR_SIZE;
	if(inode->size % SECTOR_SIZE !=0)
		physicalBlocks_count++;
	inode->blocks=physicalBlocks_count;

	return true;
}
