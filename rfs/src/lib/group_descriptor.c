/*
 * group_descriptor.c
 *
 *  Created on: May 29, 2012
 *      Author: faloi
 */

#include <commons/bitarray.h>
#include "group_descriptor.h"
#include "ext2.h"

t_bitarray* get_bitmap(uint32_t blockNumber, size_t size) {
	char* field = ext2_get_block(blockNumber);
	return bitarray_create(field, size / 8);
}

t_bitarray* group_descriptor_get_inode_bitmap(int blockGroupNumber) {
	t_ext2* ext2 = ext2_get();
	uint32_t inodeBitmap = ext2->groupDescriptors[blockGroupNumber].inode_bitmap;
	return get_bitmap(inodeBitmap, ext2->superblock->inodes_per_group);
}

t_bitarray* group_descriptor_get_block_bitmap(int blockGroupNumber) {
	t_ext2* ext2 = ext2_get();
	uint32_t blockBitmap = ext2->groupDescriptors[blockGroupNumber].block_bitmap;
	return get_bitmap(blockBitmap, ext2->superblock->blocks_per_group);
}
