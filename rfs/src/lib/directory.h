/*
 * directory.h
 *
 *  Created on: 24/05/2012
 *      Author: oli
 */

#ifndef DIRECTORY_H_
#define DIRECTORY_H_

	#include <stdint.h>
	#include <commons/collections/list.h>
	#include "inode.h"

	typedef struct {
		uint32_t inode;
		uint16_t record_size;
		uint8_t name_length;
		uint8_t padding;
		char name[255];
	} t_directory;


		t_list* 			get_files_names(char*);
		t_list* 			get_path_list(char*);
		void 				print_files_names(char*);
		int 				directory_get_inode(char*);
		int					directory_get_subdirectory_inode_number(int currentInodeNumber, char* subdir);
		void 				set_new_directory(t_directory* directory, char* newName,int offset,int father_inode_number);
		enum statusCodes	directory_new_entry(char* path,enum fileTypes);
		char* 				directory_list(t_inode* inode, char* path);
		bool 				directory_is_empty(t_inode* inode, char* path);
		void 				directory_delete_entry(char* path, char* fileName);


#endif /* DIRECTORY_H_ */
