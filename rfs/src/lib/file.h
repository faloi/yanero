/*
 * file.h
 *
 *  Created on: Jul 14, 2012
 *      Author: faloi
 */

#ifndef FILE_H_
#define FILE_H_

	#include <commons/fuse/packages.h>
	#include "inode.h"
	#include <stdint.h>

	void 			file_complete_with_zeros(int offset_block,int bytesToAdd, char* block);
	bool 			file_truncate(t_inode* inode, uint64_t size);
	void			file_kill(int inode_number, char* path);
	int 			file_write(t_inode* inode, uint64_t offset, uint64_t size, char* content);

#endif /* FILE_H_ */
