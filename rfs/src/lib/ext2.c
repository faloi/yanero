/*
 * ext2.c
 *
 *  Created on: May 6, 2012
 *      Author: faloi
 */


#include <commons/syntax_sugars.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include "group_descriptor.h"
#include "ext2.h"
#include "../lib/rfs_config.h"
#include <commons/global_log.h>
#include <stdbool.h>

#define SUPERBLOCK_SIZE 1024

t_ext2* ext2;

t_ext2* ext2_get() {
	return ext2;
}

off_t ext2_get_disk_size(const char* filePath) {
	struct stat fileAttributes;
	stat(filePath, &fileAttributes);
	return fileAttributes.st_size;
}

void ext2_simulate_lag() {
	int lag = rfs_config_get()->operation_lag;
	if (lag > 0) {
		global_log_trace("Simulando retardo de %dus / %dms",
			lag,
			lag / 1000);

		usleep(lag);
	}
}

void ext2_create(char** disk, size_t diskSize)
{
	ext2 = new(t_ext2);

	ext2->disk = *disk;
	ext2->diskSize = diskSize;
	ext2->superblock = (t_superblock*) (ext2->disk + SUPERBLOCK_OFFSET);
	ext2->groupBlocksCount = ext2->superblock->inodes_count / ext2->superblock->inodes_per_group;
	ext2->blockSize = 1024 << ext2->superblock->log_block_size;

	int groupDescriptorTableBlockNumber = ext2->blockSize == 1024? 2 : 1;
	ext2->groupDescriptors = (t_group_descriptor*) (ext2->disk + groupDescriptorTableBlockNumber * ext2->blockSize);
}

void ext2_open(const char* filePath) {
	int fileDescriptor = open(filePath, O_RDWR);
	if (fileDescriptor == -1) {
		perror("No se pudo abrir el archivo .disk");
		exit(EXIT_FAILURE);
	}

	size_t diskSize = ext2_get_disk_size(filePath);

	char* disk;
	disk = mmap(0, diskSize, PROT_WRITE, MAP_SHARED, fileDescriptor, 0);
	posix_madvise(disk, diskSize, POSIX_MADV_RANDOM);

	if (disk == MAP_FAILED) {
		perror("No se pudo mapear el disco a memoria");
		exit(EXIT_FAILURE);
	}

	ext2_create(&disk, diskSize);
}

void* ext2_get_block(off_t blockNumber) {
	t_ext2* ext2 = ext2_get();
	return ext2->disk + blockNumber * ext2->blockSize;
}

int ext2_get_free_element(int elements_per_group, t_bitarray* (*bitmapGetter)(int), bool isRequestingBlock) {
	t_ext2* ext2 = ext2_get();
	int offset = -1, group = 0;
	while (group < ext2->groupBlocksCount && offset == -1) {
		t_bitarray* bitmap = bitmapGetter(group);
		offset = bitarray_first_false_bit(bitmap);
		bitarray_set_bit(bitmap, offset);
		bitarray_destroy(bitmap);
		group++;
	}

	//TODO: hacer algo util con el -1
	if (isRequestingBlock && offset != -1)
		offset++;

	return offset != -1 ? --group * elements_per_group + offset : -1;
}

int ext2_get_free_block_number() {
	t_ext2* ext2 = ext2_get();
	int blockNumber=ext2_get_free_element(ext2->superblock->blocks_per_group, group_descriptor_get_block_bitmap, true);
	int blockGroupNumber = (blockNumber - 1) / ext2_get()->superblock->blocks_per_group;
	t_group_descriptor actualGroupDescriptor = ext2->groupDescriptors[blockGroupNumber];
	actualGroupDescriptor.free_block_count--;
	ext2_get()->superblock->free_blocks_count--;
	memset(ext2_get_block(blockNumber), '\0', ext2_get()->blockSize);
	return blockNumber;
}

void ext2_destroy() {
	t_ext2* ext2 = ext2_get();
	msync(ext2->disk,ext2->disk,MS_SYNC);
	munmap(ext2->disk, ext2->diskSize);
	free(ext2);
}
