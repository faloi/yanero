/*
 * inode_repository.h
 *
 *  Created on: Jun 27, 2012
 *      Author: faloi
 */

#ifndef INODE_REPOSITORY_H_
#define INODE_REPOSITORY_H_

	#include <sys/types.h>
	#include "inode.h"

	typedef struct {
		t_inode* 			inode;
		pthread_rwlock_t*	lock;
		int					open_times;
	} t_sync_inode;

	void 			inode_repository_create();
	bool			inode_repository_add(int inode_number);
	t_inode*		inode_repository_get(int inode_number);
	t_inode* 		inode_repository_add_and_get(int inode_number);
	void			inode_repository_lock_read(int inode_number);
	void			inode_repository_lock_write(int inode_number);
	void			inode_repository_unlock(int inode_number);
	bool			inode_repository_release(int inode_number);
	bool 			inode_repository_is_open(int inode_number);
	void			inode_repository_destroy();

#endif /* INODE_REPOSITORY_H_ */
