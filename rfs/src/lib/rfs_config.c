/*
 * rfs_config.c
 *
 *  Created on: Jul 9, 2012
 *      Author: faloi
 */

#include <stdlib.h>
#include <commons/global_log.h>
#include "rfs_config.h"
#include <commons/syntax_sugars.h>
#include <commons/config.h>
#include <commons/string.h>
#include <string.h>

static t_config* master_config;
static rfs_config* self;

rfs_config* rfs_config_create(char* path) {
	master_config = config_create(path);

	if (config_keys_amount(master_config) != RFS_CONFIG_KEYS_AMOUNT) {
		puts("El archivo de configuracion no tiene el formato correcto!");
		exit(EXIT_FAILURE);
	}

	self = new(rfs_config);

	self->rfs_ip = config_get_string_value(master_config, "rfs_ip");
	self->rfs_port = config_get_int_value(master_config, "rfs_port");
	self->disk_path = config_get_string_value(master_config, "disk_path");
	self->log_detail = config_get_int_value(master_config, "log_detail");
	self->log_path = config_get_string_value(master_config, "log_path");
	self->max_threads = config_get_int_value(master_config, "max_threads");
	self->cache_enabled = config_get_int_value(master_config, "cache_enabled");
	self->cache_ip = config_get_string_value(master_config, "cache_ip");
	self->cache_port = config_get_int_value(master_config, "cache_port");
	self->max_cache_connections = config_get_int_value(master_config, "max_cache_connections");

	char* lag = config_get_string_value(master_config, "operation_lag");
	char* lag_value = string_truncate(lag, strlen(lag) - 2);
	self->operation_lag = atoi(lag_value);

	if (string_ends_with(lag, "ms"))
		self->operation_lag *= 1000;

	self->show_log_messages = config_get_int_value(master_config,
			"show_log_messages");

	return self;
}

rfs_config* rfs_config_get() {
	return self;
}

void rfs_config_destroy() {
	config_destroy(master_config);
	free(self);
}
