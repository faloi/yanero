/*
 * group_descriptor.h
 *
 *  Created on: 03/05/2012
 *      Author: oli
 */

#ifndef GROUP_DESCRIPTOR_H_
#define GROUP_DESCRIPTOR_H_

	#include <commons/bitarray.h>
	#include <stdint.h>
	#include "inode.h"

	typedef struct {
		uint32_t	block_bitmap;
		uint32_t	inode_bitmap;
		uint32_t	inode_table;
		uint16_t	free_block_count;
		uint16_t 	free_inodes_count;
		uint16_t	used_dirs_count;
		char 		padding[14];
	} __attribute__((__packed__)) t_group_descriptor;

	t_bitarray*		group_descriptor_get_inode_bitmap(int blockGroupNumber);
	t_bitarray*		group_descriptor_get_block_bitmap(int blockGroupNumber);

#endif /* GROUP_DESCRIPTOR_H_ */
