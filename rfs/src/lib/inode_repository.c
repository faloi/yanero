/*
 * inode_repository.c
 *
 *  Created on: Jun 27, 2012
 *      Author: faloi
 */


#include <commons/collections/sync_dictionary.h>
#include <commons/syntax_sugars.h>
#include <linux/stddef.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include "inode_repository.h"
#include "inode.h"

t_sync_dictionary* dictionary;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

// ----------------------------------------------------------
//						PRIVATE HELPERS
// ----------------------------------------------------------

static void sync_inode_destroy(t_sync_inode* self) {
	pthread_rwlock_destroy(self->lock);
	free(self);
}

static t_sync_inode* get_sync_inode(char* path) {
	t_sync_inode* value = sync_dictionary_get(dictionary, path);
	free(path);

	return value;
}

static pthread_rwlock_t* get_lock(char* path) {
	return get_sync_inode(path)->lock;
}

static char* int_to_string(int number) {
	char* key = malloc(3);
	sprintf(key, "%d", number);
	return key;
}

// ----------------------------------------------------------
//						PUBLIC METHODS
// ----------------------------------------------------------

void inode_repository_create() {
	dictionary = sync_dictionary_create(sync_inode_destroy);
}

bool inode_repository_is_open(int inode_number) {
	char* key = int_to_string(inode_number);
	bool return_value = sync_dictionary_has_key(dictionary, key);
	free(key);

	return return_value;
}

bool inode_repository_add(int inode_number) {
	t_sync_inode* new_inode;
	char* key = int_to_string(inode_number);

	pthread_mutex_lock(&mutex);
	if (sync_dictionary_has_key(dictionary, key)) {
		new_inode = sync_dictionary_remove(dictionary, key);
		new_inode->open_times++;
	} else {
		t_inode* inode = inode_get(inode_number);
		if (inode == NULL) return false;

		new_inode = new(t_sync_inode);
		new_inode->inode = inode;
		new_inode->open_times = 1;
		new_inode->lock = new(pthread_rwlock_t);
		pthread_rwlock_init(new_inode->lock, NULL);
	}

	sync_dictionary_put(dictionary, key, new_inode);
	pthread_mutex_unlock(&mutex);

	return true;
}

t_inode* inode_repository_get(int inode_number) {
	char* key = int_to_string(inode_number);

	t_sync_inode* sync_inode = get_sync_inode(key);
	return sync_inode == NULL? NULL : sync_inode->inode;
}

t_inode* inode_repository_add_and_get(int inode_number) {
	bool success = inode_repository_add(inode_number);
	if (!success) return NULL;

	t_inode* inode = inode_repository_get(inode_number);
	return inode == NULL? NULL : inode;
}


void inode_repository_lock_read(int inode_number) {
	char* key = int_to_string(inode_number);

	pthread_rwlock_t* lock = get_lock(key);
	pthread_rwlock_rdlock(lock);
}

void inode_repository_lock_write(int inode_number) {
	char* key = int_to_string(inode_number);

	pthread_rwlock_t* lock = get_lock(key);
	pthread_rwlock_wrlock(lock);
}

void inode_repository_unlock(int inode_number) {
	char* key = int_to_string(inode_number);

	pthread_rwlock_t* lock = get_lock(key);
	pthread_rwlock_unlock(lock);
}

bool inode_repository_release(int inode_number) {
	char* key = int_to_string(inode_number);

	pthread_mutex_lock(&mutex);

	t_sync_inode* old_inode = sync_dictionary_get(dictionary, key);
	if (old_inode == NULL) {
		free(key);
		pthread_mutex_unlock(&mutex);
		return false;
	}

	if (old_inode->open_times == 1)
		sync_dictionary_remove(dictionary, key);
	else {
		old_inode->open_times--;
		free(key);
	}

	pthread_mutex_unlock(&mutex);

	return true;
}

void inode_repository_destroy() {
	sync_dictionary_destroy(dictionary);
	pthread_mutex_destroy(&mutex);
}
