/*
 * rfs_config.h
 *
 *  Created on: Jul 8, 2012
 *      Author: faloi
 */

#ifndef RFS_CONFIG_H_
#define RFS_CONFIG_H_

	#include <stdbool.h>
	#define RFS_CONFIG_KEYS_AMOUNT 12

	typedef struct {
		char* 	rfs_ip;
		int 	rfs_port;
		char* 	disk_path;
		char* 	log_path;
		bool 	show_log_messages;
		int 	log_detail;
		int 	operation_lag;
		int 	max_threads;
		bool	cache_enabled;
		char* 	cache_ip;
		int 	cache_port;
		int		max_cache_connections;
	} rfs_config;

	rfs_config* 	rfs_config_create(char* path);
	rfs_config* 	rfs_config_get();
	void			rfs_config_destroy();

#endif /* RFS_CONFIG_H_ */
