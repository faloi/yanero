/*
 * superblock.h
 *
 *  Created on: May 3, 2012
 *      Author: faloi
 */

#ifndef SUPERBLOCK_H_
#define SUPERBLOCK_H_

	#define SUPERBLOCK_OFFSET 1024

	#include <stdint.h>

	typedef struct {
		uint32_t	inodes_count;		//Count of inodes in the filesystem
		uint32_t	blocks_count;		//Count of blocks in the filesystem
		uint32_t	reserved_blocks_count; 	//Count of the number of reserved blocks
		uint32_t	free_blocks_count;	//Count of the number of free blocks
		uint32_t	free_inodes_count; 	//Count of the number of free inodes
		uint32_t 	superblock_container;	//The block where the superblock is stored
		uint32_t	log_block_size;		//Indicator of the block sizer
		uint32_t	log_frag_size;		//Indicator of the size of the fragments
		uint32_t	blocks_per_group;	//Count of the number of blocks in each block group
		uint32_t	frags_per_group;	//Count of the number of fragments in each block group
		uint32_t	inodes_per_group;	//Count of the number of inodes in each block group
	} __attribute__((__packed__)) t_superblock;

#endif /* SUPERBLOCK_H_ */
