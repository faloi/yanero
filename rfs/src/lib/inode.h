/*
 * inode.h
 *
 *  Created on: 07/05/2012
 *      Author: oli
 */

#ifndef INODE_H_
#define INODE_H_

	#include <stdint.h>
	#include <sys/types.h>
	#include <commons/collections/list.h>
	#include <sys/stat.h>
	#include <stdbool.h>

	//File Format
	enum fileTypes {
		EXT2_DIRECTORY =				0x4000,
		EXT2_REGULAR_FILE =			0x8000,
	};

	//Access Rights
	#define FS_FULL_READ_PRIV        S_IREAD | S_IRGRP | S_IROTH | S_IXUSR
	#define FS_FULL_WRITE_PRIV         S_IWRITE | S_IWGRP | S_IWOTH
	#define FS_FULL_EXEC_PRIV		S_IXUSR |	S_IXGRP | S_IXOTH


	typedef struct {
		uint16_t	mode; // Description of the file and the access rights
		uint16_t	uid; // User ID associated with the file
		uint32_t	size; // Indicates the size of the file in bytes
		uint32_t	atime; // The last time the inode was accessed
		uint32_t 	ctime; // When the inode was created
		uint32_t	mtime; // The last time the inode was modified
		uint32_t 	dtime; // When the inode was deleted
		uint16_t 	gid; // Group ID having access to this file
		uint16_t	links_count; // Indicates how many times this node is linked
		uint32_t	blocks; // Indicates the number of the blocks reserved to this inode
		uint32_t	flags;
		uint32_t	reserved1;
		uint32_t 	block_pointers[15]; // 15 blocks numbers pointing to the blocks containing the data for this inode
		uint32_t	version; // Indicates the file version
		uint32_t 	file_acl;
		uint32_t 	dir_acl;
		uint32_t	i_faddr; // Fragment address
		unsigned char   frag; // Fragment number
		unsigned char   fsize;// Fragment size
		uint16_t  	pad1;
		uint32_t 	reserved2[2];
	} __attribute__((__packed__)) t_inode;

	void 				inode_print_and_destroy_block_list(t_list*);
	t_inode* 			inode_get(off_t offset);
	int  				inode_number_get_free();
	t_inode*			inode_get_free();
	t_inode*			inode_get_from_path(char* path);
	int 				inode_number_get_from_path(char* path);
	t_list* 			inode_get_block_list(t_inode* inode, int offset, int* length, bool blockNumber, bool isRead);
	bool 				add_block(t_inode* inode, int indirectionLevel,int blockPointerIndex);
	void 				inode_add_block(t_inode* inode);


#endif /* INODE_H_ */
