/*
 * ext2_operations.h
 *
 *  Created on: Jun 6, 2012
 *      Author: faloi
 */

#ifndef EXT2_OPERATIONS_H_
#define EXT2_OPERATIONS_H_

	#include <commons/fuse/packages.h>

	//Operations
	t_respuesta_payload_create*		ext2_create_file(t_pedido_payload_create*);
	t_respuesta_payload_open*		ext2_open_file(t_pedido_payload_open*);
	t_respuesta_payload_read* 		ext2_read_file(t_pedido_payload_read*);
	t_respuesta_payload_write* 		ext2_write_file(t_pedido_payload_write*);
	t_respuesta_payload_release* 	ext2_release_file(t_pedido_payload_release*);
	t_respuesta_payload_truncate* 	ext2_truncate_file(t_pedido_payload_truncate*);
	t_respuesta_payload_unlink* 	ext2_unlink_file(t_pedido_payload_unlink*);
	t_respuesta_payload_getattr* 	ext2_get_attributes(t_pedido_payload_getattr*);

	t_respuesta_payload_mkdir* 		ext2_make_directory(t_pedido_payload_mkdir*);
	t_respuesta_payload_rmdir* 		ext2_remove_directory(t_pedido_payload_rmdir*);
	t_respuesta_payload_readdir* 	ext2_read_directory(t_pedido_payload_readdir*);

#endif /* EXT2_OPERATIONS_H_ */
