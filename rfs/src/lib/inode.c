/*
 * inode.c
 *
 *  Created on: 17/05/2012
 *      Author: oli
 */

#include <commons/collections/list.h>
#include <commons/syntax_sugars.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include "inode_repository.h"
#include "directory.h"
#include "inode.h"
#include "ext2.h"
#include "commons/cache_utils.h"
#include <commons/global_log.h>
#include "../lib/rfs_config.h"
#include <math.h>
#include <stdio.h>

//PRIVATE FUNCTIONS

int max(int x, int y) {
	return x > y ? x : y;
}

int min(int x, int y) {
	return x < y ? x : y;
}

t_inode* inode_get_free() {
		return ext2_get_block(inode_number_get_free());
}

t_inode* inode_get(off_t inodeNumber) {
	t_ext2* ext2 = ext2_get();

	inodeNumber--;
	int bg = (inodeNumber-1) / ext2->superblock->inodes_per_group;
	int offset = inodeNumber % ext2->superblock->inodes_per_group;
	t_inode* inodeTable = ext2_get_block(ext2->groupDescriptors[bg].inode_table);
	return (inodeTable + offset);
}

static char* get_block_from_cache(int blockNumber) {
	global_log_trace("Buscando en la cache el bloque %d", blockNumber);
	char* block = cache_get_block(blockNumber);
	if (block == NULL) {
		ext2_simulate_lag();
		block = ext2_get_block(blockNumber);
		global_log_trace("Guardando en la cache el bloque %d", blockNumber);
		cache_set_block(blockNumber, block);
	}

	return block;
}

static void get_blocks(int blockNumber, t_list* blockList, int indirectionLevel, int offset, int* length, bool blocknumberlist, bool isRead) {
	int i;
	int pointersPerBlock = ext2_get()->blockSize / sizeof(uint32_t);
	int pointersCount = indirectionLevel == 0 ? 1 : pointersPerBlock;
	uint32_t* pointers = indirectionLevel == 0? &blockNumber : ext2_get_block(blockNumber);

	if (indirectionLevel > 1) {
		indirectionLevel--;
		for (i = 0; i < pointersCount && *length > 0; i++) {
			if (offset > 0) offset -= pow(pointersPerBlock, indirectionLevel);
			if (offset < 0) offset = 0;
			get_blocks(pointers[i], blockList, indirectionLevel, offset, length, blocknumberlist, isRead);
		}
	} else {
		for (i = offset; i < pointersCount; i++) {
			if (*length == 0) return;

			if (blocknumberlist) {
				uint32_t* blockNumber = new(uint32_t);
				*blockNumber = pointers[i];
				list_add(blockList, blockNumber);
			} else {
				char* block = isRead && rfs_config_get()->cache_enabled? get_block_from_cache(pointers[i]) : ext2_get_block(pointers[i]);
				list_add(blockList, block);
			}

			(*length)--;
		}
	}
}



//PUBLIC FUNCTIONS

int inode_number_get_free() {
	t_ext2* ext2 = ext2_get();
	uint32_t freeInodeNumber = ext2_get_free_element(ext2->superblock->inodes_per_group,group_descriptor_get_inode_bitmap, false)+1;
	int blockGroupNumber = (freeInodeNumber-1)/ext2_get()->superblock->inodes_per_group;
	t_group_descriptor actualGroupDescriptor = ext2->groupDescriptors[blockGroupNumber];
	actualGroupDescriptor.free_inodes_count--;
	ext2_get()->superblock->free_inodes_count--;
	memset(inode_get(freeInodeNumber), '\0', sizeof(t_inode));
	return freeInodeNumber;
}

t_list* inode_get_block_list(t_inode* inode, int bytesOffset, int* pointerToLength, bool blocknumberlist, bool isRead) {
	t_list* block_list = list_create();
	size_t blockSize = ext2_get()->blockSize;
	int bytesLength = min(*pointerToLength, inode->size - bytesOffset);

	int blocksOffset = bytesOffset / blockSize;
	int blocksLength = (bytesOffset + bytesLength) / blockSize - blocksOffset;

	if (bytesLength % blockSize != 0 && (bytesOffset + bytesLength) % blockSize != 0)
		blocksLength++;

	int pointersPerBlock = ext2_get()->blockSize / sizeof(uint32_t);
	int indirectionLevel;
	if (blocksOffset < 12)
		indirectionLevel = 0;
	else if (blocksOffset < 12 + pointersPerBlock)
		indirectionLevel = 1;
	else if (blocksOffset < 12 + pointersPerBlock + pow(pointersPerBlock, 2))
		indirectionLevel = 2;
	else
		indirectionLevel = 3;

	int blocksToRead = blocksLength;
	int indirectionOffset = indirectionLevel == 0? 0 : blocksOffset - 12;
	while (blocksToRead > 0) {
		int pointer = indirectionLevel == 0? blocksOffset++ : 11 + indirectionLevel;
		get_blocks(inode->block_pointers[pointer], block_list, indirectionLevel, indirectionOffset, &blocksToRead, blocknumberlist, isRead);

		indirectionOffset = 0;

		if (blocksOffset > 11)
			indirectionLevel++;
	}

	*pointerToLength = (blocksLength - blocksToRead) * blockSize;

	return block_list;
}

void inode_add_block(t_inode* inode){
		int i;
		bool agregado=false;
		for (i = 0; i < 15 && agregado==false; i++) {
			int indirectionLevel = i < 12 ? 0 : (i + 1) % 12;
			agregado=add_block(inode, indirectionLevel,i);
		}
}


bool add_block(t_inode* inode, int indirectionLevel,int blockPointerIndex){
	int i;
	int pointersPerBlock = ext2_get()->blockSize / sizeof(uint32_t);
	int pointersCount = indirectionLevel == 0 ? 1 : pointersPerBlock;

	switch(indirectionLevel){
		case 0:{
			if(inode->block_pointers[blockPointerIndex]==0){
				inode->block_pointers[blockPointerIndex]=ext2_get_free_block_number();
				return true;
			}else{
				return false;
			}
		}
		case 1:{
			if(inode->block_pointers[blockPointerIndex]==0)
				inode->block_pointers[blockPointerIndex]=ext2_get_free_block_number();
			uint32_t* pointers = ext2_get_block(inode->block_pointers[blockPointerIndex]);
			int i=0;
			while(pointers[i]!=0 && i<pointersCount){
				i++;
			}
			if(i<pointersCount){
				pointers[i]=ext2_get_free_block_number();
				return true;
			}else{
				return false;
			}
		}
		case 2:{
			if(inode->block_pointers[blockPointerIndex]==0)
				inode->block_pointers[blockPointerIndex]=ext2_get_free_block_number();
			uint32_t* pointers = ext2_get_block(inode->block_pointers[blockPointerIndex]);
			int i=0;
			while(pointers[i]!=0 && i<pointersCount){
				uint32_t* double_pointers=ext2_get_block(pointers[i]);
				int a = 0;
				while(double_pointers[a]!=0 && a<pointersCount){
					a++;
				}
				if(a<pointersCount){
					double_pointers[a]=ext2_get_free_block_number();
					return true;
				}
				i++;
			}
			if(i<pointersCount){
				pointers[i]=ext2_get_free_block_number();
				uint32_t* double_pointers=ext2_get_block(pointers[i]);
				double_pointers[0]=ext2_get_free_block_number();
				return true;
			}else{
				return false;
			}
		}
		case 3:{
			if(inode->block_pointers[blockPointerIndex]==0)
				inode->block_pointers[blockPointerIndex]=ext2_get_free_block_number();
			uint32_t* pointers = ext2_get_block(inode->block_pointers[blockPointerIndex]);
			int i=0;
			while(pointers[i]!=0 && i<pointersCount){
				uint32_t* double_pointers=ext2_get_block(pointers[i]);
				int a = 0;
				while(double_pointers[a]!=0 && a<pointersCount){
					uint32_t* triple_pointers=ext2_get_block(double_pointers[a]);
					int d=0;
					while(triple_pointers[d]!=0 && d<pointersCount){
						d++;
					}
					if(d<pointersCount){
						triple_pointers[d]=ext2_get_free_block_number();
						return true;
					}
					a++;
				}
				if(a<pointersCount){
					double_pointers[a]=ext2_get_free_block_number();
					uint32_t* triple_pointers=ext2_get_block(double_pointers[a]);
					triple_pointers[0]=ext2_get_free_block_number();
					return true;
				}
				i++;
			}
			if(i<pointersCount){
				pointers[i]=ext2_get_free_block_number();
				uint32_t* double_pointers=ext2_get_block(pointers[i]);
				double_pointers[0]=ext2_get_free_block_number();
				uint32_t* triple_pointers=ext2_get_block(double_pointers[0]);
				triple_pointers[0]=ext2_get_free_block_number();
				return true;
			}else{
				return false;
			}
		}
	}
}

int inode_number_get_from_path(char* path) {
	if (strcmp(path,"/")==0){
		return 2;
	}
	t_list* directoriesToRead = get_files_names(path);
	int currentInodeNumber = 2;
	int i;
	for (i = 0; i < list_size(directoriesToRead); i++) {
		char* subdir = list_get(directoriesToRead, i);
		currentInodeNumber = directory_get_subdirectory_inode_number(currentInodeNumber, subdir);
	}
	list_destroy(directoriesToRead);
	return currentInodeNumber;
}






