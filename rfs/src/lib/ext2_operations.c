/*
 * ext2_operations.c
 *
 *  Created on: Jun 6, 2012
 *      Author: faloi
 */

#include <commons/collections/list.h>
#include <commons/syntax_sugars.h>
#include <commons/fuse/packages.h>
#include <commons/string.h>
#include <commons/math.h>
#include <stdlib.h>
#include <string.h>
#include "ext2_operations.h"
#include "inode_repository.h"
#include "directory.h"
#include "ext2_def.h"
#include "inode.h"
#include "ext2.h"
#include "ext2_def.h"
#include "file.h"
#include <commons/fuse/packages_utils.h>

t_respuesta_payload_create* ext2_create_file(t_pedido_payload_create* request) {
	t_respuesta_payload_create* answer = new(t_respuesta_payload_create);
	answer->status = directory_new_entry(request->path, EXT2_REGULAR_FILE);

	destroy_pedidoPayloadCreate(request);

	return answer;
}

t_respuesta_payload_open* ext2_open_file(t_pedido_payload_open* request) {
	int inode_number = inode_number_get_from_path(request->path);
	destroy_pedidoPayloadOpen(request);

	t_respuesta_payload_open* answer = new(t_respuesta_payload_open);

	if (inode_number < 0) {
		answer->status = OPERATION_FILE_NOT_FOUND;
		return answer;
	}

	bool success = inode_repository_add(inode_number);
	answer->status = success ? OPERATION_OK : OPERATION_ERROR;

	return answer;
}

t_respuesta_payload_read* ext2_read_file_failure(t_pedido_payload_read* request, int status_code) {
	destroy_pedidoPayloadRead(request);

	t_respuesta_payload_read* answer = new(t_respuesta_payload_read);
	answer->content = NULL;
	answer->size = 0;
	answer->status = status_code;

	return answer;
}

t_respuesta_payload_read* ext2_read_file(t_pedido_payload_read* request) {
	int inode_number = inode_number_get_from_path(request->path);

	if (inode_number < 0)
		return ext2_read_file_failure(request, OPERATION_FILE_NOT_FOUND);

	t_inode* inode = inode_repository_get(inode_number);

	if (request->offset > inode->size)
		return ext2_read_file_failure(request, OPERATION_ERROR);

	inode_repository_lock_read(inode_number);

	int* requestedSize = new(int);
	*requestedSize = min(request->size, inode->size - request->offset);
	t_list* blocks = inode_get_block_list(inode, request->offset, requestedSize, false, true);

	t_respuesta_payload_read* answer = new(t_respuesta_payload_read);
	answer->status = OPERATION_OK;
	answer->size = min(request->size, *requestedSize);

	int blockSize = ext2_get()->blockSize;
	char* data = list_to_memory_stream(blocks, blockSize);
	char* content = malloc(answer->size);
	int offset = request->offset % blockSize;
	memcpy(content, data + offset, answer->size);
	answer->content = content;

	inode_repository_unlock(inode_number);

	destroy_pedidoPayloadRead(request);
	return answer;
}

t_respuesta_payload_write* ext2_write_failure(t_pedido_payload_write* request, enum statusCodes status) {
	t_respuesta_payload_write* answer = new(t_respuesta_payload_write);
	answer->requested = 0;
	answer->status = OPERATION_FILE_NOT_FOUND;

	destroy_pedidoPayloadWrite(request);
	return answer;
}

t_respuesta_payload_write* ext2_write_file(t_pedido_payload_write* request) {
	int fileInodeNumber = inode_number_get_from_path(request->path);

	if (fileInodeNumber < 0)
		return ext2_write_failure(request, OPERATION_FILE_NOT_FOUND);

	t_inode* inode = inode_repository_add_and_get(fileInodeNumber);
	inode_repository_lock_write(fileInodeNumber);
	int written_bytes = file_write(inode, request->offset, request->size, request->content);
	inode_repository_unlock(fileInodeNumber);
	inode_repository_release(fileInodeNumber);

	t_respuesta_payload_write* answer = new(t_respuesta_payload_write);
	answer->requested = written_bytes;
	answer->status = OPERATION_OK;

	return answer;
}

t_respuesta_payload_readdir* ext2_read_directory_failure(t_pedido_payload_readdir* request, enum statusCodes status_code) {
	destroy_pedidoPayloadReaddir(request);

	t_respuesta_payload_readdir* answer = new(t_respuesta_payload_readdir);
	answer->arr_archivos = NULL;
	answer->cant_archivos = 0;
	answer->status = status_code;

	return answer;
}

t_respuesta_payload_readdir* ext2_read_directory(t_pedido_payload_readdir* request) {
	int directoryInodeNumber = inode_number_get_from_path(request->path);

	if (directoryInodeNumber < 0)
		return ext2_read_directory_failure(request, OPERATION_FILE_NOT_FOUND);

	t_inode* inodeDirectory = inode_repository_add_and_get(directoryInodeNumber);

	if (EXT2_INODE_HAS_MODE_FLAG(inodeDirectory, EXT2_REGULAR_FILE)) {
		inode_repository_release(directoryInodeNumber);
		return ext2_read_directory_failure(request, OPERATION_NOT_A_DIRECTORY);
	}

	if (directoryInodeNumber == 11) {
		inode_repository_release(directoryInodeNumber);
		return ext2_read_directory_failure(request, OPERATION_PERMISSION_DENIED);
	}

	inode_repository_lock_read(directoryInodeNumber);
	char* directoryList = directory_list(inodeDirectory, request->path);
	inode_repository_unlock(directoryInodeNumber);
	inode_repository_release(directoryInodeNumber);

	destroy_pedidoPayloadReaddir(request);

	t_respuesta_payload_readdir* answer = new(t_respuesta_payload_readdir);
	answer->arr_archivos = string_split(directoryList, "/");
	answer->cant_archivos = string_occurrences_of(directoryList, '/');
	answer->status = OPERATION_OK;

	return answer;
}

t_respuesta_payload_release* ext2_release_file(t_pedido_payload_release* request) {
	int inode_number = inode_number_get_from_path(request->path);
	bool success = inode_repository_release(inode_number);
	destroy_pedidoPayloadRelease(request);

	t_respuesta_payload_release* answer = new(t_respuesta_payload_release);
	answer->status = success? OPERATION_OK : OPERATION_ERROR;

	return answer;
}

t_respuesta_payload_truncate* ext2_truncate_file_return(t_pedido_payload_truncate* request, enum statusCodes status_code) {
	destroy_pedidoPayloadTruncate(request);

	t_respuesta_payload_truncate* answer = new(t_respuesta_payload_truncate);
	answer->status = status_code;

	return answer;
}

t_respuesta_payload_truncate* ext2_truncate_file(t_pedido_payload_truncate* request) {
	int inode_number = inode_number_get_from_path(request->path);
	if (inode_number < 0)
		return ext2_truncate_file_return(request, OPERATION_FILE_NOT_FOUND);

	t_inode* inode = inode_repository_add_and_get(inode_number);

	inode_repository_lock_write(inode_number);
	bool answer = file_truncate(inode, request->size);
	inode_repository_unlock(inode_number);
	inode_repository_release(inode_number);

	return ext2_truncate_file_return(request, answer? OPERATION_OK : OPERATION_ERROR);
}

t_respuesta_payload_unlink* ext2_unlink_return(t_pedido_payload_unlink* request, enum statusCodes status_code) {
	destroy_pedidoPayloadUnlink(request);

	t_respuesta_payload_unlink* answer = new(t_respuesta_payload_unlink);
	answer->status = status_code;

	return answer;
}

t_respuesta_payload_unlink* ext2_unlink_file(t_pedido_payload_unlink* request) {
	int inode_number = inode_number_get_from_path(request->path);

	if (inode_number < 0)
		return ext2_unlink_return(request, OPERATION_FILE_NOT_FOUND);

	if (inode_repository_is_open(inode_number))
		return ext2_unlink_return(request, OPERATION_FILE_IN_USE);

	t_inode* inode = inode_repository_add_and_get(inode_number);
	if (EXT2_INODE_HAS_MODE_FLAG(inode, EXT2_DIRECTORY)) {
		inode_repository_release(inode_number);
		return ext2_unlink_return(request, OPERATION_IS_A_DIRECTORY);
	}

	inode_repository_lock_write(inode_number);
	file_kill(inode_number, request->path);
	inode_repository_unlock(inode_number);
	inode_repository_release(inode_number);

	return ext2_unlink_return(request, OPERATION_OK);
}

t_respuesta_payload_getattr* 	ext2_get_attributes(t_pedido_payload_getattr* request){
	t_respuesta_payload_getattr* answer =new(t_respuesta_payload_getattr);

	int inodeNumber = inode_number_get_from_path(request->path);
	destroy_pedidoPayloadGetattr(request);

	if (inodeNumber < 0) {
		answer->status = OPERATION_FILE_NOT_FOUND;
		return answer;
	}

	t_inode* inode = inode_repository_add_and_get(inodeNumber);
	if (inode == NULL) {
		answer->status = OPERATION_FILE_NOT_FOUND;
		return answer;
	}

	inode_repository_lock_read(inodeNumber);
	if(EXT2_INODE_HAS_MODE_FLAG(inode,EXT2_DIRECTORY))
		answer->mode=DIRECTORY;
	else
		answer->mode=REGULAR_FILE;

	answer->size=inode->size;
	answer->status=OPERATION_OK;
	inode_repository_unlock(inodeNumber);
	inode_repository_release(inodeNumber);

	return answer;
}

t_respuesta_payload_mkdir* ext2_make_directory(t_pedido_payload_mkdir* request) {
	t_respuesta_payload_mkdir* answer = new(t_respuesta_payload_mkdir);
	answer->status = directory_new_entry(request->path, EXT2_DIRECTORY);

	destroy_pedidoPayloadMkdir(request);

	return answer;
}

t_respuesta_payload_rmdir* ext2_remove_directory_return(t_pedido_payload_rmdir* request, enum statusCodes status_code) {
	destroy_pedidoPayloadRmdir(request);

	t_respuesta_payload_rmdir* answer = new(t_respuesta_payload_rmdir);
	answer->status = status_code;

	return answer;
}

t_respuesta_payload_rmdir* ext2_remove_directory(t_pedido_payload_rmdir* request){
	int inode_number = inode_number_get_from_path(request->path);

	if (inode_number < 0)
		return ext2_remove_directory_return(request, OPERATION_FILE_NOT_FOUND);

	t_inode* inode = inode_repository_add_and_get(inode_number);

	if (EXT2_INODE_HAS_MODE_FLAG(inode, EXT2_REGULAR_FILE)) {
		inode_repository_release(inode_number);
		return ext2_remove_directory_return(request, OPERATION_NOT_A_DIRECTORY);
	}

	inode_repository_lock_read(inode_number);
	if (!directory_is_empty(inode, request->path)) {
		inode_repository_unlock(inode_number);
		inode_repository_release(inode_number);
		return ext2_remove_directory_return(request, OPERATION_ERROR);
	}
	inode_repository_unlock(inode_number);

	inode_repository_lock_write(inode_number);
	file_kill(inode_number, request->path);
	inode_repository_unlock(inode_number);
	inode_repository_release(inode_number);

	return ext2_remove_directory_return(request, OPERATION_OK);
}
