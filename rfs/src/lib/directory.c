/*
 * directory.c
 *
 *  Created on: 26/05/2012
 *      Author: oli
 */

#include <commons/collections/list.h>
#include <commons/fuse/packages.h>
#include <commons/syntax_sugars.h>
#include <commons/string.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "inode_repository.h"
#include "group_descriptor.h"
#include "directory.h"
#include "ext2_def.h"
#include "inode.h"
#include "ext2.h"

 //PRIVATE FUNCTIONS
 t_list* get_files_names(char* path) {
 	t_list* file_name_list = list_create();
 	int directoriesCount = string_occurrences_of(path, '/');
 		char** directory_names = string_split(path, "/");

 		int i;
	for (i = 0; i <= directoriesCount; i++) {
		char* dir = directory_names[i];
		if (dir != NULL)
			list_add(file_name_list, directory_names[i]);
	}

 	return file_name_list;
 }

 t_list* get_path_list(char* path){
	t_list* list_path = list_create();
	char* newPath;
	int i, last = 0;

	for (i = 0; i < strlen(path); i++)
		if (path[i] == '/')
			last = i;

	if (last == 0)
		newPath = string_truncate(path, last + 1);
	else
		newPath = string_truncate(path, last);

	for (i = 0; i < last + 1; i++)
		path++;

	char* newDir = strdup(path);
	list_add(list_path, newPath);
	list_add(list_path, newDir);

	return list_path;
 }

bool directory_is_empty(t_inode* inode, char* path) {
	char* directoryList = directory_list(inode, path);
	int files_count=string_occurrences_of(directoryList, '/');

	return files_count==2;
}

 //PUBLIC FUNCTIONS

 char* directory_list(t_inode* inode, char* path) {
	int block_size = ext2_get()->blockSize;
 	int* lengthInode = new(int);
 	*lengthInode = inode->size;
 	t_directory* currentDirectory;
 	t_list* directoryList = inode_get_block_list(inode, 0, lengthInode, false, false);
 	int i=0;
 	char* directory_list = strdup("");
 	char* currentDirectoryName;
 	while(i<=list_size(directoryList)-1){
		void* data = list_get(directoryList, i);
		currentDirectory = (t_directory*) data;
		int currentSize=currentDirectory->record_size;
		char letter=currentDirectory->name[0];
 		if((currentSize!=0)&&(letter!="C")){
			currentDirectoryName=string_truncate(currentDirectory->name, currentDirectory->name_length);
			string_append(&directory_list,currentDirectoryName);
			string_append(&directory_list,"/");
 		}
 		int offset =currentDirectory->record_size;
 		while ((currentDirectory->record_size > 0)&&(offset<=ext2_get()->blockSize)) {
 			data += currentDirectory->record_size;
 			currentDirectory = (t_directory*) data;
 			offset+= currentDirectory->record_size;
 			if((offset<=block_size) && (currentDirectory->record_size>0)){
 				currentDirectoryName = string_truncate(currentDirectory->name, currentDirectory->name_length);
 				string_append(&directory_list,currentDirectoryName);
 				string_append(&directory_list,"/");
 			}
 		}
 		i++;
 	}
 	if(strcmp(currentDirectoryName,"")==0)
 		directory_list=string_truncate(directory_list,strlen(directory_list)-1);
 	free(currentDirectoryName);
 	list_destroy(directoryList);
 	return directory_list;
 }

void directory_delete_entry(char* path, char* fileName) {
	int block_size = ext2_get()->blockSize;
	int inode_number = inode_number_get_from_path(path);
	t_inode* inode = inode_repository_add_and_get(inode_number);

	int* lengthInode = new(int);
	*lengthInode = inode->size;
	t_directory* currentDirectory;
	t_list* blocks = inode_get_block_list(inode, 0, lengthInode, false, false);

	int i = 0;
	char* currentDirectoryName;
	int encontrado = 0;
	int last_size;
	while ((i <= list_size(blocks) - 1)&& (encontrado == 0)) {
		int offset = 0;
		void* data = list_get(blocks, i);
		currentDirectory = (t_directory*) data;
		t_directory* last_directory_entry;
		int count=0;
		while ((offset < ext2_get()->blockSize) && (encontrado == 0) && (currentDirectory->record_size>0)) {
			data += currentDirectory->record_size;
			offset += currentDirectory->record_size;
			if (offset <= block_size) {
				currentDirectoryName = string_truncate(currentDirectory->name, currentDirectory->name_length);
				if (strcmp(currentDirectoryName, fileName) == 0) {
					if (offset == block_size) {
						if(count!=0){
							currentDirectory->record_size=0;
							memcpy(currentDirectory->name,"C",1);

							if(last_directory_entry->record_size!=0)
								last_directory_entry->record_size +=last_size;
						}else{
							currentDirectory->record_size=0;
							memcpy(currentDirectory->name,"C",1);
						}
					} else {
						if(count==0){
							t_directory* next= (t_directory*) data;
							currentDirectory->inode=next->inode;
							memcpy(currentDirectory->name,next->name,next->name_length);
							currentDirectory->name_length=next->name_length;
							currentDirectory->padding=next->padding;
							currentDirectory->record_size+=next->record_size;
							next->record_size=0;
							memcpy(next->name,"C",1);

						}else{
							if (last_directory_entry->record_size!=0){
								last_directory_entry->record_size = last_directory_entry->record_size + currentDirectory->record_size;
							}
							currentDirectory->record_size=0;
							memcpy(currentDirectory->name,"C",1);
							currentDirectory = (t_directory*) data;
							data += currentDirectory->record_size;
							offset += currentDirectory->record_size;
						}
					}
					encontrado = 1;
				} else {
					last_directory_entry = currentDirectory;
				}
			}
//			int realSize = 8 + currentDirectory->name_length;
//			if ((realSize % 4) != 0) {
//				int a;
//				for (a = 0; (realSize % 4) != 0; a++) {
//					realSize++;
//				}
//			}
			if(encontrado!=1){
				last_size=block_size-offset;
				currentDirectory = (t_directory*) data;
				count++;
			}
		}
		i++;
	}

	inode_repository_release(inode_number);
}

enum statusCodes directory_new_entry(char* path,enum fileTypes file_type){
	 if(ext2_get()->superblock->free_inodes_count==0)
		 return OPERATION_ERROR;
	 if((ext2_get()->superblock->free_blocks_count==0) &&(file_type==EXT2_DIRECTORY))
		 return OPERATION_ERROR;

	 int block_size = ext2_get()->blockSize;

	 t_list* fileNames = get_path_list(path);
	 char* pathFather=list_get(fileNames,0);
	 char* fileName=list_get(fileNames,1);
	 int inodeNumberFather = inode_number_get_from_path(pathFather);
	 inode_repository_add(inodeNumberFather);
	 inode_repository_lock_write(inodeNumberFather);
	 if(directory_get_subdirectory_inode_number(inodeNumberFather, fileName) != -1){
		 inode_repository_release(inodeNumberFather);
		 inode_repository_lock_write(inodeNumberFather);
		 return OPERATION_ERROR;
	 }

	 t_inode* inode = inode_repository_get(inodeNumberFather);
  	 int lengthInode = inode->size;
  	 t_directory* subDirectory;
  	 t_list* subDirectoryList = inode_get_block_list(inode, 0, &lengthInode, false, false);
  	 int i = 0;
  	 bool added=false;
  	 int last_offset;
  	 uint16_t realSize;
  	 while(i<=list_size(subDirectoryList)-1){
  	 		void* data = list_get(subDirectoryList, i);
  	 		subDirectory = (t_directory*) data;
  	 		int offset =subDirectory->record_size;
  	 		int count=0;
  	 		while (offset<=ext2_get()->blockSize && !added) {
  	 			if((count==0)&&(subDirectory->record_size==block_size)){
					realSize = 8 + subDirectory->name_length;
					if((realSize%4) != 0){
						int a;
						for(a=0;(realSize%4)!=0;a++){
							realSize++;
						}
					}
					subDirectory->record_size=realSize;
					offset=block_size;
					last_offset=realSize;
					data+=realSize;
  	 			}else{
  	  	 			data += subDirectory->record_size;
  	 			}
  	 			subDirectory = (t_directory*) data;
  	 			if((offset==ext2_get()->blockSize)&&(i==list_size(subDirectoryList)-1)){
  	 				added=true;
  	 				set_new_entry(subDirectory, fileName, last_offset, inodeNumberFather, file_type);
  	 				offset+=subDirectory->record_size;
  				}else{
  					if((offset+ subDirectory->record_size)==block_size){
  						    realSize = 8 + subDirectory->name_length;
  						    if((realSize%4) != 0){
  						    	int a;
  						    	for(a=0;(realSize%4)!=0;a++){
  						    		realSize++;
  						    	}
  						    }
  							last_offset= offset+realSize;
  							if(last_offset==block_size){
  								offset=block_size+1;
  							}else{
								offset+=subDirectory->record_size;
								subDirectory->record_size=realSize;
  							}
  					}else{
  						offset+=subDirectory->record_size;
  					}
  				}
  	 			count++;
  			}
  	 		i++;
  	 	}
  	 	if(added==false){
  	 		if(ext2_get()->superblock->free_blocks_count==0){
  	 		 	inode_repository_unlock(inodeNumberFather);
  	 		 	inode_repository_release(inodeNumberFather);
  	 	  	 	list_destroy(subDirectoryList);
  	 	  	 	list_destroy(fileNames);
  	 			return OPERATION_ERROR;
  	 		}
  	 		inode_add_block(inode);
  	 		inode->size+=block_size;
  	 		lengthInode = inode->size;
  	 		subDirectoryList = inode_get_block_list(inode, 0, &lengthInode, false, false);
  	 		t_directory* subDirectory = list_get(subDirectoryList, list_size(subDirectoryList)-1);
			set_new_entry(subDirectory,fileName, 0, inodeNumberFather, file_type);
			added=true;
  	 	}
	 	inode_repository_unlock(inodeNumberFather);
	 	inode_repository_release(inodeNumberFather);
  	 	list_destroy(subDirectoryList);
  	 	list_destroy(fileNames);
  	 	return OPERATION_OK;
   }
 
  void set_new_entry(t_directory* directory, char* newName,int offset,int father_inode_number,enum fileTypes file_type){
	    int block_size = ext2_get()->blockSize;
	  	t_inode* father_inode = inode_repository_get(father_inode_number);
	  	father_inode->links_count++;
	  	memcpy(directory->name,newName,strlen(newName));
 		directory->inode = inode_number_get_free();
 		directory->name_length=strlen(newName);
 		directory->record_size=block_size-offset;
 		int inodeNumber=directory->inode;
 		inode_repository_add(inodeNumber);
 		t_inode* inode = inode_repository_get(inodeNumber);
 		inode->version= father_inode->version++;
		inode->gid=1000;
		inode->uid=1000;
		int i;
		if(file_type==EXT2_DIRECTORY){
			inode->blocks=2;
			inode->links_count=2;
			inode->size=ext2_get()->blockSize;
			inode->mode= EXT2_DIRECTORY | EXT2_IRUSR | EXT2_IXUSR;
			int offset_empty_directory;
			int blockNumberData=ext2_get_free_block_number();
			void* data =ext2_get_block(blockNumberData);
			inode->block_pointers[0]=blockNumberData;
			t_directory* empty_directory= (t_directory*) data;
			empty_directory->inode=inodeNumber;
			memcpy(empty_directory->name,".",1);
			empty_directory->name_length=1;
			empty_directory->record_size=12;
			offset_empty_directory=empty_directory->record_size;
			data+= offset_empty_directory;
			empty_directory = (t_directory*)data;
			empty_directory->inode = father_inode_number;
			memcpy(empty_directory->name,"..",2);
			empty_directory->name_length=2;
			empty_directory->record_size=ext2_get()->blockSize-offset_empty_directory;
 		}else{
 			inode->mode=EXT2_REGULAR_FILE | EXT2_IRUSR | EXT2_IWUSR ;
 			inode->size=0;
 			inode->blocks=0;
 			inode->links_count=1;
 			inode->file_acl=0;
			inode->dir_acl=0;
			inode->pad1=0;
			inode->i_faddr=0;
 		}
 		inode_repository_release(inodeNumber);
  }

  int directory_get_subdirectory_inode_number(int currentInodeNumber, char* subdir) {
	t_inode* currentInode= inode_repository_add_and_get(currentInodeNumber);

	int* length = new(int);
  	*length = currentInode->size;
  	t_list* blocks = inode_get_block_list(currentInode, 0, length, false, false);
  	free(length);
  	t_directory* currentDirectory;
  	int i = 0;
  	int size = list_size(blocks);
  	int encontrado = 0;
  	while((i<=size-1) && (encontrado==0) ){
  		void* data = list_get(blocks, i);
  		currentDirectory = (t_directory*) data;
  		char* realName = string_truncate(currentDirectory->name, currentDirectory->name_length);
  		int offset =currentDirectory->record_size;
  		while ((strcmp(realName, subdir) != 0) && (currentDirectory->record_size > 0)&&(offset<=ext2_get()->blockSize)) {
  			data += currentDirectory->record_size;
  			currentDirectory = (t_directory*) data;
  			realName = string_truncate(currentDirectory->name, currentDirectory->name_length);
  			offset+= currentDirectory->record_size;
  		}
  		if(strcmp(realName, subdir) ==0)
  			encontrado=1;
  		i++;
  		free(realName);
  	}
	inode_repository_release(currentInodeNumber);
	list_destroy(blocks);
  	if(encontrado == 0){
  		return -1;
  	}else{
  		off_t inodeNumber=currentDirectory->inode;
  		return inodeNumber;
  	}
  }
