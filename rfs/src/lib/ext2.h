/*
 * ext2.h
 *
 *  Created on: May 6, 2012
 *      Author: faloi
 */

#ifndef EXT2_H_
#define EXT2_H_

	#include "superblock.h"
	#include "group_descriptor.h"
	#include <commons/bitarray.h>
	#include <unistd.h>
	#include <stdbool.h>

	#define EXT2_PATH_MAX_LENGTH 255

	typedef struct {
		char* 					disk;			//Archivo mapeado a memoria
		size_t					diskSize;
		size_t					blockSize;
		int						groupBlocksCount;
		t_superblock*			superblock;
		t_group_descriptor*		groupDescriptors;
	} t_ext2;

	//Methods
	void 	ext2_open(const char* filePath);
	void*	ext2_get_block(off_t blockNumber);
	char*	ext2_get_free_block();
	int 	ext2_get_free_block_number();
	void	ext2_destroy();
	int 	ext2_get_free_element(int elements_per_group, t_bitarray* (*bitmapGetter)(int), bool isRequestingBlock);
	void 	ext2_simulate_lag();

	//Getter
	t_ext2*		ext2_get();

#endif /* EXT2_H_ */
